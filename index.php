<?
date_default_timezone_set('Israel');

$yii = dirname(__FILE__) . '/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';
defined('YII_DEBUG') or define('YII_DEBUG', true);

if (!YII_DEBUG) {
	error_reporting(0);
}
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
require_once(dirname(__FILE__) . '/protected/functions/yii.php');
require_once($yii);
Yii::createWebApplication($config)->run();