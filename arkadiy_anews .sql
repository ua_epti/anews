-- phpMyAdmin SQL Dump
-- version 3.5.3
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 11 2014 г., 19:08
-- Версия сервера: 5.5.28-log
-- Версия PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `arkadiy_anews`
--

-- --------------------------------------------------------

--
-- Структура таблицы `AuthAssignment`
--

CREATE TABLE IF NOT EXISTS `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `AuthAssignment`
--

INSERT INTO `AuthAssignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '1', NULL, 'N;'),
('Authenticated', '17', NULL, 'N;'),
('Authenticated', '20', NULL, 'N;'),
('junior_manager', '4', NULL, 'N;'),
('manager', '2', NULL, 'N;'),
('middle_manager', '5', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `AuthItem`
--

CREATE TABLE IF NOT EXISTS `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `AuthItem`
--

INSERT INTO `AuthItem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Cтарший менеджер', NULL, 'N;'),
('Api.*', 1, NULL, NULL, 'N;'),
('Api.Index', 0, NULL, NULL, 'N;'),
('Authenticated', 2, 'Пользователь', NULL, 'N;'),
('Guest', 2, 'Гость', NULL, 'N;'),
('junior_manager', 2, 'Младший менеджер', NULL, 'N;'),
('manager', 2, 'Менеджер', NULL, 'N;'),
('middle_manager', 2, 'Средний менеджер', NULL, 'N;'),
('News.Default.*', 1, NULL, NULL, 'N;'),
('News.Default.Index', 0, NULL, NULL, 'N;'),
('Site.*', 1, NULL, NULL, 'N;'),
('Site.Error', 0, NULL, NULL, 'N;'),
('Site.Index', 0, NULL, NULL, 'N;'),
('Tasks.Default.*', 1, NULL, NULL, 'N;'),
('Tasks.Default.Create', 0, NULL, NULL, 'N;'),
('Tasks.Default.Delete', 0, NULL, NULL, 'N;'),
('Tasks.Default.Index', 0, NULL, NULL, 'N;'),
('Tasks.Default.Update', 0, NULL, NULL, 'N;'),
('Tasks.Default.View', 0, NULL, NULL, 'N;'),
('User.Default.*', 1, NULL, NULL, 'N;'),
('User.Default.Index', 0, NULL, NULL, 'N;'),
('User.Login.*', 1, NULL, NULL, 'N;'),
('User.Login.Login', 0, NULL, NULL, 'N;'),
('User.Logout.*', 1, NULL, NULL, 'N;'),
('User.Logout.Logout', 0, NULL, NULL, 'N;'),
('User.User.*', 1, NULL, NULL, 'N;'),
('User.User.Index', 0, NULL, NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `AuthItemChild`
--

CREATE TABLE IF NOT EXISTS `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `AuthItemChild`
--

INSERT INTO `AuthItemChild` (`parent`, `child`) VALUES
('Guest', 'Api.Index'),
('junior_manager', 'News.Default.*'),
('manager', 'News.Default.*'),
('middle_manager', 'News.Default.*'),
('Guest', 'Site.*'),
('junior_manager', 'Site.*'),
('Authenticated', 'Site.Error'),
('Guest', 'Site.Error'),
('junior_manager', 'Site.Error'),
('manager', 'Site.Error'),
('middle_manager', 'Site.Error'),
('Authenticated', 'Site.Index'),
('Guest', 'Site.Index'),
('junior_manager', 'Site.Index'),
('manager', 'Site.Index'),
('middle_manager', 'Site.Index'),
('junior_manager', 'Tasks.Default.*'),
('manager', 'Tasks.Default.*'),
('middle_manager', 'Tasks.Default.*'),
('junior_manager', 'Tasks.Default.Create'),
('manager', 'Tasks.Default.Create'),
('middle_manager', 'Tasks.Default.Create'),
('junior_manager', 'Tasks.Default.Delete'),
('manager', 'Tasks.Default.Delete'),
('middle_manager', 'Tasks.Default.Delete'),
('junior_manager', 'Tasks.Default.Index'),
('manager', 'Tasks.Default.Index'),
('middle_manager', 'Tasks.Default.Index'),
('junior_manager', 'Tasks.Default.Update'),
('manager', 'Tasks.Default.Update'),
('middle_manager', 'Tasks.Default.Update'),
('junior_manager', 'Tasks.Default.View'),
('manager', 'Tasks.Default.View'),
('middle_manager', 'Tasks.Default.View'),
('Authenticated', 'User.Default.Index'),
('junior_manager', 'User.Default.Index'),
('manager', 'User.Default.Index'),
('middle_manager', 'User.Default.Index'),
('Guest', 'User.Login.*'),
('Guest', 'User.Login.Login'),
('Authenticated', 'User.Logout.*'),
('junior_manager', 'User.Logout.*'),
('manager', 'User.Logout.*'),
('middle_manager', 'User.Logout.*'),
('Authenticated', 'User.Logout.Logout'),
('junior_manager', 'User.Logout.Logout'),
('manager', 'User.Logout.Logout'),
('middle_manager', 'User.Logout.Logout'),
('Authenticated', 'User.User.Index'),
('junior_manager', 'User.User.Index'),
('manager', 'User.User.Index');

-- --------------------------------------------------------

--
-- Структура таблицы `cms_attachment`
--

CREATE TABLE IF NOT EXISTS `cms_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contentId` int(10) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `extension` varchar(50) NOT NULL,
  `mimeType` varchar(255) NOT NULL,
  `byteSize` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contentId` (`contentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_content`
--

CREATE TABLE IF NOT EXISTS `cms_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nodeId` int(10) unsigned NOT NULL,
  `locale` varchar(50) NOT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `body` longtext,
  `css` longtext,
  `url` varchar(255) DEFAULT NULL,
  `pageTitle` varchar(255) DEFAULT NULL,
  `breadcrumb` varchar(255) DEFAULT NULL,
  `metaTitle` varchar(255) DEFAULT NULL,
  `metaDescription` varchar(255) DEFAULT NULL,
  `metaKeywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contentId_locale` (`nodeId`,`locale`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `cms_content`
--

INSERT INTO `cms_content` (`id`, `nodeId`, `locale`, `heading`, `body`, `css`, `url`, `pageTitle`, `breadcrumb`, `metaTitle`, `metaDescription`, `metaKeywords`) VALUES
(1, 1, 'ru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 'en', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 1, 'il', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cms_link`
--

CREATE TABLE IF NOT EXISTS `cms_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuId` int(10) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_menu`
--

CREATE TABLE IF NOT EXISTS `cms_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `cms_node`
--

CREATE TABLE IF NOT EXISTS `cms_node` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL,
  `parentId` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL DEFAULT 'page',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `name_deleted` (`name`,`deleted`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `cms_node`
--

INSERT INTO `cms_node` (`id`, `created`, `updated`, `parentId`, `name`, `level`, `published`, `deleted`) VALUES
(1, '2014-01-07 18:08:40', NULL, 0, '56', 'page', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `Rights`
--

CREATE TABLE IF NOT EXISTS `Rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Rights`
--

INSERT INTO `Rights` (`itemname`, `type`, `weight`) VALUES
('admin', 2, 0),
('Authenticated', 2, 2),
('Guest', 2, 1),
('junior_manager', 2, 3),
('manager', 2, 4),
('middle_manager', 2, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_migration`
--

CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1365848829),
('m110805_153437_installYiiUser', 1365848836),
('m110810_162301_userTimestampFix', 1365848836),
('m120716_142029_static', 1367197021),
('m120723_102943_staticPagesRegions', 1367197022),
('m120723_112518_pagesPath', 1367197022),
('m120815_111008_staticPageOnMainMenu', 1367197022),
('m120815_135347_staticPageDivide', 1367197022);

-- --------------------------------------------------------

--
-- Структура таблицы `yc_api_keys`
--

CREATE TABLE IF NOT EXISTS `yc_api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Дамп данных таблицы `yc_api_keys`
--

INSERT INTO `yc_api_keys` (`id`, `id_user`, `code`) VALUES
(35, 19, '8aa6184f1d71844948e0827d81f25cfe'),
(36, 8, '1b833504df2a4f3ca0d8ff7b1d24e386'),
(37, 85, '7814ef6c131dbe92a514e731355b7dc9'),
(38, 98, 'f1c504a6b7b152e52f1867855d6eb72a'),
(39, 100, 'ad8e6578f15fa9800e2ff12336090aca'),
(42, 7, 'e2b4826dea5dcde39899a628de909e4c'),
(43, 2, 'b3135ecfcb1dedabdb1482daed8a8c84'),
(44, 1, '7563a12612a424ae7969eb4b37f432f8'),
(45, 82, '8ba833cd9d32c8547f9e0da6e29720f0'),
(46, 63, '497dab56aa6c130f6ab147b6358675e7'),
(47, 99, '3bd69e96cfb794bae7254ee4722621a2'),
(49, 107, 'aa10a312cf484949c27c5f9640a637db'),
(53, 15, '1ecb8fa03f905bcb634a45d2940891c9');

-- --------------------------------------------------------

--
-- Структура таблицы `yc_currency`
--

CREATE TABLE IF NOT EXISTS `yc_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isocode` varchar(3) NOT NULL,
  `rate` decimal(15,4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `isocode` (`isocode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Валюты с курсами относительно RUR' AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `yc_currency`
--

INSERT INTO `yc_currency` (`id`, `isocode`, `rate`) VALUES
(1, 'RUR', '1.0000'),
(2, 'USD', '36.2344'),
(3, 'EUR', '49.9057');

-- --------------------------------------------------------

--
-- Структура таблицы `yc_currency_history`
--

CREATE TABLE IF NOT EXISTS `yc_currency_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate` decimal(15,4) NOT NULL,
  `timeCreate` int(11) NOT NULL,
  `currencyID` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `currencyID` (`currencyID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='История изменения курсов валют относительно рубля' AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `yc_currency_history`
--

INSERT INTO `yc_currency_history` (`id`, `rate`, `timeCreate`, `currencyID`) VALUES
(12, '36.1511', 1387401895, 2),
(13, '49.7908', 1387401895, 3),
(14, '36.2344', 1387445271, 2),
(15, '49.9057', 1387445271, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `yc_image`
--

CREATE TABLE IF NOT EXISTS `yc_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT NULL,
  `managerID` int(11) DEFAULT NULL,
  `image` longtext,
  `is_avaliable` tinyint(1) NOT NULL DEFAULT '0',
  `timeCreate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  KEY `managerID` (`managerID`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=89 ;

--
-- Дамп данных таблицы `yc_image`
--

INSERT INTO `yc_image` (`id`, `userID`, `managerID`, `image`, `is_avaliable`, `timeCreate`) VALUES
(83, 5, NULL, 'Tulips.jpg', 0, 1389117091),
(84, 5, NULL, 'Tulips.jpg', 0, 1389117098),
(85, 5, NULL, 'Desert.jpg', 0, 1389180314),
(86, 5, NULL, '', 0, 1389204552),
(87, 5, NULL, '', 0, 1389204572),
(88, 5, NULL, '', 0, 1389287498);

-- --------------------------------------------------------

--
-- Структура таблицы `yc_news`
--

CREATE TABLE IF NOT EXISTS `yc_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) DEFAULT NULL,
  `managerID` int(11) DEFAULT NULL,
  `is_avaliable` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(1000) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `locale` varchar(50) NOT NULL,
  `video` varchar(500) DEFAULT NULL,
  `timeCreate` int(11) NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  `thumbnail` varchar(500) DEFAULT NULL,
  `viewers` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  KEY `managerID` (`managerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- Дамп данных таблицы `yc_news`
--

INSERT INTO `yc_news` (`id`, `userID`, `managerID`, `is_avaliable`, `title`, `content`, `locale`, `video`, `timeCreate`, `image`, `thumbnail`, `viewers`) VALUES
(38, 20, NULL, 4, 'sadasd', 'asdadasd', 'en', 'a98073f40111a5f309eef6ab7783fa3c.mov', 1389454985, 'a3cf0b9094a5aa9fbdf458ee2d52926a.jpg', '01ea697053a13acad80bd1edc04ede20.jpg', 15);

-- --------------------------------------------------------

--
-- Структура таблицы `yc_news_content`
--

CREATE TABLE IF NOT EXISTS `yc_news_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsID` int(11) DEFAULT NULL,
  `locale` varchar(20) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `content` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yc_news_settings`
--

CREATE TABLE IF NOT EXISTS `yc_news_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `yc_news_settings`
--

INSERT INTO `yc_news_settings` (`id`, `name`, `value`) VALUES
(1, 'Лимит новостей в ленте', 40);

-- --------------------------------------------------------

--
-- Структура таблицы `yc_profiles`
--

CREATE TABLE IF NOT EXISTS `yc_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `patronymic` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `yc_profiles`
--

INSERT INTO `yc_profiles` (`user_id`, `firstname`, `lastname`, `patronymic`) VALUES
(1, 'Administrator', 'admin', 'admin'),
(2, 'manager', 'manager', 'manager'),
(4, 'juniormanager', 'junior_manager', 'juniormanager'),
(5, 'middle', 'middle', 'middle'),
(17, 'user', 'user ', 'user'),
(20, 'test user', 'test user', 'test user');

-- --------------------------------------------------------

--
-- Структура таблицы `yc_profiles_fields`
--

CREATE TABLE IF NOT EXISTS `yc_profiles_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` text,
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` text,
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `yc_profiles_fields`
--

INSERT INTO `yc_profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'firstname', 'Имя', 'VARCHAR', 255, 3, 2, '/^[A-Za-z0-9\\s\\-]+$/u', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'lastname', 'Фамилия', 'VARCHAR', 255, 3, 0, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 2, 3),
(10, 'patronymic', 'Отчество', 'VARCHAR', 255, 3, 0, '/^[A-Za-z0-9\\s\\-]+$/u', '', 'Incorrect Patronymic (length between 3 and 50 characters).', '', '', '', '', 2, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `yc_tasks`
--

CREATE TABLE IF NOT EXISTS `yc_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `managerID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `contentID` int(11) NOT NULL,
  `timeVideo` decimal(15,2) NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `map_image` varchar(500) NOT NULL,
  `is_publish` tinyint(1) NOT NULL DEFAULT '0',
  `timeCreate` int(11) NOT NULL,
  `timeUpdate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_userID` (`userID`),
  KEY `fk_managerID` (`managerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Дамп данных таблицы `yc_tasks`
--

INSERT INTO `yc_tasks` (`id`, `managerID`, `userID`, `title`, `contentID`, `timeVideo`, `price`, `map_image`, `is_publish`, `timeCreate`, `timeUpdate`) VALUES
(38, 1, 20, 'SOme tasks', 0, '2.33', '123.22', '', 2, 1389448302, 0),
(39, 4, NULL, 'New Tasks', 0, '3.22', '2.22', '', 1, 1389449452, 0),
(40, 1, 20, 'asdasd1', 0, '12.23', '2.33', '', 1, 1389453010, 0),
(41, 1, NULL, '456yh', 0, '2.30', '1.23', '6cffbd45b3131bb1c701992fda5b096b.jpg', 1, 1389464817, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `yc_tasks_content`
--

CREATE TABLE IF NOT EXISTS `yc_tasks_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskID` int(11) DEFAULT NULL,
  `locale` varchar(20) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `content` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Дамп данных таблицы `yc_tasks_content`
--

INSERT INTO `yc_tasks_content` (`id`, `taskID`, `locale`, `title`, `content`) VALUES
(30, 38, 'en', 'asdasd', 'asdasd222'),
(31, 38, 'ru', 'фывыфв', 'фывфыв'),
(32, 38, 'il', 'משימות מבחןמשימות מבחןמשימות מבחן', 'משימות מבחןמשימות מבחןמשימות מבחן'),
(33, 39, 'en', 'asdasd', 'asdasd'),
(34, 39, 'ru', 'фывфыв', 'фывфыв'),
(35, 39, 'il', '123456', '132456'),
(36, 40, 'en', '1231qewsdaf', '123ewsdcxz'),
(37, 40, 'ru', 'ыфвуфак1 3куав', 'вфыа 12а'),
(38, 40, 'il', '213456', '634'),
(39, 41, 'en', 'asdasd', 'awdasd'),
(40, 41, 'ru', 'фывфыв', 'фывфыв'),
(41, 41, 'il', 'מטרה כללית על המשימה חדשה', 'מטרה כללית על המשימה חדשה');

-- --------------------------------------------------------

--
-- Структура таблицы `yc_users`
--

CREATE TABLE IF NOT EXISTS `yc_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `paypalemail` varchar(128) DEFAULT NULL,
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username` (`username`),
  UNIQUE KEY `user_email` (`email`),
  UNIQUE KEY `paypalemail` (`paypalemail`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `yc_users`
--

INSERT INTO `yc_users` (`id`, `username`, `password`, `email`, `paypalemail`, `activkey`, `superuser`, `status`, `create_at`, `lastvisit_at`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'phenomanz14@gmail.com', NULL, '9869dc856efc69287a6d17b226a47f3f', 1, 1, '2013-04-13 04:27:16', '2014-01-11 19:08:26'),
(2, 'manager', '1d0258c2440a8d19e716292b231e3190', 'phenomanz13@gmail.com', NULL, '0dd76d493e6732fafb4ecfaa3a8a1718', 0, 1, '2013-12-27 20:49:04', '2014-01-11 18:22:47'),
(4, 'junior_manager', '46528ef484e6319baf7c4ca01923426e', 'junior_manager@test.ru', NULL, '22895f0f06ae54a2b32dbfc028ebc996', 0, 1, '2013-12-30 14:28:25', '2014-01-11 18:06:57'),
(5, 'middle_manager', 'e30fd5b5217f1429b2ef7fb4c4cca4f9', 'm_manager@gmail.ru', NULL, '1314f9aab12907f83666ea28733ccb33', 0, 1, '2013-12-30 15:44:35', '2014-01-11 18:07:18'),
(17, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user@asd.ru', 'test1@asd.ru', '6b93af0bfb2c73994907b4ed201254b8', 0, 1, '2013-12-30 18:27:40', '0000-00-00 00:00:00'),
(20, 'testuser', 'a8f5f167f44f4964e6c998dee827110c', 'testuser@asd.ru', 'testuser@asd.ru', '', 0, 1, '2014-01-11 12:35:09', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `yc_user_payment_history`
--

CREATE TABLE IF NOT EXISTS `yc_user_payment_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `newsID` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `timeCreate` int(11) NOT NULL,
  `bill` decimal(15,2) NOT NULL DEFAULT '0.00',
  `action` tinyint(1) NOT NULL DEFAULT '0',
  `currencyID` int(11) NOT NULL,
  `sysCode` varchar(10) NOT NULL,
  `balance` decimal(15,2) NOT NULL DEFAULT '0.00',
  `purse` varchar(255) DEFAULT NULL,
  `amount` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  KEY `currencyID` (`currencyID`),
  KEY `newsID` (`newsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='История платежей' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `yc_user_state`
--

CREATE TABLE IF NOT EXISTS `yc_user_state` (
  `userID` int(11) NOT NULL,
  `currencyID` int(11) NOT NULL,
  `balance` decimal(15,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`currencyID`),
  KEY `userID` (`userID`),
  KEY `currencyID` (`currencyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yc_user_state`
--

INSERT INTO `yc_user_state` (`userID`, `currencyID`, `balance`) VALUES
(1, 1, '10000.00');

-- --------------------------------------------------------

--
-- Структура таблицы `yc_video`
--

CREATE TABLE IF NOT EXISTS `yc_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `managerID` int(11) NOT NULL,
  `is_avaliable` tinyint(1) NOT NULL DEFAULT '0',
  `timeCreate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`,`managerID`),
  KEY `managerID` (`managerID`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `yc_video`
--

INSERT INTO `yc_video` (`id`, `userID`, `managerID`, `is_avaliable`, `timeCreate`) VALUES
(1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `yc_withdrawal`
--

CREATE TABLE IF NOT EXISTS `yc_withdrawal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `summ` decimal(15,2) NOT NULL,
  `currencyID` int(11) NOT NULL,
  `purse` varchar(20) NOT NULL,
  `managerID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `payed` tinyint(1) NOT NULL DEFAULT '0',
  `comment` varchar(1000) NOT NULL,
  `timeCreate` int(11) NOT NULL,
  `timePay` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `yc_currency_history`
--
ALTER TABLE `yc_currency_history`
  ADD CONSTRAINT `yc_currency_history_ibfk_1` FOREIGN KEY (`currencyID`) REFERENCES `yc_currency` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yc_image`
--
ALTER TABLE `yc_image`
  ADD CONSTRAINT `yc_image_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `yc_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `yc_image_ibfk_2` FOREIGN KEY (`managerID`) REFERENCES `yc_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yc_news`
--
ALTER TABLE `yc_news`
  ADD CONSTRAINT `yc_news_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `yc_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `yc_news_ibfk_5` FOREIGN KEY (`managerID`) REFERENCES `yc_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yc_profiles`
--
ALTER TABLE `yc_profiles`
  ADD CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `yc_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yc_tasks`
--
ALTER TABLE `yc_tasks`
  ADD CONSTRAINT `yc_tasks_ibfk_7` FOREIGN KEY (`managerID`) REFERENCES `yc_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `yc_tasks_ibfk_8` FOREIGN KEY (`userID`) REFERENCES `yc_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yc_user_payment_history`
--
ALTER TABLE `yc_user_payment_history`
  ADD CONSTRAINT `yc_user_payment_history_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `yc_users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `yc_user_payment_history_ibfk_2` FOREIGN KEY (`currencyID`) REFERENCES `yc_currency` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `yc_user_payment_history_ibfk_3` FOREIGN KEY (`newsID`) REFERENCES `yc_news` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yc_user_state`
--
ALTER TABLE `yc_user_state`
  ADD CONSTRAINT `yc_user_state_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `yc_users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `yc_user_state_ibfk_2` FOREIGN KEY (`currencyID`) REFERENCES `yc_currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yc_video`
--
ALTER TABLE `yc_video`
  ADD CONSTRAINT `yc_video_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `yc_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `yc_video_ibfk_2` FOREIGN KEY (`managerID`) REFERENCES `yc_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yc_withdrawal`
--
ALTER TABLE `yc_withdrawal`
  ADD CONSTRAINT `yc_withdrawal_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `yc_users` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
