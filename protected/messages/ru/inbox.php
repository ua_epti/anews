<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 12.01.14
 * Time: 1:43
 */

return array(
	'Inbox' => 'Входящие',
	'Create News' => 'Создать новость',
	'Control News' => 'Управление новостями',
	'Settings News' => 'Настройки новостей',
	'Archive News' => 'Архив новостей',
);