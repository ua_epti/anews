<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 12.01.14
 * Time: 1:44
 */
return array(
	'aNews' => 'аНовости',
	'Control' => 'Управление',
	'Users' => 'Пользователи',
	'Logout' => 'Выход',
	'Group fields' => 'Группы полей',
	'Permissions' => 'Права доступа',
	'Welcome to the admin panel system {site_name}' => 'Добро пожаловать в админ панель системы {site_name}',
	'Strip' => 'Лента',
);