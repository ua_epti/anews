<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 12.01.14
 * Time: 1:43
 */

return array(
	'{value} Must be not empty!' => '{value} Должны быть не пусты',
	'{value} Must be integer!' => '{value} Должно быть целым числом!',
	'Must be not empty!' => 'Должны быть не пусты!',
	'Specify the required parameters for the token' => 'Укажите необходимый параметр для получения токена',
	'You did not indicate a problem  c=' => 'Вы не указали задачу с=',
	'Password can not be the same as your username!' => 'Пароль не может совпадать с вашим логином!',
	'User with this email already exists!' => 'Пользователь с таким email уже существует!',
	'Respected, {username},\nYou will generate a new password:!\nPassword: {password}")' => 'Уважаемый, {username},\nВам сгенерирован новый пароль:!\Пароль: {password}")',
	'New password' => 'Новый пароль',
	'You misspelled username and password' => 'Вы неправильно указали логин или пароль',
	'This Token not registered in the System' => 'Этот токен не зарегистрирован в системе',
	'This user is not registered in the system' => 'Такой юзер не зарегестрирован в системе',
	'Token is empty!' => 'Ключ отсутствует',
	'' => '',
);