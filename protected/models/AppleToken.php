<?php

/**
 * This is the model class for table "{{apple_token}}".
 *
 * The followings are the available columns in table '{{apple_token}}':
 * @property integer $id
 * @property integer $userID
 * @property string $token
 * @property integer $timeCreate
 * @property integer $timeUpdate
 */
class AppleToken extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{apple_token}}';
	}

	public function rules()
	{
		return array(
			array('token, timeCreate, timeUpdate', 'required'),
			array('userID, timeCreate, timeUpdate', 'numerical', 'integerOnly' => true),
			array('token', 'length', 'max' => 255),
			array('id, userID, token, timeCreate, timeUpdate', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array();
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userID' => 'User',
			'token' => 'Token',
			'timeCreate' => 'Time Create',
			'timeUpdate' => 'Time Update',
		);
	}

	/*public function checkAppleToken($appleToken, $userID = null, $userToken = null)
	{

	}*/

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('token', $this->token, true);
		$criteria->compare('timeCreate', $this->timeCreate);
		$criteria->compare('timeUpdate', $this->timeUpdate);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}