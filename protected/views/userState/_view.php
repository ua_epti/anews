<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('currencyID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->currencyID),array('view','id'=>$data->currencyID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userID')); ?>:</b>
	<?php echo CHtml::encode($data->userID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('balance')); ?>:</b>
	<?php echo CHtml::encode($data->balance); ?>
	<br />


</div>