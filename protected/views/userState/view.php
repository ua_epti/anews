<?php
$this->breadcrumbs=array(
	'User States'=>array('index'),
	$model->currencyID,
);

$this->menu=array(
array('label'=>'List UserState','url'=>array('index')),
array('label'=>'Create UserState','url'=>array('create')),
array('label'=>'Update UserState','url'=>array('update','id'=>$model->currencyID)),
array('label'=>'Delete UserState','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->currencyID),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage UserState','url'=>array('admin')),
);
?>

<h1>View UserState #<?php echo $model->currencyID; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'userID',
		'currencyID',
		'balance',
),
)); ?>
