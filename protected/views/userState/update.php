<?php
$this->breadcrumbs=array(
	'User States'=>array('index'),
	$model->currencyID=>array('view','id'=>$model->currencyID),
	'Update',
);

	$this->menu=array(
	array('label'=>'List UserState','url'=>array('index')),
	array('label'=>'Create UserState','url'=>array('create')),
	array('label'=>'View UserState','url'=>array('view','id'=>$model->currencyID)),
	array('label'=>'Manage UserState','url'=>array('admin')),
	);
	?>

	<h1>Update UserState <?php echo $model->currencyID; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>