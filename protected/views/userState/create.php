<?php
$this->breadcrumbs=array(
	'User States'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List UserState','url'=>array('index')),
array('label'=>'Manage UserState','url'=>array('admin')),
);
?>

<h1>Create UserState</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>