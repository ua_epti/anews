<?php
$this->breadcrumbs=array(
	'User States',
);

$this->menu=array(
array('label'=>'Create UserState','url'=>array('create')),
array('label'=>'Manage UserState','url'=>array('admin')),
);
?>

<h1>User States</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
