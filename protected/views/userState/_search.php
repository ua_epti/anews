<?php
/**
* @var $form TbActiveForm
* @var $model UserState
*/
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'userID',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'currencyID',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'balance',array('class'=>'span5','maxlength'=>15)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
