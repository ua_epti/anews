<?php
/**
* @var $form TbActiveForm
* @var $model Content
*/
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'newsID',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'is_avaliable',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'locale',array('class'=>'span5','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>1000)); ?>

		<?php echo $form->textFieldRow($model,'content',array('class'=>'span5','maxlength'=>10000)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
