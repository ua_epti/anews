<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('newsID')); ?>:</b>
	<?php echo CHtml::encode($data->newsID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_avaliable')); ?>:</b>
	<?php echo CHtml::encode($data->is_avaliable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('locale')); ?>:</b>
	<?php echo CHtml::encode($data->locale); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />


</div>