<? $this->beginContent('//layouts/main'); ?>
	<div class="span9">
		<div id="content">
			<?= $content; ?>
		</div>
	</div>
	<div class="span3">
		<div id="sidebar">
			<?$this->beginWidget('zii.widgets.CPortlet', array('title' => 'Действия',));
			$this->widget('zii.widgets.CMenu', array(
				'items' => $this->menu,
				'htmlOptions' => array('class' => 'operations'),
			));
			$this->endWidget();
			?>
		</div>
	</div>
<? $this->endContent(); ?>