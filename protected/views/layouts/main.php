<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta name="language" content="en"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/"/>
	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if IE]>
	<script src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<? yii()->bootstrap->registerCoreCss(); ?>
	<? yii()->bootstrap->registerCoreScripts(); ?>
	<? yii()->bootstrap->registerResponsiveCss(); ?>
	<script type="text/javascript" src="<?= baseUrl() . '/js/jquery-1.8.2.min.js' ?>"></script>
	<link type="text/css" href="<?= baseUrl() . '/css/style.css' ?>" rel="stylesheet" media="all"/>
	<link type="text/css" href="<?= baseUrl() . '/css/zoom.css' ?>" rel="stylesheet" media="all"/>
	<script type="text/javascript" src="<?= baseUrl() . '/js/main.js' ?>"></script>
	<script type="text/javascript" src="<?= baseUrl() . '/js/zoom-c.js' ?>"></script>
	<script type="text/javascript">baseUrl = "<?=baseUrl();?>";</script>
	<title><?= CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div id="header">
	<div class="span12" style="width:100%;margin:0px;">
		<? if (user()->isAdmin()): ?>
			<? $this->widget('ext.adminMenu.AdminMenuWidget') ?>
		<? endif; ?>
		<? if (!user()->isGuest && user()->isManager(user()->id)) : ?>
			<? $this->widget('ext.managerMenu.managerMenuWidget') ?>
		<? endif; ?>
	</div>
</div>
<? $this->widget('bootstrap.widgets.TbAlert') ?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<?= $content ?>
		</div>
	</div>
</div>
<div class="clear"></div>
</body>
</html>
