<? $this->beginContent('//layouts/main') ?>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="span2">
			</div>
			<div class="span8">
				<? if (!user()->isGuest): ?>
					<? $this->widget('ext.tabsMenuNews.TabsMenuNewsWidget'); ?>
				<? endif ?>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span2">
			<? if (!user()->isGuest): ?>
				<div id="SideBarMenu">
					<? $this->widget('ext.sideBarMenu.SideBarMenuWidget') ?>
				</div>
			<? endif ?>
		</div>
		<div class="span10">
			<?= $content ?>
		</div>
	</div>
</div>
<? $this->endContent(); ?>
