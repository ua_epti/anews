<?php
$this->breadcrumbs=array(
	'User Payment Histories'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List UserPaymentHistory','url'=>array('index')),
array('label'=>'Create UserPaymentHistory','url'=>array('create')),
array('label'=>'Update UserPaymentHistory','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete UserPaymentHistory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage UserPaymentHistory','url'=>array('admin')),
);
?>

<h1>View UserPaymentHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'userID',
		'newsID',
		'comment',
		'timeCreate',
		'bill',
		'action',
		'currencyID',
		'sysCode',
		'balance',
		'purse',
		'amount',
),
)); ?>
