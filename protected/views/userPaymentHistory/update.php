<?php
$this->breadcrumbs=array(
	'User Payment Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List UserPaymentHistory','url'=>array('index')),
	array('label'=>'Create UserPaymentHistory','url'=>array('create')),
	array('label'=>'View UserPaymentHistory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage UserPaymentHistory','url'=>array('admin')),
	);
	?>

	<h1>Update UserPaymentHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>