<?php
$this->breadcrumbs=array(
	'User Payment Histories'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List UserPaymentHistory','url'=>array('index')),
array('label'=>'Manage UserPaymentHistory','url'=>array('admin')),
);
?>

<h1>Create UserPaymentHistory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>