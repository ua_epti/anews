<?php
$this->breadcrumbs=array(
	'User Payment Histories',
);

$this->menu=array(
array('label'=>'Create UserPaymentHistory','url'=>array('create')),
array('label'=>'Manage UserPaymentHistory','url'=>array('admin')),
);
?>

<h1>User Payment Histories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
