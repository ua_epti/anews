<?php
/**
* @var $form TbActiveForm
* @var $model UserPaymentHistory
*/
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-payment-history-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'userID',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'newsID',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'comment',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'timeCreate',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'bill',array('class'=>'span5','maxlength'=>15)); ?>

	<?php echo $form->textFieldRow($model,'action',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'currencyID',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sysCode',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'balance',array('class'=>'span5','maxlength'=>15)); ?>

	<?php echo $form->textFieldRow($model,'purse',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'amount',array('class'=>'span5','maxlength'=>15)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
