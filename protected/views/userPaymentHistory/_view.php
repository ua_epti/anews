<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userID')); ?>:</b>
	<?php echo CHtml::encode($data->userID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('newsID')); ?>:</b>
	<?php echo CHtml::encode($data->newsID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeCreate')); ?>:</b>
	<?php echo CHtml::encode($data->timeCreate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bill')); ?>:</b>
	<?php echo CHtml::encode($data->bill); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action')); ?>:</b>
	<?php echo CHtml::encode($data->action); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('currencyID')); ?>:</b>
	<?php echo CHtml::encode($data->currencyID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sysCode')); ?>:</b>
	<?php echo CHtml::encode($data->sysCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('balance')); ?>:</b>
	<?php echo CHtml::encode($data->balance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('purse')); ?>:</b>
	<?php echo CHtml::encode($data->purse); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amount')); ?>:</b>
	<?php echo CHtml::encode($data->amount); ?>
	<br />

	*/ ?>

</div>