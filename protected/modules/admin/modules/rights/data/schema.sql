DROP TABLE IF EXISTS AuthItem;
DROP TABLE IF EXISTS AuthItemChild;
DROP TABLE IF EXISTS AuthAssignment;
DROP TABLE IF EXISTS Rights;

CREATE TABLE AuthItem
(
  name        VARCHAR(64) NOT NULL,
  type        INTEGER     NOT NULL,
  description TEXT,
  bizrule     TEXT,
  data        TEXT,
  PRIMARY KEY (name)
);

CREATE TABLE AuthItemChild
(
  parent VARCHAR(64) NOT NULL,
  child  VARCHAR(64) NOT NULL,
  PRIMARY KEY (parent, child),
  FOREIGN KEY (parent) REFERENCES AuthItem (name)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (child) REFERENCES AuthItem (name)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE AuthAssignment
(
  itemname VARCHAR(64) NOT NULL,
  userid   VARCHAR(64) NOT NULL,
  bizrule  TEXT,
  data     TEXT,
  PRIMARY KEY (itemname, userid),
  FOREIGN KEY (itemname) REFERENCES AuthItem (name)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE Rights
(
  itemname VARCHAR(64) NOT NULL,
  type     INTEGER     NOT NULL,
  weight   INTEGER     NOT NULL,
  PRIMARY KEY (itemname),
  FOREIGN KEY (itemname) REFERENCES AuthItem (name)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);