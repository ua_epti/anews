<?if (user()->getFlashes(false)) {
	echo $this->widget('bootstrap.widgets.TbAlert', array(
			'block' => true, // display a larger alert block?
			'fade' => true, // use transitions?
			'closeText' => '&times;', // close link text - if set to false, no close link is displayed
		),
		true
	);
}
Yii::app()->clientScript->registerScript(
	'alertHideEffect',
	'$(".alert.in").animate({opacity: 1.0}, 3000).fadeOut("slow");',
	CClientScript::POS_READY
);