<?php $this->beginContent(Rights::module()->appLayout); ?>
	<div class="row-fluid">
		<div class="span12">
			<div class="span2">
			</div>
			<div class="span8">
				<? if (!user()->isGuest): ?>
					<? $this->widget('ext.tabsMenuUser.TabsMenuUserWidget'); ?>
				<? endif ?>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<div id="rights" class="offset2">
			<div id="content">
				<? if ($this->id !== 'install'): ?>
					<div id="menu">
						<?php $this->renderPartial('/_menu'); ?>
					</div>
				<? endif; ?>
				<? $this->renderPartial('/_flash'); ?>
				<?= $content; ?>
			</div>
		</div>
	</div>
<?php $this->endContent(); ?>