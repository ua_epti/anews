<?
class DefaultController extends Controller
{
	public $layout = '//layouts/tasks';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionIndex()
	{
		$model = new Tasks('search');
		$model->unsetAttributes();
		if (isset($_GET['Tasks'])) {
			$model->attributes = $_GET['Tasks'];
		}
		$this->render('admin', array('model' => $model));
	}

	public function actionStatus()
	{
		if (yii()->request->isAjaxRequest) {
			if (isset($_POST['Tasks']['arID'])) {
				$this->processMassAction($_POST['Tasks']['action'], $_POST['Tasks']['arID']);
			} elseif (!isset($_POST['Tasks']['arID']) && $_POST['Tasks']['action'] == 'statusPaid') {
				echo 'error';
				yii()->end();
			}
		}
	}


	private function processMassAction($action, $arID)
	{
		switch ($action) {
			case 'statusArchive':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_publish = Tasks::CLOSE;
						$actModel->save(false);
					}
				}
				break;
			case 'statusDelete':
				foreach ($arID as $id) {
					if ($id != '') {
						$this->loadModel($id)->delete();
					}
				}
				break;
		}
	}

	public function actionArchive()
	{
		$model = new Tasks('archive');
		$model->unsetAttributes();
		if (isset($_GET['Tasks'])) {
			$model->attributes = $_GET['Tasks'];
		}
		$this->render('archive', array('model' => $model));
	}

	public function actionView($id)
	{
		$this->render('view', array('model' => $this->loadModel($id)));
	}

	public function actionAjaxView($id)
	{
		if (yii()->request->isAjaxRequest) {
			$model = Tasks::model()->findAllByPk($id);
			$model = new CArrayDataProvider($model);
			$this->renderPartial('ajaxview', array(
				'dataProvider' => $model
			), false, true);
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	public function actionCreate()
	{
		$model = new Tasks('create');
		$model->title = uniqid('uniqID-');
		if ($model->save()) {
			user()->setFlash('success', Yii::t('site', 'Tasks Create.'));
			$this->redirect(array('update', 'id' => $model->id));
		} elseif (YII_DEBUG) {
			dump($model->getErrors());
			die;
		}
	}

	public function actionUpdate($id)
	{
		if (!empty($id)) {
			$model = $this->loadModel($id);
			$model->scenario = 'task';
			$translations = array();

			foreach (param('languages') as $language) {
				$content = $model->getContent($language);

				if ($content === null) {
					$content = $model->createContent($language);
				}
				$translations[$language] = $content;
			}


			$this->performAjaxValidation($model);
			if (isset($_POST['Tasks']) && isset($_POST['TasksContent'])) {

				foreach ($translations as $language => $content) {
					$content->attributes = $_POST['TasksContent'][$language];
					$content->taskID = $id;
					$valid = $content->validate($model);
					$translations[$language] = $content;
				}

				if (!empty($_POST['src'])) {
					$fileName = $this->processFile($model);
				}
				if ($valid) {
					$model->attributes = $_POST['Tasks'];
					$model->map_image = $fileName;
					if ($model->save()) {

					} elseif (YII_DEBUG) {
						dump($model->getErrors());
						die;
					}

					foreach ($translations as $content) {
						$content->save();
					}
					user()->setFlash('success', Yii::t('site', 'Tasks updated.'));
					$this->redirect(array('update', 'id' => $id));
				} elseif (YII_DEBUG) {
					dump($content->getErrors());
					die;
				}
			}
			$this->render('update', array(
				'model' => $model,
				'translations' => $translations,
			));
		}
	}


	private function processFile($model)
	{
		/*$targ_w = $targ_h = 1224;
		$jpeg_quality = 100;

		$str = str_replace(' ', '', $_POST['src']);
		$img_r = imagecreatefromjpeg($str);
		$dst_r = ImageCreateTrueColor($targ_w, $targ_h);

		imagecopyresampled($dst_r, $img_r, 0, 0, 0, 0,
			$targ_w, $targ_h, 1224, 1224);

		header('Content-type: image/jpeg');
		imagejpeg($dst_r, null, $jpeg_quality);*/


		$targ_w = yii()->params['files']['thumbnailMapWidth'];
		$targ_h = yii()->params['files']['thumbnailMapHeight'];

		$jpeg_quality = 100;
		$str = str_replace(' ', '', $_POST['src']);
		$img_r = imagecreatefromjpeg($str);
		$dst_r = ImageCreateTrueColor($targ_w, $targ_h);

		imagecopyresampled($dst_r, $img_r, 0, 0, 0, 0, $targ_w, $targ_h, 1224, 1224);
		$fileName = md5($model->id) . '.jpg';
		imagejpeg($dst_r, 'images/news/' . $fileName, $jpeg_quality);
		$fileName = yii()->storage->addFile('images/news/' . $fileName, $fileName);
		return $fileName;

	}

	public function actionFastAjaxUpdate()
	{
		Yii::import('editable.*');
		$es = new EditableSaver('Tasks');
		$es->update();
	}

	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel($id)->delete();
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	public function loadModel($id)
	{
		$model = Tasks::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'tasks-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}