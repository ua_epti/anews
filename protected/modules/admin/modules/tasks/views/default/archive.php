<?registerScript('viewAction', '
$("#tasks-grid table tbody tr td").bind("click", function () {
		if(!$(this).hasClass("checkBoxTD")){
			var id = $(this).parent().find("td:last").text();
			window.location.href = baseUrl + "/admin/tasks/default/view/id/"+id;
		}
	});
');?>

<?registerScript('massAction', "
	function processSelected() {
		var arElemID = new Array();
		$('input[name^=tasks-grid_c0]:checked').each(function(i){
			var id = $(this).val();
			if(id != 1){
				arElemID[i] = $(this).val();
			}
		});
		return arElemID;
	}")?>

	<div class="form-action">
		<?=
		CHtml::ajaxLink(
			'Удалить',
			createUrl("//admin/tasks/default/status"),
			array(
				'beforeSend' => 'function(){
					var arID = processSelected();
					if(arID == ""){
						alert("Выбирите новости для одобрения");
						return false;
					}
				}',
				'type' => 'POST',
				'async' => 'false',
				'data' => array(
					'Tasks' => array(
						'arID' => 'js:processSelected()',
						'action' => 'statusDelete'
					),
				),
				'success' => 'function(data){
			$.fn.yiiGridView.update("tasks-grid", {
				data: $(this).serialize()
			});
		}'
			),
			array('class' => 'btn btn-danger')) ?>
	</div>
<?$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'tasks-grid',
	'itemsCssClass' => 'striped',
	'dataProvider' => $model->archive(),
	'selectableRows' => 2,
	'columns' => array(
		array(
			'class' => 'CCheckBoxColumn',
			'htmlOptions' => array(
				'width' => '5%',
				'class' => 'checkBoxTD'
			),
		),
		array(
			'name' => 'userID',
			'type' => 'raw',
//			'value' => 'User::model()->getUserNameByPk($data->userID)',
		),
		array(
			'name' => 'managerID',
			'type' => 'raw',
//			'value' => 'User::model()->getUserNameByPk($data->managerID)',
		),
		array(
			'class' => 'editable.EditableColumn',
			'name' => 'is_publish',
			'editable' => array(
				'type' => 'select',
				'source' => Tasks::$STATUSES,
				'url' => $this->createUrl('default/FastAjaxUpdate'),
				'placement' => 'left',
			),
			'htmlOptions' => array(
				'class' => 'checkBoxTD'
			),
		),
		array(
			'name' => 'timeCreate',
			'header' => 'Дата создания',
			'type' => 'raw',
			'value' => 'yii()->dateFormatter->formatDateTime("$data->timeCreate")'
		),
		array(
			'name' => 'id',
			'header' => '',
			'type' => 'raw',
			'value' => '$data->id',
			'htmlOptions' => array('style' => 'display:none;'),
			'headerHtmlOptions' => array('size' => 0, 'style' => 'display:none;'),
		),
	),
));
?>