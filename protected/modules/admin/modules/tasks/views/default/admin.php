<?registerScript('viewAction', '
$("#tasks-grid table tbody tr td").live("click", function () {
		if(!$(this).hasClass("checkBoxTD")){
			var id = $(this).parent().find("td:last").text();
			window.location.href = baseUrl + "/tasks/default/view/id/"+id;
		}
	});
')?>
<?registerScript('massAction', "
	function processSelected() {
		var arElemID = new Array();
		$('input[name^=tasks-grid_c0]:checked').each(function(i){
			var id = $(this).val();
			if(id != 1){
				arElemID[i] = $(this).val();
			}
		});
		return arElemID;
	}")?>
	<div class="form-action">
		<?= CHtml::link('Создать', createUrl('//admin/tasks/default/create'), array('class' => 'btn btn-success')) ?>
		<?=
		CHtml::ajaxLink(
			'В Архив',
			createUrl("//admin/tasks/default/status"),
			array(
				'type' => 'POST',
				'async' => 'false',
				'data' => array(
					'Tasks' => array(
						'arID' => 'js:processSelected()',
						'action' => 'statusArchive'
					),
				),
				'success' => 'function(data){
			$.fn.yiiGridView.update("tasks-grid", {
				data: $(this).serialize()
			});
		}'
			),
			array('class' => 'btn btn-info')) ?>
	</div>

<?$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'tasks-grid',
	'itemsCssClass' => 'striped',
	'dataProvider' => $model->search(),
	'selectableRows' => 2,
	'columns' => array(
		array(
			'class' => 'CCheckBoxColumn',
			'htmlOptions' => array(
				'width' => '2%',
				'class' => 'checkBoxTD'
			),
		),
		array(
			'name' => 'id',
			'htmlOptions' => array('width' => '5%'),
		),
		/*array(
			'name' => 'title',
			'type' => 'raw',
//			'value' => '$data->getTitleByLocale("ru")',
			'htmlOptions' => array('width' => '10%'),
		),*/
		/*array(
			'header' => 'Описание',
			'name' => 'content',
			'type' => 'raw',
//			'value' => '$data->getContent("ru")',
			'htmlOptions' => array('width' => '30%'),
		),*/
		array(
			'name' => 'map_image',
			'htmlOptions' => array('width' => '20%'),
		),
		/*array(
			'name' => 'timeCreate',
			'header' => 'Дата создания',
			'type' => 'raw',
			'value' => 'yii()->dateFormatter->formatDateTime("$data->timeCreate")',
			'htmlOptions' => array('width' => '10%'),
		),*/
		array(
			'name' => 'id',
			'header' => '',
			'type' => 'raw',
			'value' => '$data->id',
			'htmlOptions' => array('style' => 'display:none;'),
			'headerHtmlOptions' => array('size' => 0, 'style' => 'display:none;'),
		),
	),
));