<h1>Update Tasks <?= $model->id; ?></h1>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'tasks-form',
	'enableAjaxValidation' => true,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
<?php $tabs = array();
foreach ($translations as $locale => $content) {
	$language = Yii::app()->cms->languages[$locale];
	$tab = array(
		'content' => $this->renderPartial('_form', array(
				'model' => $content,
				'form' => $form,
				'node' => $model,
				'language' => $language,
			), true),
	);
	$tabs[$language] = $tab;
} ?>

<?php $this->widget('zii.widgets.jui.CJuiTabs', array(
	'headerTemplate' => '<li><a href="{url}">{title}</a></li>',
	'tabs' => $tabs,
	'htmlOptions' => array('style' => 'padding: 20px')
)); ?>
<div class="clear"></div>
<?= $form->textFieldRow($model, 'map_address', array('id' => 'address')) ?>
<? $this->widget("ext.GMapCrop.GMapCropWidget",array('address'=>$model->map_address)) ?>
<?= $form->textFieldRow($model, 'timeVideo', array('class' => 'span5')); ?>
<?= $form->textFieldRow($model, 'price', array('class' => 'span5', 'maxlength' => 15)); ?>
<div class="form-actions">
	<? $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save',
		'htmlOptions' => array('onSubmit' => 'return checkCoords();')
	)); ?>
</div>
<input type="hidden" id="map_url" name="src"/>
<input type="hidden" id="x" name="x"/>
<input type="hidden" id="y" name="y"/>
<input type="hidden" id="w" name="w"/>
<input type="hidden" id="h" name="h"/>
<? $this->endWidget(); ?>
