<h1>Просмотр задания #<?= $model->id; ?></h1>
<?= CHtml::link('Update Tasks', createUrl('//admin/tasks/default/update/', array('id' => $model->id)), array('class' => 'btn btn-info')) ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'managerID',
		'userID',
		'title',
		'timeVideo',
		'price',
		'is_publish',
		'timeCreate',
		'timeUpdate',
	),
)); ?>
