<fieldset class="form-content">
	<div class="row">
		<?php echo $form->labelEx($model, 'title') ?>
		<?= $form->textField($model, '[' . $model->locale . ']title', array('class' => 'cms_title span10')) ?>
		<?php echo $form->error($model, '[' . $model->locale . ']title') ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, '[' . $model->locale . ']content') ?>
		<?= $form->textArea($model, '[' . $model->locale . ']content', array('id' => 'content-' . $model->locale, 'class' => 'cms_content span10')) ?>
		<?php echo $form->error($model, '[' . $model->locale . ']content') ?>
	</div>
</fieldset>