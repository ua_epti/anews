<div class="view">
	<b><?php echo CHtml::encode($data->getAttributeLabel('managerID')); ?>:</b>
	<?php echo CHtml::encode($data->managerID); ?>
	<br/>

	<b><?php echo CHtml::encode($data->getAttributeLabel('userID')); ?>:</b>
	<?php echo CHtml::encode($data->userID); ?>
	<br/>

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br/>

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeVideo')); ?>:</b>
	<?php echo CHtml::encode($data->timeVideo); ?>
	<br/>

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br/>

</div>