<div class="tasks-create form">
	<h1><?php echo Yii::t('site', 'Create tasks') ?></h1>
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'Tasks',
	)) ?>
	<div class="row">
		<?php echo $form->label($model, 'title') ?>
		<?php echo $form->textField($model, 'title') ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Create') ?>
	</div>
	<?php $this->endWidget() ?>
</div>