<?php

class NewsModule extends CWebModule
{
	public $defaultController = "Default";

	public function init()
	{
		$this->setImport(array(
			'application.modules.news.models.*',
			'application.modules.tasks.models.*',
			'application.modules.paypal.components.*',
			'application.modules.payment.models.*',
			'news.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}
}
