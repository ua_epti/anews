<?/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 27.01.14
 * Time: 23:16
 * @var $dataProvider News
 */
?>
<?$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'news_grid',
	'itemsCssClass' => 'striped',
	'dataProvider' => $dataProvider,
	'selectableRows' => 2,
	'rowCssClassExpression' => '$data->taskID!=null?"by_task":"no_task"',
	'columns' => array(
		array(
			'class' => 'CCheckBoxColumn',
//			'selectableRows' => false,
			'htmlOptions' => array(
				'width' => '2%',
				'class' => 'checkBoxTD'
			),
		),
		array(
			'name' => 'id',
			'header' => '',
			'type' => 'raw',
			'value' => '$data->taskID==null?CHtml::image(createUrl("/images/no_task.png")):CHtml::image(createUrl("/images/by_task.png"))',
			'sortable' => false,
			'htmlOptions' => array(
				'width' => '3%',
				'class' => 'checkBoxTD'
			),

		),
		array(
			'header' => 'Заголовок',
			'name' => 'title',
			'type' => 'raw',
			'htmlOptions' => array(
				'width' => '11%',
			),
		),
		array(
			'name' => 'content',
			'type' => 'raw',
			'htmlOptions' => array(),
		),
		array(
			'id' => 'id',
			'name' => 'id',
//			'type' => 'raw',
//			'htmlOptions' => array(),
		),
		array(
			'header' => 'Место',
			'type' => 'raw',
			'value' => '$data->address',
			'htmlOptions' => array(),
		),
		array(
			'name' => 'timeCreate',
			'header' => 'Дата',
			'type' => 'raw',
			'value' => 'yii()->dateFormatter->formatDateTime("$data->timeCreate")',
			'htmlOptions' => array(
				'width' => '10%',
			),
		),
		array(
			'name' => 'userID',
			'type' => 'raw',
			'value' => '$data->author!=null?$data->author->email:""',
			'htmlOptions' => array(
				'width' => '5%',
			),
		),
		array(
			'name' => 'thumbnail',
			'header' => 'Файл',
			'type' => 'raw',
			'value' => '
				$data->thumbnail==""?
				($data->video==""?"":yii()->getController()->widget("ext.videoWidget.VideoWidget", array("videoUrl" =>$data->video), true)):
				CHtml::tag("a",array("href" => createUrl(yii()->storage->getFileUrl("$data->image")),
						"class" => "zoom","style" => "padding:0 22%",),
							CHtml::image(createUrl(yii()->storage->getFileUrl("$data->thumbnail")),"",array("width"=>"150px")),
				true)',
			'htmlOptions' => array(
				'width' => '10%',
				'class' => 'checkBoxTD',
			)
		),
		array(
			'name' => 'id',
			'header' => '',
			'type' => 'raw',
			'value' => '$data->id',
			'htmlOptions' => array('style' => 'display:none;'),
			'headerHtmlOptions' => array('size' => 0, 'style' => 'display:none;'),
		),
	),
))?>
