<?registerScript('viewAction', '
$("#news_grid table tbody tr td").live("click", function () {
		if(!$(this).hasClass("checkBoxTD")){
			var id = $(this).parent().find("td:last").text();
			window.location.href = baseUrl + "/admin/news/default/view/id/"+id;
		}
	});
')?>
<?registerScript('massAction', "
	function processSelected() {
		var arElemID = new Array();
		$('input[name^=news_grid_c0]:checked').each(function(i){
			var id = $(this).val();
			if(id != 1){
				arElemID[i] = $(this).val();
			}
		});
		return arElemID;
	}")?>

<div class="form-action">
<!--	--><?//= CHtml::link('Создать', createUrl('//admin/news/default/create'), array('class' => 'btn btn-success')) ?>
	<?=
	CHtml::ajaxLink(
		'На 1 уровень',
		createUrl("//admin/news/default/index"),
		array(
			'beforeSend' => 'function(){
				var arID = processSelected();
				if(arID == ""){
					alert("Выбирите новости для одобрения");
					return false;
				}
			}',
			'type' => 'POST',
			'async' => 'false',
			'data' => array(
				'News' => array(
					'arID' => 'js:processSelected()',
					'action' => 'statusFirstLvl'
				),
			),
			'success' => 'function(data){
			$.fn.yiiGridView.update("news_grid", {
				data: $(this).serialize()
			});
			$("#SideBarMenu").html(data);
		}'
		),
		array('class' => 'btn btn-info')) ?>
	<?=
	CHtml::ajaxLink(
		'Оплатить',
		createUrl('//admin/news/default/index'),
		array(
			'beforeSend' => 'function(){
					var selectCount = processSelected();
					var count = 0;
					selectCount.forEach(function(entry) {
						console.log(entry);
						count++;
					});
					if(count != 1){
						alert("Выбирите одну запись.");
						return false;
					}
				}',
			'type' => 'POST',
			'data' => array(
				'News' => array(
					'arID' => 'js:processSelected()',
					'action' => 'statusPaid'
				),
			),
			'success' => 'function(data){
			if(data =="error"){
				alert("Выбирите 1 новость для оплаты")
			}else{
				$("#news_grid").replaceWith(data);
			}
		}'
		),
		array('class' => 'btn btn-info'))?>
	<?=
	CHtml::ajaxLink(
		'Готов к оплате',
		createUrl('//admin/news/default/index'),
		array(
			'type' => 'POST',
			'async' => false,
			'data' => array(
				'News' => array(
					'arID' => 'js:processSelected()',
					'action' => 'statusBeforePaid'
				),
			),
			'success' => 'function(data){
					$.fn.yiiGridView.update("news_grid", {
						data: $(this).serialize()
					});
					$("#SideBarMenu").html(data);
				}'
		),
		array('class' => 'btn btn-info'))?>
	<?=
	CHtml::ajaxLink(
		'Скачать',
		createUrl('//admin/news/default/index'),
		array(
			'type' => 'POST',
			'dataType' => 'json',
			'data' => array(
				'News' => array(
					'arID' => 'js:processSelected()',
					'action' => 'download'
				),
			),
			'success' => 'function(data){
					if(data.code == 200){
						window.location.href = data.url;
						setTimeout(function(){
							$.ajax({
								url:baseUrl+"/news/default/ArchiveDelete",
								type: "POST",
								data:{
									"archiveName":data.url
								},
								success:function(data){
									console.log(data);
								}
							});
						},3000)
					}
					if(data.code == 400){
						alert("systemError, plz try again later");
					}
				}'
		),
		array('class' => 'btn btn-info'))?>
	<?=
	CHtml::ajaxLink(
		'В Архив',
		createUrl('//admin/news/default/index'),
		array(
			'beforeSend' => 'function(){
			var arID = processSelected();
			if(arID==""){
				alert("Выбирите новости для перемещения в архив");
				return false;
			}
		}',
			'type' => 'POST',
			'async' => 'false',
			'data' => array(
				'News' => array(
					'arID' => 'js:processSelected()',
					'action' => 'statusArchive'
				),
			),
			'success' => 'function(data){
			$.fn.yiiGridView.update("news_grid", {
				data: $(this).serialize()
			});
			$("#SideBarMenu").html(data);
		}'
		),
		array('class' => 'btn btn-danger'))?>
</div>
<? $this->renderPartial('grid', array('dataProvider' => $dataProvider)) ?>
