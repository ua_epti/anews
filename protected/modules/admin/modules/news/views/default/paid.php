<?registerScript('viewAction', '
$("#news_grid table tbody tr td").live("click", function () {
		if(!$(this).hasClass("checkBoxTD")){
			var id = $(this).parent().find("td:last").text();
			window.location.href = baseUrl + "/admin/news/default/view/id/"+id;
		}
	});
')?>
<?registerScript('massAction', "
	function processSelected() {
		var arElemID = new Array();
		$('input[name^=news_grid_c0]:checked').each(function(i){
			var id = $(this).val();
			if(id != 1){
				arElemID[i] = $(this).val();
			}
		});
		return arElemID;
	}")?>
	<div class="form-action">
		<?=
		CHtml::ajaxLink(
			'В Ленту',
			createUrl("//admin/news/default/index"),
			array(
				'type' => 'POST',
				'async' => 'false',
				'data' => array(
					'News' => array(
						'arID' => 'js:processSelected()',
						'action' => 'strip'
					),
				),
				'success' => 'function(data){
			$.fn.yiiGridView.update("news_grid", {
				data: $(this).serialize()
			});
			$("#SideBarMenu").html(data);
		}'
			),
			array('class' => 'btn btn-danger')) ?>
		<?=
		CHtml::ajaxLink(
			'Скачать',
			createUrl('//admin/news/default/index'),
			array(
				'type' => 'POST',
				'dataType' => 'json',
				'data' => array(
					'News' => array(
						'arID' => 'js:processSelected()',
						'action' => 'download'
					),
				),
				'success' => 'function(data){
					if(data.code == 200){
						window.location.href = data.url;
						setTimeout(function(){
							$.ajax({
								url:baseUrl+"/news/default/ArchiveDelete",
								type: "POST",
								data:{
									"archiveName":data.url
								},
								success:function(data){
									console.log(data);
								}
							});
						},3000)
					}
					if(data.code == 400){
						alert("systemError, plz try again later");
					}
				}'
			),
			array('class' => 'btn btn-info'))?>

		<?=
		CHtml::ajaxLink(
			'В Архив',
			createUrl("//admin/news/default/index"),
			array(
				'type' => 'POST',
				'async' => 'false',
				'data' => array(
					'News' => array(
						'arID' => 'js:processSelected()',
						'action' => 'statusArchive'
					),
				),
				'success' => 'function(data){
			$.fn.yiiGridView.update("news_grid", {
				data: $(this).serialize()
			});
		}'
			),
			array('class' => 'btn btn-danger')) ?>
	</div>
<? $this->renderPartial('grid', array('dataProvider' => $dataProvider, false, true)) ?>