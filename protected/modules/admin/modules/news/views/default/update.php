<?php
/**
 * @var $form TbActiveForm
 * @var $model News
 */
?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'news-form',
	'enableAjaxValidation' => true,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	)
)); ?>

<p class="help-block">Поля с <span class="required">*</span> обязательны.</p>

<?php echo $form->errorSummary($model); ?>


<?php echo $form->dropDownListRow($model, 'is_avaliable', News::$STATUSES, array('class' => 'span5')); ?>

<?php //echo $form->fileFieldRow($model, 'image', array('class' => 'span5')); ?>

<?php //echo $form->fileFieldRow($model, 'video', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'disabled' => true)); ?>

<?php echo $form->textFieldRow($model, 'content', array('class' => 'span5', 'disabled' => true)); ?>

<?php echo $form->dropDownListRow($model, 'locale', array('en' => 'en', 'ru' => 'ru', 'il' => 'il'), array('class' => 'locale')); ?>


<?php echo $form->textFieldRow($model, 'timeCreate', array('class' => 'span5', 'disabled' => 'disabled', 'value' => yii()->dateFormatter->formatDateTime($model->timeCreate))); ?>
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save',
	)); ?>
</div>

<?php $this->endWidget(); ?>
