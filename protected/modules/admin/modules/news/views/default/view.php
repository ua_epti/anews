<?/***
 * @var $model News
 */
?>

<div class="news_status">
	<strong><?= yii::t('News', 'Status:') . News::$STATUSES[$model->is_avaliable] ?></strong>
</div>
<? $this->widget('NewsStatusButtonWidget', array('status' => $model->is_avaliable, 'id' => $model->id)) ?>
<div class="span4">
	<? $this->widget('bootstrap.widgets.TbDetailView', array(
		'id'=>'news_view',
		'data' => $model,
		'attributes' => array(
			'name' => 'userID',
			'managerID',
			'title',
			'content',
			'locale',
			array(
				'name' => 'timeCreate',
				'type' => 'raw',
				'value' => yii()->dateFormatter->formatDateTime('$data->timeCreate'),
			),
			'paypal_status',
		),
	));
	?>
</div>
<div class="span6">
	<? if ($model->image != ""): ?>
		<?= CHtml::image(createUrl(yii()->storage->getFileUrl($model->image)), '', array()) ?>
	<? elseif ($model->video != ""): ?>
		<? $this->widget("ext.videoWidget.VideoWidget", array("videoUrl" => $model->video, 'cssClass' => 'player-full')); ?>
	<? endif; ?>
</div>

