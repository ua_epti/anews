<?/***
 * @var $model News
 */
?>
<? //= CHtml::link('Удалить', 'javascript:void(0)', array('class' => 'btn btn-danger')) ?>
<?registerScript('viewAction', '
$("#news_grid table tbody tr td").live("click", function () {
		if(!$(this).hasClass("checkBoxTD")){
			var id = $(this).parent().find("td:last").text();
			window.location.href = baseUrl + "/admin/news/default/view/id/"+id;
		}
	});
')?>
<?registerScript('massAction', "
	function processSelected() {
		var arElemID = new Array();
		$('input[name^=news_grid_c0]:checked').each(function(i){
			var id = $(this).val();
			if(id != 1){
				arElemID[i] = $(this).val();
			}
		});
		return arElemID;
	}")?>
<?=
CHtml::ajaxLink(
	'Убрать',
	createUrl('//admin/news/default/index'),
	array(
		'beforeSend' => 'function(){
			var arID = processSelected();
			if(arID==""){
				alert("Выбирите новости для перемещения в архив");
				return false;
			}
		}',
		'type' => 'POST',
		'data' => array(
			'News' => array(
				'arID' => 'js:processSelected()',
				'action' => 'statusRevertPaid'
			),
		),
		'success' => 'function(data){
			$.fn.yiiGridView.update("news_grid", {
				data: $(this).serialize()
			});
			$("#SideBarMenu").html(data);
		}'
	),
	array('class' => 'btn btn-danger'))?>
<? $this->renderPartial('grid', array('dataProvider' => $model->strip())) ?>