<h1>News Settings </h1>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'news-settings-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'name',
		'value',
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
