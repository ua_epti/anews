<?php

class NewsSettingsController extends Controller
{
	public $layout = '//layouts/column2';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/*public function actionCreate()
	{
		$model = new NewsSettings;
		$this->performAjaxValidation($model);
		if (isset($_POST['NewsSettings'])) {
			$model->attributes = $_POST['NewsSettings'];
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}*/

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$this->performAjaxValidation($model);
		if (isset($_POST['NewsSettings'])) {
			$model->attributes = $_POST['NewsSettings'];
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/*public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel($id)->delete();
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}*/

	public function actionIndex()
	{
		$model = new NewsSettings('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['NewsSettings'])) {
			$model->attributes = $_GET['NewsSettings'];
		}

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function loadModel($id)
	{
		$model = NewsSettings::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'news-settings-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}