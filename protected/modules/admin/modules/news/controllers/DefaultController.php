<?
class DefaultController extends Controller
{
	public $layout = '//layouts/news';

	public $defaultAction = 'index';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionIndex()
	{
		$model = new News('search');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		if (yii()->request->isAjaxRequest) {
			if (isset($_POST['News']['arID'])) {
				if ($_POST['News']['action'] == 'statusPaid') {

					if (is_array($_POST['News']['arID'])) {
						foreach (($_POST['News']['arID']) as $key => $id) {
							if ($id == '') {
								unset($_POST['News']['arID'][$key]);
							}
						}
					}
					$newsID = array_values($_POST['News']['arID']);
					$this->renderPartial("mass", array(
						'id' => $newsID[0],
					), false, true);
					yii()->end();
				}
				$this->processMassAction($_POST['News']['action'], $_POST['News']['arID']);
			} elseif (!isset($_POST['News']['arID']) && $_POST['News']['action'] == 'statusPaid') {
				echo 'error';
				yii()->end();
			}
		}
		$this->render('index', array('dataProvider' => $model->moderate()));
	}

	public function actionFirstLvl()
	{
		$model = new News('first');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		if (yii()->request->isAjaxRequest) {
			if (isset($_POST['News']['arID'])) {
				$this->processMassAction($_POST['News']['action'], $_POST['News']['arID']);
			}
		}
		$this->render('firstlvl', array('dataProvider' => $model->first()));
	}

	public function actionSecondLvl()
	{
		$model = new News('second');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('secondlvl', array('dataProvider' => $model->second()));
	}

	public function actionThirdLvl()
	{
		$model = new News('third');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('beforePaid', array('dataProvider' => $model->third()));
	}

	private function processMassAction($action, $arID)
	{
		switch ($action) {
			case 'statusModerate':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::DEFAULT_STATUS;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'statusFirstLvl':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::CONFIRM_STEP_1;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'statusSecondLvl':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::CONFIRM_STEP_2;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'statusBeforePaid':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::CONFIRM_STEP_3;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'statusPaid':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::CONFIRM_STEP_4;
						$actModel->save(false);
					}
				}
				break;
			case 'statusRevertPaid':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::CONFIRM_STEP_4;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'strip':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::STRIP;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'statusArchive':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::ARCHIVE;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'download':
				$arFileNames = array();
				foreach ($arID as $id) {
					if ($id != '') {
						$model = $this->loadModel($id);
						if ($model->image != '') {
							$arFileNames[] = yii()->storage->getFilePath($model->image);
						} elseif ($model->video != '') {
							$arFileNames[] = yii()->storage->getFilePath($model->video);
						}
					}
				}
				if (!empty($arFileNames)) {
					$arResp = array();
					$zipResult = yii()->storage->createZipArchive($arFileNames);
					if (is_file($zipResult['zip_path'])) {
						$arResp = array('code' => '200', 'url' => yii()->controller->createAbsoluteUrl('/images/news/' . $zipResult['zip_name']));
						echo CJSON::encode($arResp);
					} else {
						$arResp = array('code' => '400', 'error' => $zipResult['error']);
						echo CJSON::encode($arResp);
					}
					yii()->end();
				}
				break;
		}
	}

	public function actionRequestPayment($id)
	{
		if (!empty($id)) {
			if (isset($_POST['price'])) {
				$news = News::model()->findByPk($id);
				if ($news->task == null) {
					$news->dop_price = $_POST['price'];
					if ($news->save()) {
						/*PayPalActionPay*/
						$query['data'] = array(
							'userEmail' => $news->author->paypalemail,
							'amount' => $news->dop_price,
							'newsID' => $news->id,
						);
						$resArray = yii()->PayPal->Pay($query);
						$isSuccess = yii()->PayPal->isCallSucceeded($resArray);
						if ($isSuccess) {
							$news->is_avaliable = News::CONFIRM_STEP_4;
							if ($news->save()) {
								UserState::model()->setUserState($news->userID, $query['data']['amount']);
								UserPaymentHistory::setUserHistory($news);
								$this->redirect('/admin/news/default/thirdLvl');
							}
						} else {
							$news->is_avaliable = News::PAY_ERROR;
							if ($news->save()) {
								$this->redirect('/admin/news/default/thirdLvl');
							}
						}
					} elseif (YII_DEBUG) {
						dump($news->getErrors());
						die;
					}
				} else {
					if (!empty($news->taskID)) {
						$task = Tasks::model()->findByPk($news->taskID);
						/*PayPalActionPay*/
						$query['data'] = array(
							'userEmail' => $news->author->paypalemail,
							'amount' => $task->price,
							'newsID' => $news->id,
						);

						$resArray = yii()->PayPal->Pay($query);
						$isSuccess = yii()->PayPal->isCallSucceeded($resArray);

						if ($isSuccess) {
							$news->is_avaliable = News::CONFIRM_STEP_4;
							if ($news->save()) {
								UserState::model()->setUserState($news->userID, $query['data']['amount']);
								UserPaymentHistory::setUserHistory($news);
								$this->redirect('/admin/news/default/thirdLvl');
							}
						} else {
							$news->is_avaliable = News::PAY_ERROR;
							if ($news->save()) {
								$this->redirect('/admin/news/default/thirdLvl');
							}
						}

						$news->is_avaliable = News::CONFIRM_STEP_4;
						if ($news->save()) {
							UserPaymentHistory::setUserHistory($news);
							$this->redirect('/admin/news/default/thirdLvl');
						}
					}
				}
			}
		}
	}

	public function actionPaid()
	{
		$model = new News('paid');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('paid', array('dataProvider' => $model->paid()));
	}

	public function actionPayError()
	{
		$model = new News('payError');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('payerror', array('dataProvider' => $model->payError()));
	}

	public function actionArchive()
	{
		$model = new News('archive');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('archive', array('dataProvider' => $model->archive()));
	}

	public function actionStrip()
	{
//		$this->layout = '//layouts/strip';
		$model = new News('strip');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('strip', array('model' => $model));
	}

	public function actionCreate()
	{
		$this->forward('update');
	}

	public function actionUpdate($id = null)
	{
		if ($id == null) {
			$model = new News();
		} else {
			$model = $this->loadModel($id);
		}
		$this->performAjaxValidation($model);
		if (isset($_POST['News'])) {
			if ($_POST['News']['image'] == '' && $_FILES['News']['name']['image'] == '') {
				unset($_POST['News']['image']);
			}
			if ($_POST['News']['video'] == '' && $_FILES['News']['name']['video'] == '') {
				unset($_POST['News']['video']);
			}
			$model->attributes = $_POST['News'];
			if ($_FILES['News']['name']['image']) {
				$this->processFile($model);
			}
			if ($_FILES['News']['name']['video']) {
				$this->processFileUpload($model);
			}
			if ($model->isNewRecord) {
				user()->setFlash('success', 'Новость создана');
			} else {
				user()->setFlash('success', 'Новость обновлена');
			}
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			} elseif (YII_DEBUG) {
				dump($model->getErrors());
				die;
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	private function processFileUpload($model)
	{
		$model->video = CUploadedFile::getInstance($model, 'video');
		if ($model->video instanceof CUploadedFile) {
			$model->video = yii()->storage->addFile($model->video->tempName, $model->video->name);
		}
		$model->save();
	}

	private function processFile($model)
	{
		Yii::import('application.extensions.image.Image');

		if (Yii::app()->request->isPostRequest) {
			$model->attributes = $_POST['News'];
			$model->image = CUploadedFile::getInstance($model, 'image');
			$ext = $model->image->extensionName;
			if ($model->image instanceof CUploadedFile) {
				$folder = Yii::app()->runtimePath . DIRECTORY_SEPARATOR;
				$tmpName = tempnam($folder, 'img') . '.' . $ext;

				$origTmpName = $model->image->tempName;
				$resize = new Image($origTmpName);
				$resize->resize(yii()->params['files']['imageWidth'], yii()->params['files']['imageHeight'], Image::AUTO);
				$resize->save($tmpName);
				$model->image = Yii::app()->storage->addFile($tmpName, $tmpName);
				$model->save();

				$tmpName = tempnam($folder, 'img') . '.' . $ext;
				$resize = new Image($origTmpName);
				$resize->resize(yii()->params['files']['thumbnailWidth'], yii()->params['files']['thumbnailHeight'], Image::AUTO);
				$resize->save($tmpName);
				$model->thumbnail = Yii::app()->storage->addFile($tmpName, $tmpName);
				$model->save();
			}
		}
	}

	public function actionFastAjaxUpdate()
	{
		Yii::import('editable.*');
		$es = new EditableSaver('News');
		$es->update();
	}

	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = News::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'news-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


}