<?
class AdminController extends Controller
{
	public $defaultAction = 'admin';
	public $layout = '//layouts/lk';
	private $_model;

	public function filters()
	{
		return CMap::mergeArray(parent::filters(), array(
			'accessControl',
		));
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'delete', 'create', 'update', 'view'),
				'users' => UserModule::getAdmins(),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionAdmin()
	{
		$model = new User('search');
		$model->unsetAttributes();
		if (isset($_GET['User'])) {
			$model->attributes = $_GET['User'];
		}

		$this->render('index', array(
			'model' => $model,
		));
	}

	public function actionView()
	{
		$model = $this->loadModel();
		$this->render('view', array(
			'model' => $model,
		));
	}

	public function actionCreate()
	{
		$model = new User;
		$profile = new Profile;
		$this->performAjaxValidation(array($model, $profile));
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			$model->activkey = Yii::app()->controller->module->encrypting(microtime() . $model->password);
			$profile->attributes = $_POST['Profile'];
			$profile->user_id = 0;
			if ($model->validate() && $profile->validate()) {
				$model->password = Yii::app()->controller->module->encrypting($model->password);
				if ($model->save()) {
					$profile->user_id = $model->id;
					$profile->save();
				}
				$this->redirect(array('view', 'id' => $model->id));
			} else {
				$profile->validate();
			}
		}

		$this->render('create', array(
			'model' => $model,
			'profile' => $profile,
		));
	}

	public function actionUpdate()
	{
		$model = $this->loadModel();
		$profile = $model->profile;
		$this->performAjaxValidation(array($model, $profile));
		if (isset($_POST['User'])) {
			$model->attributes = $_POST['User'];
			$profile->attributes = $_POST['Profile'];

			if ($model->validate() && $profile->validate()) {
				$old_password = User::model()->notsafe()->findByPk($model->id);
				if ($old_password->password != $model->password) {
					$model->password = Yii::app()->controller->module->encrypting($model->password);
					$model->activkey = Yii::app()->controller->module->encrypting(microtime() . $model->password);
				}
				$model->save();
				$profile->save();
				/*if (yii()->getModule('admin')->hasModule('rights')) {
					$authenticatedName = yii()->getModule('admin')->getModule('rights')->authenticatedName;
					yii()->authManager->assign($authenticatedName, $model->id);
				}*/
				$this->redirect(array('view', 'id' => $model->id));
			} else {
				$profile->validate();
			}
		}
		$this->render('update', array(
			'model' => $model,
			'profile' => $profile,
		));
	}

	public function actionDelete()
	{
		if (Yii::app()->request->isPostRequest) {
			$model = $this->loadModel();
			$profile = Profile::model()->findByPk($model->id);
			if ($profile) {
				$profile->delete();
			}
			$model->delete();
			if (!isset($_POST['ajax'])) {
				$this->redirect(array('/user/admin'));
			}
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	protected function performAjaxValidation($validate)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
			echo CActiveForm::validate($validate);
			Yii::app()->end();
		}
	}

	public function loadModel()
	{
		if ($this->_model === null) {
			if (isset($_GET['id'])) {
				$this->_model = User::model()->notsafe()->findbyPk($_GET['id']);
			}
			if ($this->_model === null) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}
		return $this->_model;
	}

}
