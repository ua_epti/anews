<?php
/** @var $model UserLogin */
/** @var $this Controller */
if (user()->isGuest):?>
	<?$this->widget('bootstrap.widgets.TbAlert', array('block' => true, // display a larger alert block?
		'fade' => true, // use transitions?
		'closeText' => '&times;', // close link text - if set to false, no close link is displayed
	));?>
	<?= CHtml::beginForm(user()->loginUrl, null, array('name' => 'authForm')) ?>
	<div class="a-row">
		<span class="pull-left"><i class="icon-auser"></i></span>
		<?=CHtml::activeTextField($model, 'username', array('placeholder' => 'Имя')) ?>
	</div>
	<div class="a-row">
		<span class="pull-left"><i class="icon-password"></i></span>
		<?=CHtml::activePasswordField($model, 'password', array('placeholder' => 'Пароль')) ?>
		<?=CHtml::ajaxSubmitButton(
			'',
			user()->loginUrl,
			array(
				'type' => 'POST',
				'success' => 'function(data){
					if(data.success===1)
						$("#' . $this->id . '").replaceWith(data.html);
					else
					{
						$("#' . $this->id . ' .auth-hidden").replaceWith($(data.html).find(".auth-hidden"));
						$(".auth-hidden").show();
						$("#' . $this->id . ' .auth-hidden").css({
							"top" : $("#' . $this->id . ' #auth").offset().top+$("#' . $this->id . ' #auth").height()+18,
							"left" : $("#' . $this->id . ' #auth").offset().left-120
						});
					}
				}',
				'dataType' => 'JSON'
			),
			array('class' => 'hidden')
		)?>
		<?=CHtml::ajaxLink(
			'<i class="icon-arrow"></i>',
			user()->loginUrl,
			array(
				'type' => 'POST',
				'success' => 'function(data){
					if(data.success===1)
						$("#' . $this->id . '").replaceWith(data.html);
					else
					{
						$("#' . $this->id . '").replaceWith(data.html);
						/*$("#' . $this->id . ' .auth-hidden").replaceWith($(data.html).find(".auth-hidden"));*/
						$(".auth-hidden").show();
						$(".auth-hidden").css({
							"top" : $("#auth").offset().top+$("#auth").height()+18,
							"left" : $("#auth").offset().left-120
						});
					}
				}',
				'dataType' => 'JSON'
			),
			array(
				'href' => normalizeUrl(user()->loginUrl),
				'class' => 'btn pull-right btn-warning',
			)
		)?>
	</div>
	<div class="a-links">
		<?=CHtml::ajaxLink(
			'Забыли пароль',
			user()->recoveryUrl,
			array(
				'type' => 'POST',
				'success' => 'function(data){
					$(".auth-content").html(data);
				}'
			),
			array(
				'href' => normalizeUrl(user()->recoveryUrl),
				'class' => 'pull-left forget-pass',
			)
		)?>
		<div class="pull-right social">
			<? yii()->eauth->renderWidget(array('action' => '/user/login')); ?>
		</div>
	</div>
	<?= CHtml::endForm() ?>
	<script type="text/javascript">
		$("#get-new-code").live('click', function (event) {
			event.preventDefault();
			$.ajax({
				url: '<?=createUrl('/user/login/getNewCode')?>',
				type: 'POST',
				data: $("form[name=authForm]").serializeArray()
			});
		});
	</script>
<? else: ?>
	<div class="registered auth contacts">
		<a class="pull-left mini-cart"
		   href="<?= (user()->isManager()) ? createUrl('//admin') : createUrl('//personal') ?>">
			<span class="count">
				<?=(user()->isManager()) ? Order::model()->getManagerOrdersCount(OrderStatus::$STATUSES_CURRENT) : Order::model()->getOrdersCount(OrderStatus::$STATUSES_CURRENT)?>
			</span>
		</a>
		<?//TODO: пофиксить цифирку?>
		<a class="pull-left mail"
		   href="<?= (user()->isManager()) ? createUrl('//admin') : createUrl('//personal', array('#' => 'messages')) ?>"><span
				class="count">0</span></a>
		<?=hyperlink(user()->name, yii()->getModule('user')->logoutUrl, array('class' => 'pull-right logout', 'title' => 'Выйти'))?>
	</div>
<?endif; ?>