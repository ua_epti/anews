<?php
$this->layout = '//layouts/column2';
$this->menu = array(
	array('label' => UserModule::t('Manage Users'), 'url' => array('//admin/user/admin')),
	array('label' => UserModule::t('Manage Profile Field'), 'url' => array('//admin/user/profileField/admin')),
);
?>

<h4><?= UserModule::t("List User"); ?></h4>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered condensed',
	'dataProvider' => $dataProvider,
	'columns' => array(
		array(
			'name' => 'username',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->username),array("user/view","id"=>$data->id))',
			'htmlOptions' => array(
				'width' => '76%',
			)
		),
//		'create_at',
		array(
			'header' => 'Был в системе',
			'name' => 'lastvisit_at',
			'type' => 'raw',
			'htmlOptions' => array(
				'width' => '24%',
			)
		),
	),
)); ?>
