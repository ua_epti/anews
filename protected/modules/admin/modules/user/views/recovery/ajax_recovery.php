<div id="recovery-form">
	<?php echo CHtml::beginForm(); ?>
	<?php echo CHtml::errorSummary($form); ?>

	<div class="a-row">
		<span class="pull-left"><i class="icon-auser"></i></span>
		<?=CHtml::activeTextField($form, 'login_or_email', array('placeholder' => 'Логин или email')) ?>
		<?=CHtml::ajaxSubmitButton(
			'',
			user()->recoveryUrl,
			array(
				'type' => 'POST',
				'success' => 'function(data){
						$("#recovery-form").replaceWith(data);
						$(".auth-hidden").show();
						$(".auth-hidden").css({
							"top" : $("#auth").offset().top+$("#auth").height()+18,
							"left" : $("#auth").offset().left-120
						});
					}',
//					'dataType' => 'JSON'
			),
			array('class' => 'hidden', 'id' => 'recovery-submit')
		)?>
		<?=CHtml::ajaxLink(
			'<i class="icon-arrow"></i>',
			user()->loginUrl,
			array(
				'type' => 'POST',
				'success' => 'function(data){
						$("#recovery-form").replaceWith(data);
						$(".auth-hidden").show();
						$(".auth-hidden").css({
							"top" : $("#auth").offset().top+$("#auth").height()+18,
							"left" : $("#auth").offset().left-120
						});
					}',
			),
			array(
				'href' => normalizeUrl(user()->loginUrl),
				'class' => 'btn pull-right btn-warning',
				'id' => 'recovery-submit-link',
			)
		)?>
	</div>
	<?php echo CHtml::endForm(); ?>
</div><!-- form -->
