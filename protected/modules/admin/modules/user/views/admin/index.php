<?php
/**
 * @var $this AdminController
 * @var $model User
 */
?>
<?= CHtml::link(UserModule::t('Create User'), createUrl("//admin/user/admin/create"), array('class' => 'btn btn-success')) ?>
<? if (YII_DEBUG): ?>
	<?= CHtml::link(UserModule::t('Manage Profile Field'), createUrl("//admin/user/profileField/admin"), array('class' => 'btn btn-info')) ?>
<? endif; ?>

	<h4><?= UserModule::t("Manage Users"); ?></h4>
<?$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'user-grid',
	'type' => 'striped bordered condensed',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array(
			'name' => 'id',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->id),array("admin/update","id"=>$data->id))',
			'filter' => false,
		),
		array(
			'name' => 'username',
			'type' => 'raw',
			'value' => 'CHtml::link(UHtml::markSearch($data,"username"),array("admin/view","id"=>$data->id))',
		),
		array(
			'name' => 'email',
			'type' => 'raw',
			'value' => 'CHtml::link(UHtml::markSearch($data,"email"), "mailto:".$data->email)',
		),
		array(
			'header' => 'Роль',
			'name' => 'user_group_search',
			'value' => 'implode(", ",array_map(function($item){return $item->authItem;},$data->user_group))',
			'filter' => AuthItem::getForFilter(),
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{plus}',
			'buttons' => array(
				'update' => array(
					'label' => '<i class="ictable-edit"></i>',
					'imageUrl' => false,
					'options' => array(
						'title' => 'Редактировать пользователя',
					),
				),
				'plus' => array(
					'label' => '<i class="btn-add"></i>',
					'imageUrl' => false,
					'url' => 'createUrl("//admin/rights/assignment/user",array("id"=>$data->id))',
					'options' => array(
						'title' => 'Группы пользователя',
					),
				),
			),
		),
	),
));
?>