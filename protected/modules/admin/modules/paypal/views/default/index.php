<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'paymenthistory-grid',
	'dataProvider' => $model->getUserHistory(),
	'filter' => $model,
	'columns' => array(
		array(
			'name' => 'userID',
			'value' => 'User::model()->getUserNameByPk($data->userID)',
		),
		array(
			'name' => 'shopID',
		),
		array(
			'name' => 'comment',
		),
		array(
			'name' => 'bill',
		),
		array(
			'name' => 'action',
			'value' => 'UserPaymentHistory::model()->getAction($data->action)',
		),
		array(
			'name' => 'currencyID',
			'value' => 'Currency::model()->getIsoCodeById($data->currencyID)',
		),
		array(
			'name' => 'sysCode',
		),
		array(
			'name' => 'balance',
		),
		array(
			'name' => 'orderID',
		),
		array(
			'name' => 'purse',
		),
		array(
			'name' => 'qiwiTxn',
		),
		array(
			'name' => 'amount',
		),
		array(
			'name' => 'timeCreate',
			'value' => 'yii()->dateFormatter->formatDateTime($data->timeCreate,"short")',
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
		),
	),
));