<?
class DefaultController extends Controller
{
	public $layout = '//layouts/main';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionIndex()
	{
		$model = new UserPaymentHistory('getUserHistory');
		$model->unsetAttributes();
		if (isset($_GET['UserPaymentHistory'])) {
			$model->attributes = $_GET['UserPaymentHistory'];
		}
		$this->render('index', array(
			'model' => $model
		));
	}
}