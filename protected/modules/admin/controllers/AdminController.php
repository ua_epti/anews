<?php

class AdminController extends Controller
{
	public $layout = '//layouts/lk';


	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionIndex()
	{
		if (user()->isGuest) {
			$this->redirect('//admin/admin/login');
		}
		$this->redirect('admin/news/default');
	}
}
