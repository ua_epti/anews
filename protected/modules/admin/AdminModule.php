<?
class AdminModule extends CWebModule
{
	public $DefaultController = 'admin';

	public function init()
	{
		$this->setImport(array(
			'admin.models.*',
			'admin.components.*',
		));
		Yii::setPathOfAlias('admin', dirname(__FILE__));
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}
}
