<?php

/**
 * This is the model class for table "{{withdrawal}}".
 *
 * The followings are the available columns in table '{{withdrawal}}':
 * @property integer $id
 * @property string $summ
 * @property integer $currencyID
 * @property string $purse
 * @property integer $managerID
 * @property integer $userID
 * @property integer $payed
 * @property string $comment
 * @property integer $timeCreate
 * @property integer $timePay
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class Withdrawal extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Withdrawal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{withdrawal}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('summ, currencyID, purse, managerID, userID, comment, timeCreate, timePay', 'required'),
			array('currencyID, managerID, userID, payed, timeCreate, timePay', 'numerical', 'integerOnly'=>true),
			array('summ', 'length', 'max'=>15),
			array('purse', 'length', 'max'=>20),
			array('comment', 'length', 'max'=>1000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, summ, currencyID, purse, managerID, userID, payed, comment, timeCreate, timePay', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'summ' => 'Summ',
			'currencyID' => 'Currency',
			'purse' => 'Purse',
			'managerID' => 'Manager',
			'userID' => 'User',
			'payed' => 'Payed',
			'comment' => 'Comment',
			'timeCreate' => 'Time Create',
			'timePay' => 'Time Pay',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('summ',$this->summ,true);
		$criteria->compare('currencyID',$this->currencyID);
		$criteria->compare('purse',$this->purse,true);
		$criteria->compare('managerID',$this->managerID);
		$criteria->compare('userID',$this->userID);
		$criteria->compare('payed',$this->payed);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('timeCreate',$this->timeCreate);
		$criteria->compare('timePay',$this->timePay);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}