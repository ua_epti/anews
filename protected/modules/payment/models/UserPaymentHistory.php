<?php

/**
 * This is the model class for table "{{user_payment_history}}".
 *
 * The followings are the available columns in table '{{user_payment_history}}':
 * @property integer $id
 * @property integer $userID
 * @property integer $newsID
 * @property string $comment
 * @property string $datetime
 * @property string $bill
 * @property integer $action
 * @property string $model
 * @property integer $currencyID
 * @property string $sysCode
 * @property string $balance
 * @property integer $orderID
 * @property string $purse
 * @property string $qiwiTxn
 * @property string $amount
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Currency $currency
 */
class UserPaymentHistory extends CActiveRecord
{

	const DEBIT = 1;
	const CREDIT = -1;
	protected $arActionValue = array('D' => self::DEBIT, 'C' => self::CREDIT);
	protected $arActionLabel = array('D' => 'Пополнение', 'C' => 'Расход');
	protected $arActionList = array('1' => 'Пополнение', '-1' => 'Расход');

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{user_payment_history}}';
	}

	public function rules()
	{
		return array(
			array('userID, model, bill, amount, balance, currencyID, sysCode,timeCreate', 'required', 'on' => 'exchange'),
			array('userID, newsID, action, currencyID, timeCreate', 'numerical', 'integerOnly' => true),
			array('comment, purse', 'length', 'max' => 255),
			array('bill, balance, amount', 'length', 'max' => 15),
			array('sysCode', 'length', 'max' => 10),
			array('id, userID, newsID, comment, timeCreate, bill, action, currencyID, sysCode, balance, purse, amount', 'safe', 'on' => 'search'),
			array('id, userID, newsID, comment, timeCreate, bill, action, currencyID, sysCode, balance, purse, amount', 'safe', 'on' => 'getUserHistory'),
			array('id, userID, newsID, comment, timeCreate, bill, action, currencyID, sysCode, balance, purse, amount', 'safe', 'on' => 'getHistory'),
		);
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userID'),
			'currency' => array(self::BELONGS_TO, 'Currency', 'currencyID'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userID' => 'User',
			'newsID' => 'Shop',
			'comment' => 'Comment',
			'timeCreate' => 'timeCreate',
			'bill' => 'Bill',
			'action' => 'Action',
			'currencyID' => 'Currency',
			'sysCode' => 'Sys Code',
			'balance' => 'Balance',
			'purse' => 'Purse',
			'amount' => 'Amount',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('newsID', $this->newsID);
		$criteria->compare('comment', $this->comment, true);
		$criteria->compare('timeCreate', $this->timeCreate, true);
		$criteria->compare('bill', $this->bill, true);
		$criteria->compare('action', $this->action);
		$criteria->compare('currencyID', $this->currencyID);
		$criteria->compare('sysCode', $this->sysCode, true);
		$criteria->compare('balance', $this->balance, true);
		$criteria->compare('purse', $this->purse, true);
		$criteria->compare('amount', $this->amount, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function getUserHistory($userID = null)
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		if ($userID == null) {
			$userID = user()->id;
			$criteria->compare('userID', $userID);
		}
		$criteria->compare('userID', $userID);
		$criteria->compare('newsID', $this->newsID);
		$criteria->compare('comment', $this->comment, true);
		$criteria->compare('timeCreate', $this->timeCreate, true);
		$criteria->compare('bill', $this->bill, true);
		$criteria->compare('action', $this->action);
		$criteria->compare('currencyID', $this->currencyID);
		$criteria->compare('sysCode', $this->sysCode, true);
		$criteria->compare('balance', $this->balance, true);
		$criteria->compare('purse', $this->purse, true);
		$criteria->compare('amount', $this->amount, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria
		));
	}

	public function getHistory()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('id', $this->id);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('newsID', $this->newsID);
		$criteria->compare('comment', $this->comment, true);
		$criteria->compare('timeCreate', $this->timeCreate, true);
		$criteria->compare('bill', $this->bill, true);
		$criteria->compare('action', $this->action);
		$criteria->compare('currencyID', $this->currencyID);
		$criteria->compare('sysCode', $this->sysCode, true);
		$criteria->compare('balance', $this->balance, true);
		$criteria->compare('purse', $this->purse, true);
		$criteria->compare('amount', $this->amount, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria
		));
	}

	public function getAction($id)
	{
		$result = '';
		$action = array_search($id, $this->arActionValue);
		foreach ($this->arActionLabel as $key => $label) {
			if ($key == $action) {
				$result = $label;
			}
		}
		return $result;
	}

	public function getActionList()
	{
		return $this->arActionList;
	}

	public static function setUserHistory($news)
	{
		$model = new self();
		$model->newsID = $news->id;
		$model->userID = $news->author->id;
		$model->purse = $news->author->email;
		if ($news->dop_price != 0.00) {
			$model->bill = $news->dop_price;
		} else {
			$model->bill = $news->task->price;
		}
		$model->comment = 'Оплата Новости #' . $news->id;
		$model->currencyID = 1;
		$model->timeCreate = time();
		if ($model->save()) {

		} elseif (YII_DEBUG) {
			dump($model->getErrors());
			die;
		}
	}

	/*public function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->isNewRecord || empty($this->timeCreate)) {
				$this->timeCreate = time();
			}
			return true;
		}
		return false;
	}*/

	/*public function afterSave()
	{
		if (parent::afterSave()) {
			return true;
		}
		return false;
	}*/
}