<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'paymenthistory-grid',
	'dataProvider' => $model->getUserHistory(),
	'filter' => $model,
	'columns' => array(
		array(
			'header' => 'Имя пользователя',
			'name' => 'userID',
			'value' => 'User::model()->getUserNameByPk($data->userID)',
		),
		array(
			'header' => 'Комментарий',
			'name' => 'comment',
		),
		array(
			'header' => 'Кошелек',
			'name' => 'purse',
		),
		array(
			'header' => 'Сумма',
			'name' => 'bill',
			'value' => '$data->bill',
		),
		array(
			'header' => 'Время оплаты',
			'name' => 'timeCreate',
			'value' => 'yii()->dateFormatter->formatDateTime($data->timeCreate,"short")',
		),
	),
));