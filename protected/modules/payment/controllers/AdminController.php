<?
class AdminController extends Controller
{
	public $layout = '//layouts/main';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionIndex()
	{
		$model = new UserPaymentHistory('getHistory');
		$model->unsetAttributes();
		if (isset($_GET['UserPaymentHistory'])) {
			$model->attributes = $_GET['UserPaymentHistory'];
		}
		$this->render('index', array(
			'model' => $model
		));
	}
}