<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UserLogin extends CFormModel
{
	public $username;
	public $password;
	public $onetime_pass;
	public $rememberMe;

	public function __construct($scenario = 'default')
	{
		parent::__construct($scenario);
	}


	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required', 'on' => 'default,managerPrepare'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			array('password', 'checkManager', 'on' => 'managerPrepare'),
			array('username', 'required', 'on' => 'step2'),
			array('onetime_pass', 'checkCode', 'on' => 'step2'),
			// password needs to be authenticated
			array('password', 'authenticate', 'on' => 'default'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe' => UserModule::t("Remember me next time"),
			'username' => UserModule::t("username or email"),
			'password' => UserModule::t("password"),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params)
	{
		if (!$this->hasErrors()) // we only want to authenticate when no input errors
		{
			$identity = new UserIdentity($this->username, $this->password);
			$identity->authenticate();
			switch ($identity->errorCode) {
				case UserIdentity::ERROR_NONE:
					$duration = $this->rememberMe ? Yii::app()->controller->module->rememberMeTime : 0;
					Yii::app()->user->login($identity, $duration);
					break;
				case UserIdentity::ERROR_EMAIL_INVALID:
					$this->addError("username", UserModule::t("Email is incorrect."));
					break;
				case UserIdentity::ERROR_USERNAME_INVALID:
					$this->addError("username", UserModule::t("Username is incorrect."));
					break;
				case UserIdentity::ERROR_STATUS_NOTACTIV:
					$this->addError("status", UserModule::t("You account is not activated."));
					break;
				case UserIdentity::ERROR_STATUS_BAN:
					$this->addError("status", UserModule::t("You account is blocked."));
					break;
				case UserIdentity::ERROR_PASSWORD_INVALID:
					$this->addError("password", UserModule::t("Password is incorrect."));
					break;
			}
		}
	}

	public function checkManager($attribute, $params)
	{
		if (!$this->hasErrors()) // we only want to authenticate when no input errors
		{
			$identity = new UserIdentity($this->username, $this->password);
			$identity->checkManager();
			switch ($identity->errorCode) {
				case UserIdentity::ERROR_NONE:
					break;
				case UserIdentity::ERROR_EMAIL_INVALID:
					$this->addError("username", UserModule::t("Email is incorrect."));
					break;
				case UserIdentity::ERROR_USERNAME_INVALID:
					$this->addError("username", UserModule::t("Username is incorrect."));
					break;
				case UserIdentity::ERROR_STATUS_NOTACTIV:
					$this->addError("status", UserModule::t("You account is not activated."));
					break;
				case UserIdentity::ERROR_STATUS_BAN:
					$this->addError("status", UserModule::t("You account is blocked."));
					break;
				case UserIdentity::ERROR_PASSWORD_INVALID:
					$this->addError("password", UserModule::t("Password is incorrect."));
					break;
				case UserIdentity::ERROR_NOT_MANAGER:
					break;
			}
			if ($identity->errorCode != UserIdentity::ERROR_NONE) {
				$this->setScenario('default');
				$this->validate();
			}
		}
	}

	public function checkCode($attribute, $params)
	{
		if (!$this->hasErrors()) // we only want to authenticate when no input errors
		{
			$identity = new UserIdentity($this->username, $this->onetime_pass);
			$identity->authenticateByCode();
			switch ($identity->errorCode) {
				case UserIdentity::ERROR_NONE:
					$duration = $this->rememberMe ? Yii::app()->controller->module->rememberMeTime : 0;
					Yii::app()->user->login($identity, $duration);
					break;
				case UserIdentity::ERROR_EMAIL_INVALID:
					$this->addError("username", UserModule::t("Email is incorrect."));
					break;
				case UserIdentity::ERROR_USERNAME_INVALID:
					$this->addError("username", UserModule::t("Username is incorrect."));
					break;
				case UserIdentity::ERROR_CODE_INVALID:
					$this->addError("onetime_pass", UserModule::t("Некорректный код подтверждения"));
					break;
			}
		}
	}
}
