<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * user registration form data. It is used by the 'registration' action of 'UserController'.
 */
class RegistrationForm extends User
{
	public $verifyPassword;
	public $verifyCode;
	public $on_time_pass;

	public $upload;


	public function rules()
	{
		$rules = array(
			array('username, password, verifyPassword, email', 'required', 'on' => 'register'),
			array('username', 'length', 'max' => 20, 'min' => 3, 'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max' => 128, 'min' => 4, 'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),
			array('email,password,verifyPassword,', 'required', 'on' => 'ajax_register'),
			array('email', 'unique', 'on' => 'ajax_register'),
			array('email', 'email', 'on' => 'ajax_register'),
//			array('on_time_pass', 'numerical', 'integerOnly' => true, 'on' => 'ajax_register'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
			array('verifyPassword', 'compare', 'compareAttribute' => 'password', 'message' => UserModule::t("Retype Password is incorrect.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => UserModule::t("Incorrect symbols (A-z0-9).")),
		);
		if (!(isset($_POST['ajax']) && $_POST['ajax'] === 'registration-form')) {
			array_push($rules, array('verifyCode', 'captcha', 'allowEmpty' => !UserModule::doCaptcha('registration'), 'on' => 'register'));
		}
		if ($this->scenario == 'ajax_register') {
			array_push($rules, array('verifyPassword', 'compare', 'compareAttribute' => 'password', 'message' => UserModule::t("Retype Password is incorrect.")));
		} else {
//			array_push($rules, array('verifyPassword', 'compare', 'compareAttribute' => 'password', 'message' => UserModule::t("Retype Password is incorrect.")));
		}
		return $rules;
	}


	public function Register($data, $scenario = 'register')
	{
		$model = new RegistrationForm;
		$profile = new Profile;
		if ($scenario == 'ajax_register') {
			$model->scenario = $scenario;
		} else {
			$model->scenario = $scenario;
		}
		if (isset($_POST['RegistrationForm']['details'])) {
			$_POST['RegistrationForm']['details'] = _clearArray($_POST['RegistrationForm']['details']);
		}
		$model->attributes = $data['RegistrationForm'];
		do {
			$usr = 'id' . rand(100000, 999999);
			$user = User::model()->findByAttributes(array('username' => $usr));
		} while ($user !== null);

		$model->username = $usr;
		$profile->attributes = ((isset($data['Profile']) ? $data['Profile'] : array()));
		if ($model->validate() && $profile->validate()) {
			$sourcePassword = $model->password;
			$model->activkey = UserModule::encrypting(microtime() . $model->password);
			$model->password = UserModule::encrypting($model->password);
			$model->verifyPassword = UserModule::encrypting($model->verifyPassword);
			$model->superuser = 0;
			$model->status = User::STATUS_PRE_ACTIVE;

			if ($model->save()) {
				$profile->user_id = $model->id;
				do {
					$refCode = randString(8);
					$exists = Profile::model()->findByAttributes(array('referral_code' => $refCode));
				} while ($exists !== null);
				$profile->referral_code = $refCode;
				if (!empty($_REQUEST['lid']) || !empty(yii()->session['lid'])) {
					$parent = $profile::model()->findByAttributes(array('referral_code' => !empty($_REQUEST['lid']) ? $_REQUEST['lid'] : yii()->session['lid']));
					if ($parent !== null) {
						$profile->parent_user_id = $parent->user_id;
					}
				}
				$profile->save();
				if ($scenario == 'ajax_register') {
					yii()->user->setFlash('success', $this->GetRegSuccessMessage());
					$activation_url = yii()->createAbsoluteUrl('/user/activation/activation', array("activkey" => $model->activkey, "email" => $model->email));
					UserModule::sendMail(
						$model->email,
						UserModule::t("You registered from {site_name}", array('{site_name}' => yii()->name)),
						UserModule::t(
							"<p>&nbsp;&nbsp;&nbsp;&nbsp;Настоящим сообщаем, что системой {site_name} была получена заявка на участие в отборе участников для программы Битва Мастеров.<br>
					        В заявке был указан Ваш электронный адрес.<br>
					        Если заявка была сформирована и отправлена Вами, пройдите по следующей ссылке <br> {activation_url} .</p><br>
					        <p>&nbsp;&nbsp;&nbsp;&nbsp;После активации заявки и ее удачного рассмотрения, мы дополнительно свяжемся с вами по вопросу последующего участия в программе отбора.<br>
					        Вам присвоен регистрационный номер: {userID} .<br>
					        Сохраните данное сообщение и регистрационный номер.<br>
					        Данное сообщение не требует ответа.</p><br>
					        ",
							array(
								'{activation_url}' => $activation_url,
								'{userID}' => $model->id,
								'{login}' => $model->email,
								'{password}' => $sourcePassword,
								'{site_name}' => yii()->name
							)
						)
					);
					return true;
				}
			} elseif (YII_DEBUG) {
				user()->setFlash('error', var_export($model->getErrors(), 1));
			}
		} else {
			if ($scenario == 'ajax_register') {
				$errors = array();
				if ($model->hasErrors()) {
					foreach ($model->getErrors() as $error) {
						foreach ($error as $val) {
							$errors[] = $val;
						}
					}
					$model->password = '';
				} elseif ($profile->hasErrors()) {
					foreach ($profile->getErrors() as $error) {
						foreach ($error as $val) {
							$errors[] = $val;
						}
					}
				}
				yii()->user->setFlash('error', $this->GetRegErrorMessage($errors));
				return false;
			}
		}
		return array('model' => $model, 'profile' => $profile);
	}


	protected function GetRegSuccessMessage()
	{
		return yii::t('registration', 'Your application will be accepted after activation. In the letter we sent to you at your address, proydte the link and activate an application. We will report further on its successful review');
	}

	protected function GetRegErrorMessage($errors)
	{
		return yii::t('registration', 'Registration Error {errors}',
			array(
				'{errors}' => $errors[0]
			));
	}
}