<?php
/**
 * UserChangemasterKey class.
 * UserChangemasterKey is the data structure for keeping
 * user change masterKey form data. It is used by the 'MasterKey' action of 'UserController'.
 */
class UserChangeMasterKey extends CFormModel
{
	public $oldMasterKey;
	public $masterKey;
	public $verifyMasterKey;

	public function rules()
	{
		return Yii::app()->controller->id == 'recovery' ? array(
			array('masterKey, verifyMasterKey', 'required'),
			array('masterKey, verifyMasterKey', 'length', 'max' => 128, 'min' => 4, 'message' => UserModule::t("Incorrect masterKey (minimal length 4 symbols).")),
			array('verifyMasterKey', 'compare', 'compareAttribute' => 'masterKey', 'message' => UserModule::t("Retype masterKey is incorrect.")),
		) : array(
			array('oldMasterKey, masterKey, verifyMasterKey', 'required'),
			array('oldMasterKey, masterKey, verifyMasterKey', 'length', 'max' => 128, 'min' => 4, 'message' => UserModule::t("Incorrect masterKey (minimal length 4 symbols).")),
			array('verifyMasterKey', 'compare', 'compareAttribute' => 'masterKey', 'message' => UserModule::t("Retype masterKey is incorrect.")),
			array('oldMasterKey', 'verifyoldMasterKey'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'oldMasterKey' => UserModule::t("Old masterKey"),
			'masterKey' => UserModule::t("masterKey"),
			'verifyMasterKey' => UserModule::t("Retype masterKey"),
		);
	}

	/**
	 * Verify Old masterKey
	 */
	public function verifyoldMasterKey($attribute, $params)
	{
		if (User::model()->notsafe()->findByPk(Yii::app()->user->id)->masterKey != Yii::app()->getModule('user')->encrypting($this->$attribute)) {
			$this->addError($attribute, UserModule::t("Старый пароль введен не верно."));
		}
	}
}