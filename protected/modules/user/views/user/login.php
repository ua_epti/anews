<? $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Login") ?>
	<h1><?= UserModule::t("Login"); ?></h1>
<? if (Yii::app()->user->hasFlash('loginMessage')): ?>
	<div class="success">
		<?= Yii::app()->user->getFlash('loginMessage'); ?>
	</div>
<? endif; ?>
	<p><?= UserModule::t("Please fill out the following form with your login credentials:"); ?></p>
	<div class="form" id="user-login-form">
		<?= CHtml::beginForm(); ?>
		<p class="note"><?= UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
		<?= CHtml::errorSummary($model); ?>
		<div class="">
			<?= CHtml::activeLabelEx($model, 'username'); ?>
			<?= CHtml::activeTextField($model, 'username') ?>
		</div>
		<div class="">
			<?= CHtml::activeLabelEx($model, 'password'); ?>
			<?= CHtml::activePasswordField($model, 'password') ?>
		</div>
		<div class="submit">
			<?= CHtml::submitButton(UserModule::t("Login")); ?>
		</div>
		<?= CHtml::endForm(); ?>
	</div>
<?
$form = new CForm(array(
	'elements' => array(
		'username' => array(
			'type' => 'text',
			'maxlength' => 32,
		),
		'password' => array(
			'type' => 'password',
			'maxlength' => 32,
		),
		'rememberMe' => array(
			'type' => 'checkbox',
		)
	),
	'buttons' => array(
		'login' => array(
			'type' => 'submit',
			'label' => 'Login',
		),
	),
), $model);
?>