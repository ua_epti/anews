<?
/**
 * @var $field ProfileField
 * @var $form CActiveForm
 * @var $profile User
 * @var $user_model_password User
 */
?>
<?
$containerId = "private-data-tab";
?>
<div id="<?= $containerId ?>">
	<div class="row-fluid private-data">
		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'image-form',
			'enableAjaxValidation' => true,
			'action' => createUrl('//user/profile/edit'),
			'htmlOptions' => array('enctype' => 'multipart/form-data'),
		));
		?>
		<div class="span6 pull-left">
			<table class="private-info">
				<tr>
					<td>
						<?php echo $form->labelEx($profile, 'firstname'); ?>
					</td>
					<td>
						<?php echo $form->textField($profile, 'firstname', array('size' => 20, 'maxlength' => 20, 'onkeyup' => 'return withoutCyr(this);', 'class' => 'pull-left')); ?>
						<?php echo $form->error($profile, 'firstname'); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo $form->labelEx($profile, 'lastname'); ?>
					</td>
					<td>
						<?php echo $form->textField($profile, 'lastname', array('size' => 20, 'maxlength' => 20, 'onkeyup' => 'return withoutCyr(this);', 'class' => 'pull-left')); ?>
						<?php echo $form->error($profile, 'lastname'); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo $form->labelEx($profile, 'phone'); ?>
					</td>
					<td>
						<?php echo $form->textField($profile, 'phone', array('size' => 60, 'maxlength' => 128, 'class' => 'pull-left')); ?>
						<?php echo $form->error($profile, 'phone'); ?>
					</td>
				</tr>
				<tr>
					<td>Введите старый пароль</td>
					<td>
						<?php echo $form->passwordField($user_model_password, 'oldPassword', array('size' => 60, 'maxlength' => 128)); ?>
						<?php echo $form->error($user_model_password, 'oldPassword'); ?>
					</td>
				</tr>
				<tr>
					<td>Введите новый пароль</td>
					<td>
						<?php echo $form->passwordField($user_model_password, 'password', array('size' => 60, 'maxlength' => 128)); ?>
						<?php echo $form->error($user_model_password, 'password'); ?>
					</td>
				</tr>
				<tr>
					<td>
						Подтвердите новый пароль
					</td>
					<td>
						<?php echo $form->passwordField($user_model_password, 'verifyPassword', array('size' => 60, 'maxlength' => 128)); ?>
						<?php echo $form->error($user_model_password, 'verifyPassword'); ?>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<?= CHtml::ajaxLink('Отправить',
							yii()->createUrl('//user/profile/edit'),
							array(
								'beforeSend' => 'function(){
								if($("#Profile_firstname").val() != "" && $("#Profile_lastname").val() != ""){

								}else{
									alert("Введите Имя и Фамилию");
									return false;
								}
							}',
								'type' => 'POST',
								'success' => 'function(data){
								$("#private-data-tab").html(data);
							}',
							),
							array(
								'class' => 'btn btn-warning',
								'id' => uniqid('user-form-add'),
								'type' => 'button'
							));
						?>
					</td>
				</tr>
			</table>
		</div>
		<?php $this->endWidget(); ?>
		<div class="span5 pull-right">
			<div class="thumb">
				<?
				if ($profile->photo != '') {
					$user_photo = createUrl($profile->photo);
				} else {
					$user_photo = createUrl('images/users_photo/default_image.jpg');
				}
				?>
				<img src="<?= $user_photo; ?>" alt=""/>
			</div>
			<div class="photo-title">
				<h4>Персональное фото</h4>

				<p>(Максимальный размер изображения 110x110px, 100Kb.)</p>
			</div>
			<div class="btns-set o-shell">
				<?php $form = $this->beginWidget('CActiveForm', array(
					'id' => 'image-form',
					'enableAjaxValidation' => true,
					'action' => createUrl('//user/profile/edit'),
					'htmlOptions' => array('enctype' => 'multipart/form-data'),
				));
				?>
				<?php
				$profileFields = Profile::getFields();

				if ($profileFields) {
					foreach ($profileFields as $field) {
						if ($field->widget == 'UWfile') {
							if ($widgetEdit = $field->widgetEdit($profile)) {
								echo $widgetEdit;
								echo $form->error($profile, $field->varname);
							}
						}
					}
				}
				?>
				<div style="clear: both"></div>
				<?//TODO: Установить max file Size?>
				<button class="btn btn-undo btn-small pull-left btn-upload" type="button">Выбирите файл</button>
				<?registerScript('uploadPhotoButton', '
	uploadPhotoButton = function (self) {
		var file = $(self).siblings("input[type=file]").val();
		/*if (file == "") {
		    alert("Выбирите файл");
		}
		else*/
		{
		    var data = new FormData($(self).parents("form")[0]);
		    $.ajax({
		        url: "' . $form->action . '",
		        data: data,
		        contentType: false,
		        processData: false,
		        type: "POST",
		        success: function (data) {
					$("#' . $containerId . '").html(data);
		        }
		    });
		}
	}
', CClientScript::POS_READY);?>
				<?= CHtml::htmlButton('Загрузить',
					array(
						'class' => 'btn btn-undo btn-small pull-left load_photo',
						'type' => 'button',
						'onclick' => 'uploadPhotoButton(this)'
					));
				?>
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>