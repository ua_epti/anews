<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Change masterKey");
/*$this->breadcrumbs = array(
	UserModule::t("Profile") => array('/user/profile'),
	UserModule::t("Change password"),
);*/
$this->menu = array(
	((UserModule::isAdmin())
		? array('label' => UserModule::t('Manage Users'), 'url' => array('/user/admin'))
		: array()),
	array('label' => UserModule::t('List User'), 'url' => array('/user')),
	array('label' => UserModule::t('Profile'), 'url' => array('/user/profile')),
	array('label' => UserModule::t('Edit'), 'url' => array('edit')),
	array('label' => UserModule::t('Logout'), 'url' => array('/user/logout')),
);
?>
<?$this->widget('bootstrap.widgets.TbMenu', array(
	'type' => 'tabs',
	'stacked' => false,
	'items' => array(
		array('label' => UserModule::t('Edit'), 'url' => array('edit')),
		array('label' => UserModule::t('Change password'), 'url' => array('changepassword')),
		array('label' => UserModule::t('Change MasterKey'), 'url' => array('changeMasterKey'), 'active' => true),
		array('label' => UserModule::t('Security Log'), 'url' => array('//user/log')),
	),
));
?>

<h1><?= UserModule::t("Change MasterKey"); ?></h1>

<div class="form">
	<? $form = $this->beginWidget('CActiveForm', array(
		'id' => 'changeMasterKey-form',
		'enableAjaxValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
	)); ?>

	<p class="note"><?= UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	<?= $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'oldMasterKey'); ?>
		<?php echo $form->passwordField($model, 'oldMasterKey'); ?>
		<?php echo $form->error($model, 'oldMasterKey'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'masterKey'); ?>
		<?php echo $form->passwordField($model, 'masterKey'); ?>
		<?php echo $form->error($model, 'masterKey'); ?>
		<p class="hint">
			<?= UserModule::t("Minimal password length 6 symbols."); ?>
		</p>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'verifyMasterKey'); ?>
		<?php echo $form->passwordField($model, 'verifyMasterKey'); ?>
		<?php echo $form->error($model, 'verifyMasterKey'); ?>
	</div>


	<div class="row submit">
		<?php echo CHtml::submitButton(UserModule::t("Save")); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->