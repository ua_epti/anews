<? $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Profile"); ?>
<?$this->widget('bootstrap.widgets.TbMenu', array(
	'type' => 'tabs',
	'stacked' => false,
	'items' => array(
		array('label' => UserModule::t('Edit'), 'url' => array('edit')),
		array('label' => UserModule::t('Change password'), 'url' => array('changepassword')),
	),
));
?>
<h1><?php echo UserModule::t('Your profile'); ?></h1>

<?php if (Yii::app()->user->hasFlash('profileMessage')): ?>
	<div class="success">
		<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
	</div>
<?php endif; ?>
<table class="dataGrid">
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('username')); ?></th>
		<td><?php echo CHtml::encode($model->username); ?></td>
	</tr>
	<?php
	$profileFields = ProfileField::model()->forOwner()->sort()->findAll();
	if ($profileFields) {
		foreach ($profileFields as $field) {
			?>
			<tr>
				<th class="label"><?php echo CHtml::encode(UserModule::t($field->title)); ?></th>
				<td><?php echo(($field->widgetView($profile)) ? $field->widgetView($profile) : CHtml::encode((($field->range) ? Profile::range($field->range, $profile->getAttribute($field->varname)) : $profile->getAttribute($field->varname)))); ?></td>
			</tr>
		<?php
		}
	}
	?>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('email')); ?></th>
		<td><?php echo CHtml::encode($model->email); ?></td>
	</tr>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('create_at')); ?></th>
		<td><?php echo $model->create_at; ?></td>
	</tr>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('lastvisit_at')); ?></th>
		<td><?php echo $model->lastvisit_at; ?></td>
	</tr>
	<tr>
		<th class="label"><?php echo CHtml::encode($model->getAttributeLabel('status')); ?></th>
		<td><?php echo CHtml::encode(User::itemAlias("UserStatus", $model->status)); ?></td>
	</tr>
</table>
