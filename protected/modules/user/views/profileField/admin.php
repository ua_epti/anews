<?php
$this->menu = array(
	array('label' => UserModule::t('Create Profile Field'), 'url' => array('//admin/user/profile-field/create')),
	array('label' => UserModule::t('Manage Users'), 'url' => array('/admin/user/admin')),
);
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered condensed',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		array(
			'name' => 'id',
			'type' => 'raw',
			'htmlOptions' => array('width' => '2% !important'),
		),
		/*array(
			'name' => 'varname',
			'type' => 'raw',
			'value' => 'UHtml::markSearch($data,"varname")',
		),*/
		array(
			'name' => 'title',
			'value' => 'UserModule::t($data->title)',
		),
		array(
			'name' => 'field_type',
			'value' => '$data->field_type',
			'filter' => ProfileField::itemAlias("field_type"),
		),
		'field_size',
		//'field_size_min',
		array(
			'name' => 'required',
			'value' => 'ProfileField::itemAlias("required",$data->required)',
			'filter' => ProfileField::itemAlias("required"),
		),
		//'match',
		//'range',
		//'error_message',
		//'other_validator',
		//'default',
//		'position',
		array(
			'name' => 'visible',
			'value' => 'ProfileField::itemAlias("visible",$data->visible)',
			'filter' => ProfileField::itemAlias("visible"),
		),
		//*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>
