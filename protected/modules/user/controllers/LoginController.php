<?php

class LoginController extends Controller
{
	public $layout = '//layouts/lk';

	public $defaultAction = 'login';

	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			/*$service = Yii::app()->request->getQuery('service');
			if (isset($service)) {
				$authIdentity = Yii::app()->eauth->getIdentity($service);
				$authIdentity->redirectUrl = Yii::app()->user->returnUrl;
				$authIdentity->cancelUrl = $this->createAbsoluteUrl($this->route);
				if ($authIdentity->authenticate()) {
					$identity = new ServiceUserIdentity($authIdentity);
					// Успешный вход
					if ($identity->authenticate()) {
						//ищем пользуна или регистрируем нового
						Yii::app()->user->login($identity);

						// Специальный редирект с закрытием popup окна
						$authIdentity->redirect();
					} else {
						// Закрываем popup окно и перенаправляем на cancelUrl
						$authIdentity->cancel();
					}
				}
				// Что-то пошло не так, перенаправляем на страницу входа
				$this->redirect(array('/user/login'));
			}*/

			$model = new UserLogin;
			// collect user input data
			if (isset($_POST['UserLogin'])) {
				$model->attributes = $_POST['UserLogin'];
				if ($model->validate()) {
					$this->lastViset();
					if (user()->isAdmin()) {
						$this->redirect(createUrl('/admin/news/default'));
					} else {
						$this->redirect(createUrl('//news/default/index'));
					}
				} else {
					user()->setFlash('error', UserModule::t('Не верный логин или пароль'));
				}
			}
			$this->render('/user/login', array('model' => $model));
		} else {
			$this->redirect(Yii::app()->controller->module->returnUrl);
		}
	}

	private function lastViset()
	{
		if (!Yii::app()->user->isGuest) {
			$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
			$lastVisit->lastvisit_at = date('Y-m-d H:i:s');
			$lastVisit->save();
		}
	}

	public function actionGetNewCode()
	{
		if (isset($_POST['UserLogin'])) {
			if (strpos($_POST['UserLogin']['username'], "@")) {
				$user = User::model()->notsafe()->findByAttributes(array('email' => $_POST['UserLogin']['username']));
			} else {
				$user = User::model()->notsafe()->findByAttributes(array('username' => $_POST['UserLogin']['username']));
			}
			if ($user !== null) {
				$user->onetime_pass = rand(1000, 9999);
				$user->save(false);
//				sms($user->profile->phone, param('managerLoginSmsCode'), array('#CODE#' => $user->onetime_pass));
				$arResponse['html'] = $this->render('getNewCode', null, true);
				echo CJSON::encode($arResponse);
				yii()->end();
			}
		}
	}
}