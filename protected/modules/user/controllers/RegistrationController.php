<?php

class RegistrationController extends Controller
{
	public $layout = '//layouts/lk';
	public $defaultAction = 'registration';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
		);
	}

	protected function generateRefCode()
	{
		do {
			$refCode = randString(8);
			$exists = User::model()->findByAttributes(array('ref_code' => $refCode));
		} while ($exists !== null);
		return $refCode;
	}

	/**
	 * Registration user
	 */
	public function actionRegistration()
	{
		Profile::$regMode = true;
		$model = new RegistrationForm;
		$profile = new Profile;

		if (Yii::app()->user->id) {
			$this->redirect(Yii::app()->controller->module->profileUrl);
		} else {
			if (isset($_POST['RegistrationForm']) && isset($_POST['Profile'])) {
				$model->Register($_POST, $scenario = 'ajax_register');
				$this->redirect(createUrl('//site/index'));
				yii()->end();
			}
			$this->render('/user/registration', array('model' => $model, 'profile' => $profile));
		}
	}
}