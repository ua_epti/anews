<?php

class ProfileController extends Controller
{
	public $defaultAction = 'profile';
	public $layout = '//layouts/lk';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * Shows a particular model.
	 */
	public function actionProfile()
	{
		$model = $this->loadUser();
		$this->render('profile', array(
			'model' => $model,
			'profile' => $model->profile,
		));
	}

	public function actionEdit()
	{
		$model = $this->loadUser();
		$profile = $model->profile;
		// ajax validator
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
			echo UActiveForm::validate(array($model, $profile));
			Yii::app()->end();
		}

		if (isset($_POST['User']) || isset($_POST['Profile'])) {
			$model->attributes = $_POST['User'];
			$profile->attributes = $_POST['Profile'];

			if ($model->validate() && $profile->validate()) {
				$model->save();
				$profile->save();

				if (isset($_POST['UserChangePassword']) && !empty($_POST['UserChangePassword']['oldPassword'])) {
					$this->forward('changepassword');
				}
				Yii::app()->user->setFlash('success', ("Данные сохранены"));
			} else {
				$profile->validate();
				Yii::app()->user->setFlash('error', ("Произошла ошибка."));
			}
		}

		$this->render('edit', array(
			'model' => $model,
			'profile' => $profile
		));
	}

	public function actionChangepassword()
	{
		$model = new UserChangePassword;
		if (Yii::app()->user->id) {

			// ajax validator
			if (isset($_POST['ajax']) && $_POST['ajax'] === 'changepassword-form') {
				echo UActiveForm::validate($model);
				Yii::app()->end();
			}

			if (isset($_POST['UserChangePassword'])) {
				$model->attributes = $_POST['UserChangePassword'];
				if ($model->validate()) {
					$new_password = User::model()->notsafe()->findbyPk(Yii::app()->user->id);
					$new_password->password = UserModule::encrypting($model->password);
					$new_password->activkey = UserModule::encrypting(microtime() . $model->password);
					$new_password->save();
					Yii::app()->user->setFlash('success', UserModule::t("Новый пароль сохранен."));
				} else {
					$errors = array_reduce(
						$model->getErrors(),
						function ($result, $item) {
							return array_merge($result, array_values($item));
						},
						array()
					);
					Yii::app()->user->setFlash('error', implode('<br>', $errors));
				}
			}
			$this->render('changepassword', array(
				'model' => $model,
			));
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
	 */
	public function loadUser()
	{
		if ($this->_model === null) {
			if (Yii::app()->user->id) {
				$this->_model = Yii::app()->controller->module->user();
			}
			if ($this->_model === null) {
				$this->redirect(Yii::app()->controller->module->loginUrl);
			}
		}
		return $this->_model;
	}
}