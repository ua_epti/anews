<?

class UserController extends Controller
{
	private $_model;

	public function filters()
	{
		return CMap::mergeArray(parent::filters(), array(
			'accessControl',
		));
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('index', 'view'),
				'users' => array('*'),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}

	public function actionView()
	{
		$model = $this->loadModel();
		$this->render('view', array(
			'model' => $model,
		));
	}

	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('User', array(
			'criteria' => array(
				'condition' => 'status>' . User::STATUS_BANNED,
			),

			'pagination' => array(
				'pageSize' => Yii::app()->controller->module->user_page_size,
			),
		));

		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function loadModel()
	{
		if ($this->_model === null) {
			if (isset($_GET['id'])) {
				$this->_model = User::model()->findbyPk($_GET['id']);
			}
			if ($this->_model === null) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}
		return $this->_model;
	}

	public function loadUser($id = null)
	{
		if ($this->_model === null) {
			if ($id !== null || isset($_GET['id'])) {
				$this->_model = User::model()->findbyPk($id !== null ? $id : $_GET['id']);
			}
			if ($this->_model === null) {
				throw new CHttpException(404, 'The requested page does not exist.');
			}
		}
		return $this->_model;
	}
}