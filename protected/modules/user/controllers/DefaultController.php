<?
class DefaultController extends Controller
{
	public $layout = '//layouts/lk';

	public function actionIndex()
	{
		if (user()->isGuest) {
			$this->redirect('//admin/admin/login');
		}
		$this->redirect('news/default/index');
	}
}