<?

class ActivationController extends Controller
{
	public $defaultAction = 'activation';

	/**
	 * Activation user account
	 */
	public function actionActivation()
	{
		$email = $_GET['email'];
		$activkey = $_GET['activkey'];
		if ($email && $activkey) {
			$find = User::model()->findByAttributes(array('email' => $email));
			if (isset($find) && $find->status == User::STATUS_PRE_ACTIVE) {
				yii()->user->setFlash('info', yii::t('registration', "You application is active."));
			} elseif (isset($find) && $find->status == User::STATUS_ACTIVE) {
				yii()->user->setFlash('info', yii::t('registration', "You account is active."));
				UserModule::sendMail(
					$email,
					UserModule::t("You registered from {site_name}", array('{site_name}' => yii()->name)),
					yii::t('registration', "We hereby inform you that the system has been obtained xtrenk application to participate in the selection of participants for the program Battle Masters")
				);
			} elseif (isset($find->activkey) && ($find->activkey == $activkey)) {
				$find->activkey = UserModule::encrypting(microtime());
				$find->status = User::STATUS_PRE_ACTIVE;
				$find->save();
				yii()->user->setFlash('info', yii::t('registration', "You application is active."));
				UserModule::sendMail(
					$email,
					UserModule::t("You registered from {site_name}", array('{site_name}' => yii()->name)),
					yii::t('registration', "We hereby inform you that the system has been obtained xtrenk application to participate in the selection of participants for the program Battle Masters")
				);
			} else {
				yii()->user->setFlash('error', yii::t('registration', "Incorrect activation URL."));
			}
		} else {
			yii()->user->setFlash('error', yii::t('registration', "Incorrect activation URL."));
		}
		$this->redirect(createUrl('//site/index'));
	}
}