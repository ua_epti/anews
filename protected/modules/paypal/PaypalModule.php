<?php

class PaypalModule extends CWebModule
{
	public $defaultController = "Paypal";

	public function init()
	{
		$this->setImport(array(
			'paypal.models.*',
			'paypal.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}
}
