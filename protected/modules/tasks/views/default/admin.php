<?registerScript('viewAction', '
$("#tasks-grid table tbody tr td").live("click", function () {
		if(!$(this).hasClass("checkBoxTD")){
			var id = $(this).parent().find("td:last").text();
			window.location.href = baseUrl + "/tasks/default/view/id/"+id;
		}
	});
')?>
<?registerScript('massAction', "
	function processSelected() {
		var arElemID = new Array();
		$('input[name^=tasks-grid_c0]:checked').each(function(i){
			var id = $(this).val();
			if(id != 1){
				arElemID[i] = $(this).val();
			}
		});
		return arElemID;
	}")?>


	<div class="form-action">
		<?= CHtml::link('Создать', createUrl('//tasks/default/create'), array('class' => 'btn btn-success')) ?>
		<?=
		CHtml::ajaxLink(
			'В Архив',
			createUrl("//tasks/default/status"),
			array(
				'type' => 'POST',
				'async' => 'false',
				'data' => array(
					'Tasks' => array(
						'arID' => 'js:processSelected()',
						'action' => 'statusArchive'
					),
				),
				'success' => 'function(data){
			$.fn.yiiGridView.update("tasks-grid", {
				data: $(this).serialize()
			});
		}'
			),
			array('class' => 'btn btn-danger')) ?>
	</div>

<?$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'tasks-grid',
	'itemsCssClass' => 'striped',
	'dataProvider' => $model->search(),
	'selectableRows' => 2,
	'columns' => array(
		array(
			'class' => 'CCheckBoxColumn',
			'htmlOptions' => array(
				'width' => '2%',
				'class' => 'checkBoxTD'
			),
		),
		array(
			'name' => 'managerID',
			'type' => 'raw',
			'value' => 'User::model()->getUserNameByPk($data->managerID)',
		),
		array(
			'name' => 'title',
			'header' => 'Заголовок',
			'type' => 'raw',
			'value' => 'Tasks::model()->getTitleByLocale("ru")',
		),
		array(
			'name' => 'timeCreate',
			'header' => 'Дата создания',
			'type' => 'raw',
			'value' => 'yii()->dateFormatter->formatDateTime("$data->timeCreate")',
		),
		array(
			'name' => 'timeVideo',
			'header' => 'Время Видео',
			'type' => 'raw',
		),
		array(
			'name' => 'price',
			'header' => 'Цена',
			'type' => 'raw',
		),
		array(
			'name' => 'id',
			'header' => '',
			'type' => 'raw',
			'value' => '$data->id',
			'htmlOptions' => array('style' => 'display:none;'),
			'headerHtmlOptions' => array('size' => 0, 'style' => 'display:none;'),
		),
	),
));