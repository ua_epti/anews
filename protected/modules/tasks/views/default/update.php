<h1>Update Tasks <?php echo $model->id; ?></h1>

<?php
/**
 * @var $form TbActiveForm
 * @var $model Tasks
 */
?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'tasks-form',
	'enableAjaxValidation' => false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php $tabs = array();
foreach ($translations as $locale => $content) {
	$language = Yii::app()->cms->languages[$locale];
	$tab = array(
		'content' => $this->renderPartial('_form', array(
				'model' => $content,
				'form' => $form,
				'node' => $model,
				'language' => $language,
			), true),
	);
	$tabs[$language] = $tab;
} ?>

<?php $this->widget('zii.widgets.jui.CJuiTabs', array(
	'headerTemplate' => '<li><a href="{url}">{title}</a></li>',
	'tabs' => $tabs,
	'htmlOptions' => array('style' => 'padding: 20px')
)); ?>
<?php //echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 100)); ?>

<?php //echo $form->textFieldRow($model, 'description', array('class' => 'span5', 'maxlength' => 5000)); ?>

<?= $form->textFieldRow($model, 'map_image', array('id' => 'address')) ?>

<?php echo $form->textFieldRow($model, 'timeVideo', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'price', array('class' => 'span5', 'maxlength' => 15)); ?>

<?php //echo $form->checkBoxRow($model, 'is_publish', array('class' => '')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => $model->isNewRecord ? 'Create' : 'Save',
	)); ?>
</div>

<?php $this->endWidget(); ?>
