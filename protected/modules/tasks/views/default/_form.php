<fieldset class="form-content">
	<div class="row">
		<?php echo $form->labelEx($model, 'title') ?>
		<?php $this->widget('cms.widgets.markitup.CmsMarkItUp', array(
			'model' => $model,
			'attribute' => '[' . $model->locale . ']title',
			'set' => 'default',
			'htmlOptions' => array('id' => 'title-' . $model->locale, 'class' => 'cms_title'),
		)) ?>
		<?php echo $form->error($model, '[' . $model->locale . ']title') ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, '[' . $model->locale . ']content') ?>
		<?php $this->widget('cms.widgets.markitup.CmsMarkItUp', array(
			'model' => $model,
			'attribute' => '[' . $model->locale . ']content',
			'set' => 'default',
			'htmlOptions' => array('id' => 'content-' . $model->locale, 'class' => 'cms_content'),
		)) ?>
		<?php echo $form->error($model, '[' . $model->locale . ']content') ?>
	</div>
</fieldset>