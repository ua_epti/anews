<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('managerID')); ?>:</b>
	<?php echo CHtml::encode($data->managerID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userID')); ?>:</b>
	<?php echo CHtml::encode($data->userID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeVideo')); ?>:</b>
	<?php echo CHtml::encode($data->timeVideo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('is_avaliable')); ?>:</b>
	<?php echo CHtml::encode($data->is_avaliable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeCreate')); ?>:</b>
	<?php echo CHtml::encode($data->timeCreate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timeUpdate')); ?>:</b>
	<?php echo CHtml::encode($data->timeUpdate); ?>
	<br />

	*/ ?>

</div>