<?
class DefaultController extends Controller
{
	public $layout = '//layouts/tasks';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionIndex()
	{
		$model = new Tasks('searchByManager');
		$model->unsetAttributes();
		if (isset($_GET['Tasks'])) {
			$model->attributes = $_GET['Tasks'];
		}
		$this->render('admin', array('model' => $model));
	}

	public function actionStatus()
	{
		if (yii()->request->isAjaxRequest) {
			if (isset($_POST['Tasks']['arID'])) {
				$this->processMassAction($_POST['Tasks']['action'], $_POST['Tasks']['arID']);
			} elseif (!isset($_POST['Tasks']['arID']) && $_POST['Tasks']['action'] == 'statusPaid') {
				echo 'error';
				yii()->end();
			}
		}
	}

	private function processMassAction($action, $arID)
	{
		switch ($action) {
			case 'statusArchive':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_publish = Tasks::CLOSE;
						$actModel->save(false);
					}
				}
				break;
		}
	}

	public function actionView($id)
	{
		$this->render('view', array('model' => $this->loadModel($id)));
	}

	public function actionArchive()
	{
		$model = new Tasks('archive');
		$model->unsetAttributes();
		if (isset($_GET['Tasks'])) {
			$model->attributes = $_GET['Tasks'];
		}
		$this->render('archive', array(
			'dataProvider' => $model->archive()
		));
	}

	public function actionCreate()
	{
		$model = new Tasks('create');
		$model->title = uniqid('uniqID-');
		if ($model->save()) {
			user()->setFlash('success', Yii::t('site', 'Tasks Create.'));
			$this->redirect(array('update', 'id' => $model->id));
		} elseif (YII_DEBUG) {
			dump($model->getErrors());
			die;
		}
	}

	public function actionUpdate($id)
	{
		if (!empty($id)) {
			$model = $this->loadModel($id);
			$model->scenario = 'task';
			$translations = array();
			foreach (yii()->sourceLanguage as $language) {
				$content = $model->getContent($language);
				if ($content === null) {
					$content = $model->createContent($language);
				}
				$translations[$language] = $content;
			}

			$this->performAjaxValidation($model);
			if (isset($_POST['Tasks']) && isset($_POST['TasksContent'])) {
				foreach ($translations as $language => $content) {
					$content->attributes = $_POST['TasksContent'][$language];
					$content->taskID = $id;
					$valid = $content->validate($model);
					$translations[$language] = $content;
				}


				if ($valid) {
					$model->attributes = $_POST['Tasks'];
					$model->save(); // we need to save the node so that the updated column is updated

					foreach ($translations as $content)
						$content->save();
					user()->setFlash('success', Yii::t('site', 'Tasks updated.'));
					$this->redirect(array('update', 'id' => $id));
				} elseif (YII_DEBUG) {
					dump($content->getErrors());
					die;
				}
			}

			$this->render('update', array(
				'model' => $model,
				'translations' => $translations,
			));
		}
	}

	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel($id)->delete();
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	public function loadModel($id)
	{
		$model = Tasks::model()->with(array('content'))->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'tasks-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}