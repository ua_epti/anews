<?php

class TasksModule extends CWebModule
{
	public $defaultController = "Default";

	public function init()
	{
		$this->setImport(array(
			'application.modules.tasks.models.*',
			'application.modules.user.models.User',
			'tasks.components.*',
			'tasks.components.*',
		));
		registerScriptFile('http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-GB');
		registerScript('googleInit', '
			var autocomplete = new google.maps.places.Autocomplete($("#address")[0], {});
			google.maps.event.addListener(autocomplete, "place_changed", function () {
				var place = autocomplete.getPlace();
				console.log(place.address_components);
			});
		');
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}
}
