<?php

/**
 * This is the model class for table "{{tasks}}".
 *
 * The followings are the available columns in table '{{tasks}}':
 * @property integer $id
 * @property integer $managerID
 * @property integer $userID
 * @property string $title
 * @property string $description
 * @property integer $timeVideo
 * @property string $price
 * @property integer $is_publish
 * @property integer $timeCreate
 * @property integer $timeUpdate
 *
 * The followings are the available model relations:
 * @property User $manager
 * @property User $user
 */

class Tasks extends CActiveRecord
{

	public $upload;

	const PUBLISH_FALSE = 0;
	const PUBLISH_TRUE = 1;
	const CLOSE = 2;

	public static $STATUSES = array(
		self::PUBLISH_FALSE => 'Не публиковать',
		self::PUBLISH_TRUE => 'Опубликована',
		self::CLOSE => 'Архив',
	);

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{tasks}}';
	}

	public function rules()
	{
		return array(
			array('title', 'required', 'on' => 'create'),
			array('managerID, userID, contentID, is_publish,timeVideo, price,timeCreate, timeUpdate', 'numerical', 'integerOnly' => true),
			array('title', 'length', 'max' => 20),
//			array('price', 'length', 'max' => 15),
			array('map_image', 'length', 'max' => 500),
			array('map_address', 'length', 'max' => 1000),
			array('id, managerID, userID, title, timeVideo, price, is_publish, timeCreate, timeUpdate', 'safe', 'on' => 'search'),
			array('id, managerID, userID, title, timeVideo, price, is_publish, timeCreate, timeUpdate', 'safe', 'on' => 'searchByManager'),
			array('id, managerID, userID, title, timeVideo, price, is_publish, timeCreate, timeUpdate', 'safe', 'on' => 'archive'),
		);
	}

	public function relations()
	{
		return array(
			'manager' => array(self::BELONGS_TO, 'Users', 'managerID'),
			'user' => array(self::BELONGS_TO, 'Users', 'userID'),
			'content' => array(self::BELONGS_TO, 'TasksContent', array('id' => 'taskID')),
		);
	}


	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'managerID' => 'Менеджер',
			'userID' => 'Пользвотваель',
			'title' => 'Название',
			'timeVideo' => 'Врмя видео',
			'map_image' => 'Фрагмент карты',
			'map_address' => 'Адрес',
			'price' => 'Стоимость',
			'is_publish' => 'Опубликовать',
			'timeCreate' => 'Время создания',
			'timeUpdate' => 'Время обнволения',
		);
	}

	public function getContent($locale)
	{
		return $model = TasksContent::model()->findByAttributes(array(
			'taskID' => $this->id,
			'locale' => $locale,
		));
	}

	public function getContentByLocale($locale = null)
	{
		$lang = '';
		if ($locale == null) {
			$lang = 'ru';
		} else {
			$lang = $locale;
		}
		$model = TasksContent::model()->findByAttributes(array(
			'taskID' => $this->id,
			'locale' => $lang,
		));
		return $model->content;
	}

	public function getTitle($locale)
	{
		return $model = TasksContent::model()->findByAttributes(array(
			'taskID' => $this->id,
			'locale' => $locale,
		));
	}

	public function getTitleByLocale($locale)
	{
		$model = TasksContent::model()->findByAttributes(array(
			'taskID' => $this->id,
			'locale' => $locale,
		));

		return $model->title;
	}

	public function createContent($locale)
	{
		$content = new TasksContent();
		$content->taskID = $this->id;
		$content->locale = $locale;
		$content->save();
		return $content;
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('timeVideo', $this->timeVideo);
		$criteria->compare('price', $this->price, true);
		$criteria->compare('timeCreate', $this->timeCreate);
		$criteria->compare('timeUpdate', $this->timeUpdate);
		$criteria->condition = 't.is_publish<>' . self::CLOSE;
		/*if (user()->isJuniorManager(user()->id) || user()->isMiddleManager(user()->id) || user()->isHighManager(user()->id)) {
			$is_publish = $this->checkLevelAccess();

		}*/

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function archive()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('timeVideo', $this->timeVideo);
		$criteria->compare('price', $this->price, true);
		$criteria->compare('timeCreate', $this->timeCreate);
		$criteria->compare('timeUpdate', $this->timeUpdate);
		$criteria->condition = 't.is_publish=:is_publish';
		$criteria->params = array(':is_publish' => self::CLOSE);
		/*if (user()->isJuniorManager(user()->id) || user()->isMiddleManager(user()->id) || user()->isHighManager(user()->id)) {
			$is_publish = $this->checkLevelAccess();

		}*/

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->isNewRecord || empty($this->timeCreate)) {
				$this->timeCreate = time();
			}
			$this->managerID = user()->id;
			return true;
		}
		return false;
	}
}