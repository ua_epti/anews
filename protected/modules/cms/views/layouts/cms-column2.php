<? $this->beginContent(Yii::app()->cms->appLayout) ?>
	<div class="cms bp span12">
		<div class="span8">
			<div class="cms-content">
				<?= $content ?>
			</div>
		</div>
		<div class="span3">
			<div class="cms-sidebar">
				<?= Yii::t('CmsModule.core', 'Nodes') ?>
				<?= CmsNode::model()->renderTree() ?>
				<p><?= CHtml::link(Yii::t('CmsModule.core', 'Create a new node'), array('node/create')) ?></p>
			</div>
		</div>
	</div>
<? $this->endContent() ?>