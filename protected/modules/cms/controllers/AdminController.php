<?php
/**
 * AdminController class file.
 * @author Christoffer Niska <christoffer.niska@nordsoftware.com>
 * @copyright Copyright &copy; 2011, Nord Software Ltd
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package cms.controllers
 * @since 0.9.1
 */

class AdminController extends CmsController
{
	public function actionIndex()
	{
		$this->render('index');
	}

	/*public function createMultilanguageReturnUrl($lang = 'en')
	{
		if (count($_GET) > 0) {
			$arr = $_GET;
			$arr['language'] = $lang;
		} else {
			$arr = array('language' => $lang);
		}
		return $this->createUrl('', $arr);
	}*/
}
