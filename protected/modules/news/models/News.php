<?php

/**
 * This is the model class for table "{{news}}".
 *
 * The followings are the available columns in table '{{news}}':
 * @property integer $id
 * @property integer $userID
 * @property integer $managerID
 * @property integer $is_avaliable
 * @property integer $imageID
 * @property integer $video
 * @property integer $contentID
 * @property integer $timeCreate
 *
 * The followings are the available model relations:
 * @property User $user
 * @property User $manager
 * @property UserPaymentHistory[] $userPaymentHistories
 */
class News extends CActiveRecord
{
	const DEFAULT_STATUS = 0;
	const CONFIRM_STEP_1 = 1;
	const CONFIRM_STEP_2 = 2;
	const CONFIRM_STEP_3 = 3;
	const CONFIRM_STEP_4 = 4;
	const STRIP = 5;
	const ARCHIVE = 6;
	const BANNED = -1;
	const PAY_ERROR = -2;
//	const DOWNLOAD = 999;

	public $image;
	public $video;

	public static $STATUSES = array(
		self::DEFAULT_STATUS => 'Ожидает модерации',
		self::CONFIRM_STEP_1 => '1 уровень модерации',
		self::CONFIRM_STEP_2 => '2 уровень модерации',
		self::CONFIRM_STEP_3 => 'Готов к оплате',
		self::CONFIRM_STEP_4 => 'Оплачено',
		self::STRIP => 'Лента',
		self::BANNED => 'Заблокированно',
		self::ARCHIVE => 'Архив',
		self::PAY_ERROR => 'Ошибка оплаты',
	);

	public static $STATUS_FLOW = array(
		self::DEFAULT_STATUS => array(
			self::DEFAULT_STATUS,
			self::CONFIRM_STEP_1,
//			self::BANNED,
			self::ARCHIVE,
		),
		self::CONFIRM_STEP_1 => array(
			self::DEFAULT_STATUS,
			self::CONFIRM_STEP_2,
//			self::BANNED,
			self::ARCHIVE,
		),
		self::CONFIRM_STEP_2 => array(
			self::CONFIRM_STEP_1,
			self::CONFIRM_STEP_3,
//			self::BANNED,
			self::ARCHIVE,
		),
		self::CONFIRM_STEP_3 => array(
			self::CONFIRM_STEP_2,
			self::CONFIRM_STEP_4,
//			self::BANNED,
			self::ARCHIVE,
		),
		self::CONFIRM_STEP_4 => array(
			self::STRIP,
		),
		self::STRIP => array(
			self::CONFIRM_STEP_4,
			self::ARCHIVE,
		),
		self::PAY_ERROR => array(
			self::CONFIRM_STEP_4,
			self::CONFIRM_STEP_3,
			self::ARCHIVE,
		),
		self::ARCHIVE => array(
			self::CONFIRM_STEP_1,
			self::CONFIRM_STEP_2,
		),
	);

	public function getAvailableStatuses($is_available)
	{
		$arAvailableStatuses = array();
		if ($is_available != null) {
			foreach (News::$STATUS_FLOW[$is_available] as $status) {
				$arAvailableStatuses[$status] = News::$STATUSES[$status];
			}
		} else {
			foreach (News::$STATUS_FLOW[self::DEFAULT_STATUS] as $status) {
				$arAvailableStatuses[$status] = News::$STATUSES[$status];
			}
		}
		return $arAvailableStatuses;
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{news}}';
	}

	public function rules()
	{
		return array(
			array('', 'required'),
			array('userID, managerID, is_avaliable, viewers, timeCreate,is_anonym', 'numerical', 'integerOnly' => true),
			array('image, thumbnail,video', 'length', 'max' => 500),
			array('token', 'length', 'max' => 255),
			array('title,address,paypal_status', 'length', 'max' => 1000),
			array('content', 'length', 'max' => 10000),
			array('locale,latitude,longitude', 'length', 'max' => 50),

			array('id, userID, managerID, is_avaliable,video,title, content, timeCreate', 'safe', 'on' => 'search'),

			array('id, userID, managerID, is_avaliable,video,title, content, timeCreate', 'safe', 'on' => 'moderate'),

			array('id, userID, managerID, is_avaliable,video,title, content, timeCreate', 'safe', 'on' => 'first'),

			array('id, userID, managerID, is_avaliable,video,title, content, timeCreate', 'safe', 'on' => 'second'),

			array('id, userID, managerID, is_avaliable,video,title, content, timeCreate', 'safe', 'on' => 'third'),

			array('id, userID, managerID, is_avaliable,video,title, content, timeCreate', 'safe', 'on' => 'archive'),

			array('id, userID, managerID, is_avaliable,video,title, content, timeCreate', 'safe', 'on' => 'strip'),

			array('id, userID, managerID, is_avaliable,video,title, content, timeCreate', 'safe', 'on' => 'paid'),

			array('id, userID, managerID, is_avaliable,video,title, content, timeCreate', 'safe', 'on' => 'payError'),
		);
	}

	public function is_visible($is_available)
	{
		if ($is_available == News::CONFIRM_STEP_3) {
			$result = true;
		} else {
			$result = false;
		}
		return $result;

	}

	public function relations()
	{
		return array(
			'author' => array(self::BELONGS_TO, 'User', 'userID'),
			'manager' => array(self::BELONGS_TO, 'User', 'managerID'),
			'like' => array(self::BELONGS_TO, 'NewsLike', 'newsID'),
			'userPaymentHistories' => array(self::HAS_MANY, 'UserPaymentHistory', 'newsID'),
			'task' => array(self::BELONGS_TO, 'Tasks', array('taskID' => 'id')),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userID' => 'Пользователь',
			'managerID' => 'Менеджер',
			'is_avaliable' => 'Статус',
			'title' => 'Заголовок',
			'Viewers' => 'Просмотров',
			'video' => 'Видео',
			'content' => 'Описание',
			'locale' => 'Язык',
			'timeCreate' => 'Время создания',
			'image' => 'Фото',
			'thumbnail' => 'Превью',
			'latitude' => 'Ширтоа',
			'longitude' => 'Долгота',
			'is_anonym' => 'Анонимность',
			'paypal_status' => 'Статус PayPal',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);

		if (user()->isJuniorManager(user()->id) || user()->isMiddleManager(user()->id) || user()->isHighManager(user()->id)) {
			$is_avaliable = $this->checkLevelAccess();
			if (is_array($is_avaliable)) {
				$str = '';
				$count = count($is_avaliable);
				foreach ($is_avaliable as $key => $val) {
					if ($key == ($count - 1)) {
						$str .= $val;
					} else {
						$str .= $val . ',';
					}
				}
				$criteria->condition = 't.is_avaliable in(' . $str . ')';
			} else {
				$criteria->condition = 't.is_avaliable=:is_available';
				$criteria->params = array(':is_available' => $is_avaliable);
			}
		}
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function moderate()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);

		$criteria->condition = 't.is_avaliable=:is_avaliable';
		$criteria->params = array(
			':is_avaliable' => self::DEFAULT_STATUS,
		);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function first()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);

		$criteria->condition = 't.is_avaliable=:is_avaliable';
		$criteria->params = array(
			':is_avaliable' => self::CONFIRM_STEP_1
		);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function second()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);

		$criteria->condition = 't.is_avaliable=:is_avaliable';
		$criteria->params = array(':is_avaliable' => self::CONFIRM_STEP_2);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function third()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);

		$criteria->condition = 't.is_avaliable=:is_avaliable';
		$criteria->params = array(':is_avaliable' => self::CONFIRM_STEP_3);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function archive()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);

		$criteria->condition = 't.is_avaliable=:is_avaliable';
		$criteria->params = array(':is_avaliable' => self::ARCHIVE);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/*Лента новостей - идет в на клиента через апи*/
	public function strip()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('viewers', $this->viewers, true);

		$criteria->condition = 't.is_avaliable=:is_avaliable';
		$criteria->params = array(':is_avaliable' => self::STRIP);

		$criteria->order = 't.viewers DESC';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function paid()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);

		$criteria->condition = 't.is_avaliable=:is_avaliable';
		$criteria->params = array(':is_avaliable' => self::CONFIRM_STEP_4);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function payError()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('managerID', $this->managerID);
		$criteria->compare('userID', $this->userID);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);

		$criteria->condition = 't.is_avaliable=:is_avaliable';
		$criteria->params = array(':is_avaliable' => self::PAY_ERROR);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	protected function checkLevelAccess()
	{
		$is_avaliable = self::DEFAULT_STATUS;
		if (user()->isJuniorManager(user()->id)) {
			$is_avaliable = self::DEFAULT_STATUS;
		} elseif (user()->isMiddleManager(user()->id)) {
			$is_avaliable = self::CONFIRM_STEP_1;
		} elseif (user()->isHighManager(user()->id)) {
			$is_avaliable = array(self::CONFIRM_STEP_2, self::CONFIRM_STEP_3);
		}
		return $is_avaliable;
	}

	public function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->isNewRecord || empty($this->timeCreate)) {
				$this->timeCreate = time();
			}
			if ($this->userID == null || empty($this->userID)) {
				$this->userID = user()->id;
			}
			if (!user()->isGuest) {
				$this->managerID = user()->id;
				return true;
			}
		}
		return false;
	}
}