<?php

/**
 * This is the model class for table "{{news}}".
 *
 * The followings are the available columns in table '{{news}}':
 * @property integer $id
 * @property integer $userID
 * @property integer $managerID
 * @property integer $is_avaliable
 * @property integer $imageID
 * @property integer $video
 * @property integer $contentID
 * @property integer $timeCreate
 *
 * The followings are the available model relations:
 * @property User $user
 * @property User $manager
 * @property UserPaymentHistory[] $userPaymentHistories
 */
class NewsLikes extends CActiveRecord
{

	const NEWS_LIKE = 1;
	const NEWS_UNLIKE = 0;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{news_likes}}';
	}

	public function rules()
	{
		return array(
			array('newsID, userID', 'required'),
			array('userID, newsID, like', 'numerical', 'integerOnly' => true),
			array('token, ', 'length', 'max' => 255),
		);
	}

	public function relations()
	{
		return array();
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'userID' => 'Пользователь',
			'newsID' => 'Новость',
			'like' => 'Лайк',
		);
	}
}