<?php
/**
 * This is the model class for table "{{content}}".
 *
 * The followings are the available columns in table '{{content}}':
 * @property integer $id
 * @property integer $newsID
 * @property string $locale
 * @property string $title
 * @property string $content
 *
 * The followings are the available model relations:
 * @property News $news
 */
class NewsContent extends CActiveRecord
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{news_content}}';
	}

	public function rules()
	{
		return array(
			array('newsID, locale, title, content', 'required', 'on' => 'task'),
			array('newsID', 'numerical', 'integerOnly' => true),
			array('locale', 'length', 'max' => 20),
			/*array('model', 'length', 'max' => 50),*/
			array('title', 'length', 'max' => 1000),
			array('content', 'length', 'max' => 10000),
			array('id, newsID, is_avaliable, locale, title, content', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		/*return array(
			'news' => array(self::BELONGS_TO, 'News', 'newsID'),
		);*/
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'newsID' => 'News',
			'locale' => 'Locale',
			'title' => 'Title',
			'content' => 'Content',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('newsID', $this->newsID);
		$criteria->compare('locale', $this->locale, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('content', $this->content, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}