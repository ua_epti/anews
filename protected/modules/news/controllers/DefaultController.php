<?

yii::import('application.modules.payment.models.UserPaymentHistory');
class DefaultController extends Controller
{
	public $layout = '//layouts/news';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionIndex()
	{

		$model = new News('moderate');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('admin', array('dataProvider' => $model->moderate()));
	}

	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	public function actionArchiveDelete()
	{
		if (yii()->request->isAjaxRequest) {
			if (isset($_POST['archiveName'])) {
				$archiveName = $_POST['archiveName'];
				if (!empty($archiveName) && $archiveName != '') {
					yii()->storage->deleteTempArchive($archiveName);
					dump('ok');
					yii()->end();
				}
				dump('error');
				yii()->end();
			}
		}
	}

	public function actionArchive()
	{
		$model = new News('archive');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('archive', array('dataProvider' => $model->archive()));
	}

	public function actionCreate()
	{
		$this->forward('update');
	}

	public function actionFirstLvl()
	{
		$model = new News('first');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		if (yii()->request->isAjaxRequest) {
			if (isset($_POST['News']['arID'])) {
				$this->processMassAction($_POST['News']['action'], $_POST['News']['arID']);
			}
		}
		$this->render('firstlvl', array('dataProvider' => $model->first()));
	}

	public function actionSecondLvl()
	{
		$model = new News('second');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('secondlvl', array('dataProvider' => $model->second()));
	}

	public function actionThirdLvl()
	{
		$model = new News('third');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('beforePaid', array('dataProvider' => $model->third()));
	}

	public function actionRequestPayment($id)
	{
		if (!empty($id)) {
			if (isset($_POST['price'])) {
				$news = News::model()->findByPk($id);
				if ($news->task == null) {
					$news->dop_price = $_POST['price'];
					if ($news->save()) {
						/*PayPalActionPay*/
						$query['data'] = array(
							'userEmail' => $news->author->paypalemail,
							'amount' => $news->dop_price,
							'newsID' => $news->id,
						);
						$resArray = yii()->PayPal->Pay($query);
						$isSuccess = yii()->PayPal->isCallSucceeded($resArray);
						$payPalMessage = yii()->PayPal->GetErrorMessage($resArray);
						if ($isSuccess) {
							$news->is_avaliable = News::CONFIRM_STEP_4;
							if ($news->save()) {
								UserState::model()->setUserState($news->userID, $query['data']['amount']);
								UserPaymentHistory::setUserHistory($news);
								$this->redirect('/admin/news/default/thirdLvl');
							}
						} else {
							$news->is_avaliable = News::PAY_ERROR;
							$news->paypal_status = $payPalMessage;
							if ($news->save()) {
								$this->redirect('/admin/news/default/thirdLvl');
							}
						}
					} elseif (YII_DEBUG) {
						dump($news->getErrors());
						die;
					}
				} else {
					if (!empty($news->taskID)) {
						$task = Tasks::model()->findByPk($news->taskID);
						/*PayPalActionPay*/
						$query['data'] = array(
							'userEmail' => $news->author->paypalemail,
							'amount' => $task->price,
							'newsID' => $news->id,
						);

						$resArray = yii()->PayPal->Pay($query);
						$isSuccess = yii()->PayPal->isCallSucceeded($resArray);
						$payPalMessage = yii()->PayPal->GetErrorMessage($resArray);
						if ($isSuccess) {
							$news->is_avaliable = News::CONFIRM_STEP_4;
							if ($news->save()) {
								UserState::model()->setUserState($news->userID, $query['data']['amount']);
								UserPaymentHistory::setUserHistory($news);
								$this->redirect('/admin/news/default/thirdLvl');
							}
						} else {
							$news->is_avaliable = News::PAY_ERROR;
							$news->paypal_status = $payPalMessage;
							if ($news->save()) {
								$this->redirect('/admin/news/default/thirdLvl');
							}
						}
					}
				}
			}
		}
	}

	public function actionPaid()
	{
		$model = new News('paid');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('paid', array('dataProvider' => $model->paid()));
	}

	public function actionPayError()
	{
		$model = new News('payError');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('inbox', array('dataProvider' => $model->payError()));
	}

	public function actionStrip()
	{
		$this->layout = '//layouts/strip';
		$model = new News('strip');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		$this->render('strip', array('model' => $model));
	}

	public function actionUpdate($id = null)
	{

		if ($id == null) {
			$model = new News();
		} else {
			$model = $this->loadModel($id);
		}
		$this->performAjaxValidation($model);
		if (isset($_POST['News'])) {
			$model->attributes = $_POST['News'];
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = News::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'news-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}