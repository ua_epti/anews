<?
class NewsStatusController extends Controller
{
	public $layout = '//layouts/news';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionIndex()
	{
		$model = new News('moderate');
		$model->unsetAttributes();
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}
		if (yii()->request->isAjaxRequest) {
			if (isset($_POST['News']['arID'])) {
				if ($_POST['News']['action'] == 'statusPaid') {

					if (is_array($_POST['News']['arID'])) {
						foreach (($_POST['News']['arID']) as $key => $id) {
							if ($id == '') {
								unset($_POST['News']['arID'][$key]);
							}
						}
					}
					$newsID = array_values($_POST['News']['arID']);
					$this->renderPartial("mass", array(
						'id' => $newsID[0],
					), false, true);
					yii()->end();
				}
				$this->processMassAction($_POST['News']['action'], $_POST['News']['arID']);
			} elseif (!isset($_POST['News']['arID']) && $_POST['News']['action'] == 'statusPaid') {
				echo 'error';
				yii()->end();
			}
		}
	}

	public function actionSingle($id)
	{
		if (yii()->request->isAjaxRequest) {
			$action = yii()->request->getParam('action');
			if (!empty($action)) {
				if ($action == 'statusPaid') {
					$this->renderPartial("mass", array(
						'id' => $id,
					), false, true);
					yii()->end();
				}
				$this->processSingleAction($action, $id);
			} elseif (!isset($_POST['News']['arID']) && $_POST['News']['action'] == 'statusPaid') {
				echo 'error';
				yii()->end();
			}
		}
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	public function loadModel($id)
	{
		$model = News::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'news-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	private function processSingleAction($action, $id)
	{
		switch ($action) {
			case 'statusModerate':
				$actModel = $this->loadModel($id);
				$actModel->is_avaliable = News::DEFAULT_STATUS;
				$actModel->save(false);
				break;
			case 'statusFirstLvl':
				$actModel = $this->loadModel($id);
				$actModel->is_avaliable = News::CONFIRM_STEP_1;
				$actModel->save(false);
				break;
			case 'statusSecondLvl':
				$actModel = $this->loadModel($id);
				$actModel->is_avaliable = News::CONFIRM_STEP_2;
				$actModel->save(false);
				break;
			case 'statusBeforePaid':
				$actModel = $this->loadModel($id);
				$actModel->is_avaliable = News::CONFIRM_STEP_3;
				$actModel->save(false);
				break;
			case 'statusPaid':

				$actModel = $this->loadModel($id);
				$actModel->is_avaliable = News::CONFIRM_STEP_4;
				$actModel->save(false);
				break;
			case 'statusArchive':

				$actModel = $this->loadModel($id);
				$actModel->is_avaliable = News::ARCHIVE;
				$actModel->save(false);
				break;
			case 'statusStrip':

				$actModel = $this->loadModel($id);
				$actModel->is_avaliable = News::STRIP;
				$actModel->save(false);
				break;
			case 'statusBanned':
				$actModel = $this->loadModel($id);
				$actModel->is_avaliable = News::ARCHIVE;
				$actModel->save(false);
				break;
			case 'download':
				$arFileNames = array();

				$model = $this->loadModel($id);
				if ($model->image != '') {
					$arFileNames[] = yii()->storage->getFilePath($model->image);
				} elseif ($model->video != '') {
					$arFileNames[] = yii()->storage->getFilePath($model->video);
				}

				if (!empty($arFileNames)) {
					$arResp = array();
					$zipName = yii()->storage->createZipArchive($arFileNames);
					if (is_file($zipName)) {
						$arResp = array('code' => '200', 'url' => $zipName);
						echo CJSON::encode($arResp);
					} else {
						$arResp = array('code' => '400', 'url' => '');
						echo CJSON::encode($arResp);
					}
					yii()->end();
				}
				break;
		}
	}

	private function processMassAction($action, $arID)
	{
		switch ($action) {
			case 'statusModerate':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::DEFAULT_STATUS;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'statusFirstLvl':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::CONFIRM_STEP_1;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'statusSecondLvl':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::CONFIRM_STEP_2;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'statusBeforePaid':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::CONFIRM_STEP_3;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'statusPaid':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::CONFIRM_STEP_4;
						$actModel->save(false);
					}
				}
				break;
			case 'statusArchive':
				foreach ($arID as $id) {
					if ($id != '') {
						$actModel = $this->loadModel($id);
						$actModel->is_avaliable = News::ARCHIVE;
						$actModel->save(false);
					}
				}
				echo $this->widget("ext.sideBarMenu.SideBarMenuWidget", array(), true);
				yii()->end();
				break;
			case 'download':
				$arFileNames = array();
				foreach ($arID as $id) {
					if ($id != '') {
						$model = $this->loadModel($id);
						if ($model->image != '') {
							$arFileNames[] = yii()->storage->getFilePath($model->image);
						} elseif ($model->video != '') {
							$arFileNames[] = yii()->storage->getFilePath($model->video);
						}
					}
				}
				if (!empty($arFileNames)) {
					$arResp = array();
					$zipResult = yii()->storage->createZipArchive($arFileNames);
					if (is_file($zipResult['zip_path'])) {
						$arResp = array('code' => '200', 'url' => yii()->controller->createAbsoluteUrl('/images/news/' . $zipResult['zip_name']));
						echo CJSON::encode($arResp);
					} else {
						$arResp = array('code' => '400', 'error' => $zipResult['error']);
						echo CJSON::encode($arResp);
					}
					yii()->end();
				}
				break;
		}
	}
}