<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 26.01.14
 * Time: 19:29
 */
?>
<? if (!empty($id)): ?>
	<?= CHtml::beginForm(createUrl('//news/default/RequestPayment', array('id' => $id)), 'POST', array()) ?>
	<table class="detail-view table table-striped table-condensed">
		<tbody>
		<? $model = News::model()->with('author')->findByPk($id) ?>
		<tr class="odd">
			<th>User</th>
			<td><?= $model->author->username ?></td>
		</tr>
		<tr class="even">
			<th>Price</th>
			<td>
				<?
				if ($model->task->price == null || $model->task->price == 0.00) {
					echo CHtml::textField('price', '', array('placeholder' => 'Введите сумму'));
				} else {
					echo $model->task->price;
					echo CHtml::hiddenField('price', $model->task->price, array('placeholder' => 'Введите сумму'));
				}
				?>
			</td>
		</tr>
		</tbody>
	</table>
<? endif; ?>
<?= CHtml::submitButton('Оплатить', array('class' => 'btn btn-success')) ?>
<?= CHtml::endForm() ?>
<?= CHtml::link('Отмена', createUrl("//news/default/index"), array('class' => 'btn btn-danger')) ?>