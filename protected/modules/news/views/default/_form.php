<?php
/**
 * @var $form TbActiveForm
 * @var $model News
 */
?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'news-form',
	'enableAjaxValidation' => false,
)); ?>

<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?= User::model()->getUserNameByPk($model->userID) ?>
	</div>

	<div class="row">
		<? if ($model->is_avaliable == 4) : ?>
			<?= News::$STATUSES[$model->is_avaliable] ?>
		<? else: ?>
			<?= CHtml::activeDropDownList($model, 'is_avaliable', $model->getAvailableStatuses($model->is_avaliable)) ?>
		<? endif ?>
	</div>
	<div class="row">
		<?= CHtml::image(createUrl(yii()->storage->getFileUrl($model->thumbnail))) ?>
	</div>
	<div class="row">

		<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5')); ?>
	</div>
	<div class="row">
		<?php echo $form->textFieldRow($model, 'content', array('class' => 'span5')); ?>
	</div>
	<div class="row">
		<?php echo $form->textFieldRow($model, 'timeCreate', array('class' => 'span5', 'disabled' => 'disabled', 'value' => yii()->dateFormatter->formatDateTime($model->timeCreate))); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>
<?php $this->endWidget(); ?>