<?/***
 * @var $dataProvider News
 */
?>
<?registerScript('viewAction', '
$("#news-grid table tbody tr td").bind("click", function () {
		if(!$(this).hasClass("checkBoxTD")){
			var id = $(this).parent().find("td:last").text();
			window.location.href = baseUrl + "/news/default/view/id/"+id;
		}
	});
')?>
<?registerScript('massAction', "
	function processSelected() {
		var arElemID = new Array();
		$('input[name^=news-grid_c0]:checked').each(function(i){
			var id = $(this).val();
			if(id != 1){
				arElemID[i] = $(this).val();
			}
		});
		return arElemID;
	}")?>
	<div class="form-action">
		<? /*=
		CHtml::ajaxLink(
			'На 1 уровень',
			createUrl("//news/newsStatus/index"),
			array(
				'type' => 'POST',
				'data' => array(
					'News' => array(
						'arID' => 'js:processSelected()',
						'action' => 'statusFirstLvl'
					),
				),
				'success' => 'function(data){
			$.fn.yiiGridView.update("news-grid", {
				data: $(this).serialize()
			});
		}'
			),
			array('class' => 'btn btn-info')) */
		?>
		<?=
		CHtml::ajaxLink(
			'Готов к оплате',
			createUrl('//news/newsStatus/index'),
			array(
				'type' => 'POST',
				'data' => array(
					'News' => array(
						'arID' => 'js:processSelected()',
						'action' => 'statusBeforePaid'
					),
				),
				'success' => 'function(data){
					$.fn.yiiGridView.update("news-grid", {
						data: $(this).serialize()
					});
					$("#SideBarMenu").html(data);
				}'
			),
			array('class' => 'btn btn-info'))?>
		<?=
		CHtml::ajaxLink(
			'Скачать',
			createUrl('//news/newsStatus/index'),
			array(
				'type' => 'POST',
				'dataType' => 'json',
				'data' => array(
					'News' => array(
						'arID' => 'js:processSelected()',
						'action' => 'download'
					),
				),
				'success' => 'function(data){
					window.location.href = baseUrl+"/"+data.url;
					setTimeout(function(){
						$.ajax({
						url:baseUrl+"/news/default/ArchiveDelete",
						type: "POST",
						data:{
							"archiveName":data.url
						},
						success:function(data){
							console.log(data);
						}
					});
					},3000)
				}'
			),
			array('class' => 'btn btn-info'))?>
		<?=
		CHtml::ajaxLink(
			'В Архив',
			createUrl("//news/newsStatus/index"),
			array(
				'type' => 'POST',
				'async' => 'false',
				'data' => array(
					'News' => array(
						'arID' => 'js:processSelected()',
						'action' => 'statusArchive'
					),
				),
				'success' => 'function(data){
					$.fn.yiiGridView.update("news-grid", {
						data: $(this).serialize()
					});
					$("#SideBarMenu").html(data);
				}'
			),
			array('class' => 'btn btn-danger')) ?>
	</div>
<? $this->renderPartial('grid', array('dataProvider' => $dataProvider)) ?>