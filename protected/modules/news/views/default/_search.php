<?php
/**
* @var $form TbActiveForm
* @var $model News
*/
?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'userID',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'managerID',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'is_avaliable',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'imageID',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'videoID',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'contentID',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'timeCreate',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
</div>

<?php $this->endWidget(); ?>
