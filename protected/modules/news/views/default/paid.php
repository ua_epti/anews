<?registerScript('viewAction', '
$("#news-grid table tbody tr td").live("click", function () {
		if(!$(this).hasClass("checkBoxTD")){
			var id = $(this).parent().find("td:last").text();
			window.location.href = baseUrl + "/news/default/view/id/"+id;
		}
	});
')?>
<?registerScript('massAction', "
	function processSelected() {
		var arElemID = new Array();
		$('input[name^=news-grid_c0]:checked').each(function(i){
			var id = $(this).val();
			if(id != 1){
				arElemID[i] = $(this).val();
			}
		});
		return arElemID;
	}")?>

<?=
CHtml::ajaxLink(
	'Скачать',
	createUrl('//news/newsStatus/index'),
	array(
		'type' => 'POST',
		'dataType' => 'json',
		'data' => array(
			'News' => array(
				'arID' => 'js:processSelected()',
				'action' => 'download'
			),
		),
		'success' => 'function(data){
					window.location.href = baseUrl+"/"+data.url;
					setTimeout(function(){
						$.ajax({
						url:baseUrl+"/news/default/ArchiveDelete",
						type: "POST",
						data:{
							"archiveName":data.url
						},
						success:function(data){
							console.log(data);
						}
					});
					},3000)
				}'
	),
	array('class' => 'btn btn-info'))?>
<? $this->renderPartial('grid', array('dataProvider' => $dataProvider, false, true)) ?>