<?/***
 * @var $dataProvider News
 */
?>

<?registerScript('viewAction', '
$("#news-grid table tbody tr td").live("click", function () {
		if(!$(this).hasClass("checkBoxTD")){
			var id = $(this).parent().find("td:last").text();
			window.location.href = baseUrl + "/news/default/view/id/"+id;
		}
	});
')?>
<?registerScript('massAction', "
	function processSelected() {
		var arElemID = new Array();
		$('input[name^=news-grid_c0]:checked').each(function(i){
			var id = $(this).val();
			if(id != 1){
				arElemID[i] = $(this).val();
			}
		});
		return arElemID;
	}")?>


	<div class="form-action">
		<?=
		CHtml::ajaxLink(
			'1 уровень',
			createUrl("//news/newsStatus/index"),
			array(
				'type' => 'POST',
				'data' => array(
					'News' => array(
						'arID' => 'js:processSelected()',
						'action' => 'statusFirstLvl'
					),
				),
				'success' => 'function(data){
					$.fn.yiiGridView.update("news-grid", {
						data: $(this).serialize()
					});
				}'
			),
			array('class' => 'btn btn-info')) ?>
		<?=
		CHtml::ajaxLink(
			'В Архив',
			createUrl('//news/newsStatus/index'),
			array(
				'type' => 'POST',
				'data' => array(
					'News' => array(
						'arID' => 'js:processSelected()',
						'action' => 'statusArchive'
					),
				),
				'success' => 'function(data){
					$.fn.yiiGridView.update("news-grid", {
						data: $(this).serialize()
					});
				}'
			),
			array('class' => 'btn btn-danger'))?>
	</div>
<? $this->renderPartial('grid', array('dataProvider' => $dataProvider)); ?>