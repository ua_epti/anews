<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userID')); ?>:</b>
	<?php echo CHtml::encode($data->userID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('managerID')); ?>:</b>
	<?php echo CHtml::encode($data->managerID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_avaliable')); ?>:</b>
	<?php echo CHtml::encode($data->is_avaliable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imageID')); ?>:</b>
	<?php echo CHtml::encode($data->imageID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('videoID')); ?>:</b>
	<?php echo CHtml::encode($data->videoID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contentID')); ?>:</b>
	<?php echo CHtml::encode($data->contentID); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('timeCreate')); ?>:</b>
	<?php echo CHtml::encode($data->timeCreate); ?>
	<br />

	*/ ?>

</div>