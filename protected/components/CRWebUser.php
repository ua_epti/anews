<?php

class CRWebUser extends RWebUser
{

	/**
	 * @var boolean whether to enable cookie-based login. Defaults to false.
	 */
	public $allowAutoLogin = true;

	/**
	 * @var string|array the URL for login. If using array, the first element should be
	 * the route to the login action, and the rest name-value pairs are GET parameters
	 * to construct the login URL (e.g. array('/site/login')). If this property is null,
	 * a 403 HTTP exception will be raised instead.
	 * @see CController::createUrl
	 */
	public $loginUrl = array('//user/login');
	public $logoutUrl = array('//user/logout');
	public $registrationUrl = array('//user/registration');
	public $recoveryUrl = '';
	public $managerReturnUrl;
	public $profileUrl;

	public function init()
	{
		$conf = Yii::app()->session->cookieParams;
		$this->identityCookie = array(
			'path' => $conf['path'],
			'domain' => $conf['domain'],
		);
		parent::init();
		$this->allowAutoLogin = Yii::app()->getModule('user')->autoLogin;
		$this->loginUrl = normalizeUrl(Yii::app()->getModule('user')->loginUrl);
		$this->logoutUrl = normalizeUrl(Yii::app()->getModule('user')->logoutUrl);
		$this->registrationUrl = normalizeUrl(Yii::app()->getModule('user')->registrationUrl);
		$this->recoveryUrl = normalizeUrl(Yii::app()->getModule('user')->recoveryUrl);
		$this->loginRequiredAjaxResponse = normalizeUrl(Yii::app()->getModule('user')->loginRequiredAjaxResponse);
		$this->managerReturnUrl = normalizeUrl(Yii::app()->getModule('user')->managerReturnUrl);
		$this->profileUrl = normalizeUrl(Yii::app()->getModule('user')->profileUrl);
	}

	public function getRole()
	{
		return $this->getState('__role');
	}

	public function getUserRoles($userId = 0)
	{
		if ($userId == 0 || $userId == user()->id) {
			return $this->getState('roles');
		} else {
			return yii()->getModule('admin')->getModule('rights')->authorizer->getUserRoles($userId);
		}
	}

	/**
	 *
	 * @param int $userId
	 * @return bool
	 */
	public function isManager($userId = null)
	{
		return count(array_intersect(array('manager', 'junior_manager', 'middle_manager'), $this->getUserRoles($userId)));
	}

	public function isJuniorManager($userId = null)
	{
		return count(array_intersect(array('junior_manager'), $this->getUserRoles($userId)));
	}

	public function isMiddleManager($userId = null)
	{
		return count(array_intersect(array('middle_manager'), $this->getUserRoles($userId)));
	}

	public function isHighManager($userId = null)
	{
		return count(array_intersect(array('manager'), $this->getUserRoles($userId)));
	}

	public function getId()
	{
		return $this->getState('__id') ? $this->getState('__id') : 0;
	}

	public function afterLogin($fromCookie)
	{
		parent::afterLogin($fromCookie);
		$this->updateSession();
	}

	public function updateSession()
	{
		if ($this->id) {
			$user = Yii::app()->getModule('user')->user($this->id);
			$this->name = $user->username;
			$userAttributes = CMap::mergeArray(
				array(
					'email' => $user->email,
					'username' => $user->username,
					'create_at' => $user->create_at,
					'lastvisit_at' => $user->lastvisit_at,
					'roles' => yii()->getModule('admin')->getModule('rights')->authorizer->getUserRoles(),
				),
				$user->profile->getAttributes()
			);
			foreach ($userAttributes as $attrName => $attrValue) {
				$this->setState($attrName, $attrValue);
			}
		}
	}

	public function model($id = 0)
	{
		return Yii::app()->getModule('user')->user($id);
	}

	public function user($id = 0)
	{
		return $this->model($id);
	}

	public function getUserByName($username)
	{
		return Yii::app()->getModule('user')->getUserByName($username);
	}

	public function getAdmins()
	{
		return Yii::app()->getModule('user')->getAdmins();
	}

	public function isAdmin()
	{
		return Yii::app()->getModule('user')->isAdmin();
	}

}