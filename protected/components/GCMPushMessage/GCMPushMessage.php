<?php

/**
 * Yii component to send push notifications using Google Cloud Messaging for Android
 *
 * Config
 * -----------------------
 * 'components' => array(
 *            ...
 *             'gcm' => array(
 *                 'class' => 'common.extensions.gcm.GCMPushMessage',
 *                 'serverApiKey' => 'passw',
 *             ),
 *          ...
 * -----------------------
 *
 * Example usage
 * -----------------------
 * Yii::app()->gcm->setDevices($google_id)->send("done");
 * -----------------------
 *
 * $apiKey Your GCM api key
 * $devices An array or string of registered device tokens
 * $message The mesasge you want to push out
 *
 **/
class GCMPushMessage extends CComponent
{

	public $url = 'https://android.googleapis.com/gcm/send';
	public $serverApiKey = "";
	public $devices = array();

	public function init()
	{
	}

	/**
	 * Add device list to send
	 * @param array|string $deviceIds
	 * @return GCMPushMessage
	 **/
	public function setDevices($deviceIds)
	{
		if (is_array($deviceIds)) {
			$this->devices = $deviceIds;
		} else {
			$this->devices = array($deviceIds);
		}
		return $this;
	}

	/**
	 * Send message to device or devices
	 * @param string $message
	 * @return string
	 **/
	public function send($message)
	{

		if (!is_array($this->devices) || count($this->devices) == 0) {
			$this->error("No devices set");
		}

		if (strlen($this->serverApiKey) < 8) {
			$this->error("Server API Key not set");
		}

		$fields = array(
			'registration_ids' => $this->devices,
			'data' => array("message" => $message),
		);

		$headers = array(
			'Authorization: key=' . $this->serverApiKey,
			'Content-Type: application/json'
		);

		// Open connection
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, CJSON::encode($fields));

		// Execute post
		$result = curl_exec($ch);

		// Close connection
		curl_close($ch);
		dump($result);die;

		return $result;
	}

	public function error($msg)
	{
		throw new CException(Yii::t('ext.gcm.GCMPushMessage', 'Android send notification failed with error: {msg}', array('{msg}' => $msg)));
	}

}
