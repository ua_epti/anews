<?php
class CRFileHelper extends CFileHelper
{

	/**
	 * Removes directory and any files found into it
	 * @param string $dir Directory to delete
	 */
	public static function deleteDirectory($dir)
	{
		if (file_exists($dir) && is_dir($dir)) {
			self::deleteDirectoryRecursive($dir);
		}
	}

	/**
	 * Recursive function for directory removing
	 * @param type $dir
	 */
	protected static function deleteDirectoryRecursive($dir)
	{
		$dir = rtrim($dir,' /\\');
		$contents = glob($dir . DIRECTORY_SEPARATOR . '*');
		foreach ($contents as $object) {
			if (is_dir($object)) {
				self::deleteDirectoryRecursive($object);
			}
			else {
				@unlink($object);
			}
		}
		@rmdir($dir);
	}

	/**
	 * Checks if directory is empty
	 * @param string $dir Directory path
	 * @return null|boolean True if directory exists, readable and empty, false if directory is not empty, null otherwise
	 */
	public static function isDirEmpty($dir)
	{
		if (!file_exists($dir) || !is_dir($dir) || !is_readable($dir)) {
			return null;
		}

		$content = glob(rtrim($dir,' /\\') . DIRECTORY_SEPARATOR . '*');

		if ($content && is_array($content) && count($content)) {
			return false;
		}

		return true;
	}

	/**
	 * Download file helper
	 * @param mixed $object One of CRContentFile, CRContentMedia or CRContentImage classes object
	 */
	public static function download($object)
	{
		if (!($object instanceof CRContentFile) && !($object instanceof CRContentMedia) && !($object instanceof CRContentImage)) {
			return;
		}

		$fd = @fopen(CR::app()->storage->getFilePath($object->path), 'rb');
		if (isset($_SERVER['HTTP_RANGE'])) {
			$range = $_SERVER['HTTP_RANGE'];
			$range = str_replace('bytes=', '', $range);
			list($range, $end) = explode('-', $range);

			if (!empty($range)) {
				fseek($fd, $range);
			}
		}
		else {
			$range = 0;
		}

		if ($range) {
			header($_SERVER['SERVER_PROTOCOL'].' 206 Partial Content');
		}

		else {
			header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
		}

		header('Content-Disposition: attachment; filename='.$object->filename);
		header('Last-Modified: '.date('D, d M Y H:i:s T', $object->timeUpdate));
		header('Accept-Ranges: bytes');
		header('Content-Length: '.($object->size - $range));
		if ($range) {
			header("Content-Range: bytes $range-".($object->size - 1).'/'.$object->size);
		}
		header('Content-Type: '.$object->mime);

		fpassthru($fd);
		fclose($fd);
	}	
}
?>