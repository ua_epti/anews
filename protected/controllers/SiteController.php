<?

class SiteController extends Controller
{
	public $layout = '//layouts/main';

	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xe1e1e1,
			),
		);
	}

	public function actionIndex()
	{
		if (user()->isGuest) {
			$this->forward('//user/login/');
		} elseif (user()->isAdmin()) {
			$this->redirect('../admin');
		} elseif (user()->isManager()) {
			$this->redirect('../news');
		}
	}

	public function actionError()
	{
		if ($error = yii()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}

	public function actionTest()
	{
		$query['data'] = array(
			'userEmail' => 'asd@sd.ru',
			'amount' => 1,
			'newsID' => '1',
		);
		$resArray = yii()->PayPal->Pay($query);
		dump($resArray);die;
	}

	public function actionPush()
	{
		date_default_timezone_set('Europe/Kiev');
//		error_reporting(-1);
		yii::import('application.components.apnsp.ApnsPHP.ApnsPHP_Autoload');
		yii::import('application.components.apnsp.ApnsPHP.ApnsPHP_Push');
		yii::import('application.components.apnsp.ApnsPHP.ApnsPHP_Abstract');
		yii::import('application.components.apnsp.ApnsPHP.ApnsPHP_Exception');
		yii::import('application.components.apnsp.ApnsPHP.ApnsPHP_Message');

// Instantiate a new ApnsPHP_Push object
		$push = new ApnsPHP_Push(
			ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
			__DIR__ . '/../components/apnsp/news_dev.pem'
		);

// Set the Provider Certificate passphrase
		$push->setProviderCertificatePassphrase('123');

// Set the Root Certificate Autority to verify the Apple remote peer
		$push->setRootCertificationAuthority(__DIR__ . '/../components/apnsp/root.pem');

// Connect to the Apple Push Notification Service
		$push->connect();

// Instantiate a new Message with a single recipient
		$message = new ApnsPHP_Message('957ee00f 3502a438 1682cdfb 26efe9d5 eaa2bcae c2e51c80 f3edfed0 a24dbcee');

// Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
// over a ApnsPHP_Message object retrieved with the getErrors() message.
		$message->setCustomIdentifier("Message-Badge-3");

// Set badge icon to "3"
		$message->setBadge(3);

// Set a simple welcome text
		$message->setText('Hello APNs-enabled device!');

// Play the default sound
		$message->setSound();

// Set a custom property
		$message->setCustomProperty('acme2', array('bang', 'whiz'));

// Set another custom property
		$message->setCustomProperty('acme3', array('bing', 'bong'));

// Set the expiry value to 30 seconds
		$message->setExpiry(30);

// Add the message to the message queue
		$push->add($message);

// Send all messages in the message queue
		$push->send();

// Disconnect from the Apple Push Notification Service
		$push->disconnect();

// Examine the error message container
		$aErrorQueue = $push->getErrors();
		if (!empty($aErrorQueue)) {
			var_dump($aErrorQueue);
		}
	}

	public function actionAptest()
	{
		// Adjust to your timezone
		date_default_timezone_set('Europe/Rome');

// Report all PHP errors
		error_reporting(-1);

// Using Autoload all classes are loaded on-demand
		require_once 'ApnsPHP/Autoload.php';

// Instanciate a new ApnsPHP_Push object
		$server = new ApnsPHP_Push_Server(
			ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
			'server_certificates_bundle_sandbox.pem'
		);

// Set the Root Certificate Autority to verify the Apple remote peer
		$server->setRootCertificationAuthority('entrust_root_certification_authority.pem');

// Set the number of concurrent processes
		$server->setProcesses(2);

// Starts the server forking the new processes
		$server->start();

// Main loop...
		$i = 1;
		while ($server->run()) {

			// Check the error queue
			$aErrorQueue = $server->getErrors();
			if (!empty($aErrorQueue)) {
				// Do somethings with this error messages...
				var_dump($aErrorQueue);
			}

			// Send 10 messages
			if ($i <= 10) {
				// Instantiate a new Message with a single recipient
				$message = new ApnsPHP_Message('1e82db91c7ceddd72bf33d74ae052ac9c84a065b35148ac401388843106a7485');

				// Set badge icon to "i"
				$message->setBadge($i);

				// Add the message to the message queue
				$server->add($message);

				$i++;
			}

			// Sleep a little...
			usleep(200000);
		}
	}

	public function actionANP()
	{

		/*	yii::import('application.components.anp.Pusher');
			$apiKey = "AIzaSyAglmF_UGVFiIhEESlBXXZ1yV5I-5slrnI";
			$regId = "BGSV-TEPV-AQLD-D";

			$pusher = new Pusher($apiKey);
			$pusher->notify($regId, "Hola");

			print_r($pusher->getOutputAsArray());*/
		/*$api_key = "AIzaSyAglmF_UGVFiIhEESlBXXZ1yV5I-5slrnI";
		$registrationIDs = array("BGSV-TEPV-AQLD-D");
		$message = "congratulations";
		$url = 'https://android.googleapis.com/gcm/send';
		$fields = array(
			'registration_ids' => $registrationIDs,
			'data' => array("message" => $message),
		);

		$headers = array(
			'Authorization: key=' . $api_key,
			'Content-Type: application/json');

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		$result = curl_exec($ch);
		curl_close($ch);
		dump($result);die;

		echo $result;*/
		Yii::app()->gcm->setDevices('950600862534')->send("done");

	}
}

