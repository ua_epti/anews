<?

yii::import('application.modules.news.models.*');
yii::import('application.modules.tasks.models.*');

yii::import('application.components.paginator.Pagination_Helper');
yii::import('application.components.paginator.Pagination_Manager');

class ApiController extends Controller
{
	const APPLICATION_ID = 'ASCCPE';

	const API_CONFIRM_BEFORE = 0;
	const API_CONFIRM_SUCCESS = 1;
	const API_CONFIRM_ERROR = -1;

	private $format = 'json';

	public function filters()
	{
		return array('rights');
	}

	public $locale;

	protected function beforeAction($action)
	{
		$this->locale = yii()->request->getParam('locale');
		return true;
	}

	public function actionIndex()
	{
		if (!isset ($_GET['c'])) {
			$response['responce'] = array('Result' => "error", 'Message' => yii::t('api', 'You did not indicate a problem  c='), null, null, $this->locale);
			$this->_sendResponse(501, CJSON::encode($response));
			yii()->end();
		} else {
			if (!isset($_GET['task'])) {

				$getFunction = $_GET['c'] . "Default";
			} else {
				$getFunction = $_GET['c'] . strtoupper($_GET['task']);
			}
			$response = $this->$getFunction($_POST);
		}
		$this->_sendResponse(200, CJSON::encode($response));
	}

	public function dump_result($models, $params = null)
	{
		if (!isset($models)) {
			$responce['responce'] = array('Result' => "error", 'Message' => yii::t('api', 'Model is empty', null, null, $this->locale));
			$this->_sendResponse(500, CJSON::encode($responce));
		} else {
			if (empty($models)) {
				$responce['responce'] = array('Result' => "success", 'Data' => array($params => $models));
				$this->_sendResponse(200, CJSON::encode($responce));
			} else {
				if ($params != '') {
					$responce['responce'] = array('Result' => "success", 'Data' => array($params => $models));
				} else {
					$responce['responce'] = array('Result' => "success", 'Data' => $models);
				}

			}
		}
		return $responce;
	}

	public function validate($values, $option)
	{
		foreach ($values as $key => $value) {
			switch ($option) {
				case 'empty':
					if ($value == "") {
						$this->_sendResponse(501, CJSON::encode(array('Result' => "error", 'Message' => yii::t('api', '{value} Must be not empty!', array('{value}' => $key), null, $this->locale))));
					}
					break;
				/*case 'positive_number':
					if (!(is_numeric($value) and $value > 0)) {
						$this->_sendResponse(501, CJSON::encode(array('Result' => "error", 'Message' => yii::t('api', '{value} Must be positive!', array('{value}' => $key), null, $this->locale))));
					}
					break;*/
				/*case 'float':
					if (!preg_match('/^[0-9-]*[\.]{1}[0-9-]+$/', $value)) {
						$this->_sendResponse(501, CJSON::encode(array('Result' => "error", 'Message' => yii::t('api', '{value} Must be float!!', array('{value}' => $key), null, $this->locale))));
					}
					break;*/
				case 'numeric':
					if (!(is_numeric($value))) {
						$this->_sendResponse(501, CJSON::encode(array('Result' => "error", 'Message' => yii::t('api', '{value} Must be integer!', array('{value}' => $key), null, $this->locale))));
					}
					break;
					/*	case 'timestamp':
							if (!preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $value)) {
								$this->_sendResponse(501, CJSON::encode(array('Result' => "error", 'Message' => yii::t($key . ' Must be in format timestamp! ----' . $value . '----')));
							}
							break;*/
					/*case 'unix':
						if (is_numeric($value) && 0 < $value && $value < strtotime("some date in the future that you don't expect the value to exceed, but before year 2038")) {
							$this->_sendResponse(501, CJSON::encode(array('Result' => "error", 'Message' =>
								$key . ' Must be in format unixTime! ----' . $value . '----')));
						}*/

					break;
				case 'validate':
					break;
			}
		}
	}

	private function _sendResponse($status = 200, $body = '', $content_type = 'text/json')
	{
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		header($status_header);
		header('Content-type: ' . $content_type);
		if ($body != '') {
			echo $body;
		} else {
			echo 'error';
		}
		yii()->end();
	}

	private function _getStatusCodeMessage($status)
	{
		$codes = array(
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			999 => 'Not Authorized');
		return (isset($codes[$status])) ? $codes[$status] : '';
	}

	public function userRegistration($attr)
	{
		$this->validate(array('token' => $attr['token']), 'empty');

		if (md5($attr['username']) == $attr['password']) {
			$responce['responce'] = array('Result' => "error", 'Message' => yii::t('api', 'Password can not be the same as your username!', null, null, $this->locale));
			$this->_sendResponse(501, CJSON::encode($responce));
			yii()->end();
		}
		$model = yii()->db->createCommand('SELECT Count(*) FROM yc_users WHERE email="' . $attr['email'] . '"')->queryScalar();
		if ($model != 0) {
			$responce['responce'] = array('Result' => "error", 'Message' => yii::t('api', 'User with this email already exists!', null, null, $this->locale));
			$this->_sendResponse(501, CJSON::encode($responce));
			yii()->end();
		} else {
			$arResp = array();
			$models = new User;
			$profile = new Profile();
			$models->attributes = $attr;
			$models->password = $attr['password'];
			$models->status = User::STATUS_ACTIVE;

			$token = ApiKeys::model()->findByAttributes(array('code' => $attr['token']));
			if ($token) {
				$token->status_confirm = self::API_CONFIRM_SUCCESS;
				$token->save(false);
			}
			if ($models->validate() && $profile->validate()) {
				if ($models->save()) {
					$profile->user_id = $models->id;
					$arResp['User'] = $models->attributes;
					if ($profile->save()) {
						if (yii()->getModule('admin')->hasModule('rights')) {
							$authenticatedName = yii()->getModule('admin')->getModule('rights')->authenticatedName;
							yii()->authManager->assign($authenticatedName, $models->id);
						}
						$arResp['Profile'] = $profile->attributes;
						/*$body = yii::t('api', "
							Respected, {username},\n
							Password:!\n
                            Password: {password}", array('{username}' => $models->username, '{password}' => $attr['password']), null, $this->locale);
						$headers = "MIME-Version: 1.0\r\n";
						$headers .= "Content-type: text/plain; charset=utf-8\r\n";
						$headers .= "From:" . yii()->params['adminEmail'] . "\r\nReply-To:" . yii()->params['adminEmail'];
						mail($models->email, yii::t('api', 'New password'), $body, $headers);*/
					} elseif (!YII_DEBUG) {
						$this->_sendResponse(501, CJSON::encode($profile->getErrors()));
						yii()->end();
					}
				} elseif (!YII_DEBUG) {
					$this->_sendResponse(501, CJSON::encode($models->getErrors()));
					yii()->end();
				}
			} elseif (!YII_DEBUG) {
				$arErrors = array();
				if ($profile->hasErrors()) {
					$arErrors[] = $profile->getErrors();
				}
				if ($models->hasErrors()) {
					$arErrors[] = $models->getErrors();
				}
				$this->_sendResponse(501, CJSON::encode($arErrors));
				yii()->end();
			} else {
				$arErrors = array('Result' => "error", 'error' => 'Error data');
				$this->_sendResponse(501, CJSON::encode($arErrors));
				yii()->end();
			}

		}
		return $this->dump_result($arResp);
	}

	public function userCkeckLogin($attr)
	{
		$this->validate($attr, 'empty');
		$models = Yii::app()->db->createCommand('
			SELECT u.*,ak.id,ak.code as apiKey
			FROM yc_users as u
			LEFT JOIN yc_api_keys as ak ON u.id = ak.id_user
			WHERE u.email="' . $attr['email'] . '" AND u.password="' . md5($attr['password']) . '"')->queryRow();
		if (empty($models)) {
			$responce['responce'] = array('Result' => "error", 'Message' => yii::t('api', 'You misspelled username and password', null, null, $this->locale));
			$this->_sendResponse(501, CJSON::encode($responce));
			yii()->end();
			return $this->dump_result($models);
		} else {
			return $this->dump_result($models);
		}
	}

	public function userLogin($attr)
	{
		$this->validate($attr, 'empty');
		$arResp = array();
		if (!is_md5($attr['password'])) {
			$attr['password'] = md5($attr['password']);
		}

		$is_login = ApiKeys::model()->findByAttributes(array('code' => $attr['token'], 'status_confirm' => self::API_CONFIRM_BEFORE));

		if ($is_login) {
			$models = yii()->db->createCommand('
			SELECT *
			FROM yc_users
			WHERE email="' . $attr['email'] . '" and password="' . $attr['password'] . '"')->queryRow();

			$arResp['User'] = $models;
			if (!empty($models)) {
				$profile = Yii::app()->db->createCommand('
				SELECT *
				FROM yc_profiles
				WHERE user_id =' . $models['id'])
					->queryRow();
				$arResp['Profile'] = $profile;
				$this->setTokenByUserID($models['id'], $attr['token']);
				$this->setUserIDInNews($models['id'], $attr['token']);
			}
			if (empty($models)) {
				$responce['responce'] = array('Result' => "error", 'Message' => yii::t('api', 'You misspelled username and password', null, null, $this->locale));
				$this->_sendResponse(501, CJSON::encode($responce));
				Yii::app()->end();
			} else {

			}
		}
		return $this->dump_result($arResp);
	}

	public function userLogout($attr)
	{
		$this->validate(array('token' => $attr['token']), 'empty');
		$status_confirm = ApiKeys::model()->findByAttributes(array('code' => $attr['token']));
		$models = null;
		if ($status_confirm->status_confirm == 1) {
			$models = ApiKeys::model()->findByAttributes(array('code' => $attr['token']));
			$models->status_confirm = self::API_CONFIRM_BEFORE;
			$models->save(false);
			if ($models->id_user) {
				$this->setUserIDInNews($models['id'], $attr['token']);
			}
		} else {
			$responce['responce'] = array('Result' => "error", 'Message' => 'Данный пользователь не был авторизирован');
			$this->_sendResponse(501, CJSON::encode($responce));
			Yii::app()->end();
		}

		return $this->dump_result($models);
	}

	public function userRemember($attr)
	{
		$this->validate(array('email' => $attr['email']), 'empty');
		$user = User::model()->findByAttributes(array('email' => $attr['email']));
		if (count($user) != 0) {
			$newPasswd = User::model()->generatePasswd(10);
			$body = yii::t('api', "
					Respected, {username},\n
					You will generate a new password:!\n
                    Password: {password}", array('{username}' => $user->username, '{password}' => $newPasswd), null, $this->locale);
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/plain; charset=utf-8\r\n";
			$headers .= "From:" . yii()->params['adminEmail'] . "\r\nReply-To:" . yii()->params['adminEmail'];
			mail($user->email, yii::t('api', 'New password'), $body, $headers);
			yii()->db->createCommand()->update('yc_users', array('password' => md5($newPasswd),), 'id=:id', array(':id' => $user->id));
			$models = 'ok';
		} else {
			$models = '';
		}
		return $this->dump_result($models);
	}

	public function userUserinfo($attr)
	{
		$models = null;
		if ($attr['user_id'] != '') {
			$searchterm = explode(',', $attr['user_id']);
			$searchCondition = "id='" . implode("' OR id ='", $searchterm) . "'";
			$sql = "SELECT u.*,us.*
					FROM yc_users as u
					LEFT JOIN yc_user_state as us ON u.id = us.userID
					WHERE u. " . $searchCondition;
			$models['User'] = yii()->db->createCommand($sql)->queryAll();
		} elseif (!empty($attr['token'])) {
			$apiModel = ApiKeys::model()->findByAttributes(array('code' => $attr['token']));
			if ($apiModel != null) {
				$userID = $apiModel->id_user;
				if (!empty($userID)) {
					$models = User::model()->findByPk($userID);
				} else {
					$response['responce'] = array('Result' => "error", 'Message' => 'Bad request');
					$this->_sendResponse(500, CJSON::encode($response));
				}
			}
		} else {
			$response['responce'] = array('Result' => "error", 'Message' => 'enter or Token or userID');
			$this->_sendResponse(500, CJSON::encode($response));
		}
		return $this->dump_result($models);
	}

	public function userUpdate($attr)
	{
//		$this->validate(array('userID' => $attr['userID']), 'empty');
		$this->validate(array('paypalemail' => $attr['paypalemail']), 'empty');
		$this->validate(array('username' => $attr['username']), 'empty');
		$this->validate(array('password' => $attr['password']), 'empty');

		if ($attr['userID'] != '' || $attr['token'] != '') {
			$new_password = User::model()->notsafe()->findbyPk($attr['userID']);
			$new_password->password = $attr['password'];
			$new_password->username = $attr['username'];
			$new_password->paypalemail = $attr['paypalemail'];
			if ($new_password->save(false)) {

			} elseif (YII_DEBUG) {
				dump($new_password->getErros());
				die;
			}
		}

		return $this->dump_result($new_password);
	}

	public function tasksAll($attr)
	{
		if (empty($attr['locale'])) {
			$locale = 'en';
		}
		$locale = $attr['locale'];
		$response = array();
		$models = Tasks::model()->findAllByAttributes(array('is_publish' => 1, 'userID' => NULL));
		foreach ($models as $key => $task) {
			$response[$key]['id'] = $task->id;
			$content = $task->getContentByLocale($locale);
			$title = $task->getTitleByLocale($locale);
			$response[$key]['title'] = $title;
			$response[$key]['content'] = $content;
			$response[$key]['timeVideo'] = $task->timeVideo;
			$map_image = '';
			if ($task->map_image != '') {
				$map_image = createUrl(yii()->storage->getFileUrl($task->map_image));
			}
			$response[$key]['map_image'] = $map_image;
			$response[$key]['map_address'] = $task->map_address;
			$response[$key]['price'] = $task->price;
			$response[$key]['locale'] = $locale;
			$response[$key]['timeCreate'] = yii()->dateFormatter->format('dd.MM.yyyy HH:mm', $task->timeCreate);
		}
		return $this->dump_result($response);
	}

	public function newsUpdate($attr)
	{
		$this->validate(array('newsID' => $attr['newsID']), 'empty');
		$this->validate(array('content' => $attr['content']), 'empty');
		$this->validate(array('locale' => $attr['locale']), 'empty');
		News::model()->updateByPk(
			$attr['newsID'],
			array(
				'content' => $attr['content'],
				'latitude' => $attr['latitude'],
				'longitude' => $attr['longitude'],
				'locale' => $attr['locale'],
				'taskID' => $attr['taskID'],
				'is_anonym' => $attr['is_anonym'],
				'address' => $attr['address'],
			)
		);
		$news = News::model()->findByPk($attr['newsID']);
		return $this->dump_result($news, 'News');
	}

	public function taskByuser($attr)
	{
		$this->validate(array('userID' => $attr['userID']), 'empty');
		if (empty($attr['locale'])) {
			$locale = 'en';
		} else {
			$locale = $attr['locale'];
		}
		$response = array();
		$models = Tasks::model()->findAllByAttributes(array('is_publish' => Tasks::PUBLISH_TRUE, 'userID' => $attr['userID']));
		foreach ($models as $key => $task) {
			$response[$key]['id'] = $task->id;
			$content = $task->getContent($locale);
			$response[$key]['title'] = $content->title;
			$response[$key]['content'] = $content->content;
			$response[$key]['locale'] = $locale;
			$response[$key]['timeVideo'] = $task->timeVideo;
			$response[$key]['map_address'] = $task->map_image;
			$response[$key]['price'] = $task->price;
		}

		return $this->dump_result($response, 'Tasks');
	}

	public function newsAll($attr)
	{
		$this->validate(array('page' => $attr['page']), 'empty');
		if ($attr['page'] != 0) {


			$settings = NewsSettings::model()->findByPk(NewsSettings::PAGE_COUNT)->value;

			$paginationManager = new Pagination_Manager($settings, 100, $_REQUEST);

			$sql = "SELECT
           SQL_CALC_FOUND_ROWS
           `id`,`title`,`content`,`video`,`image`,`thumbnail`,`viewers`,`userID`,`token`,`is_avaliable`,
           `locale`,`timeCreate`,`taskID`
        FROM
           `yc_news`
        WHERE
            `is_avaliable`=" . News::STRIP . "
        ORDER BY
           `timeCreate` DESC
        LIMIT " .
				$paginationManager->getStartLimit() . "," .
				$paginationManager->getStopLimit();
			$response = array();
			$result_list = yii()->db->createCommand($sql)->queryAll();
			if (!empty($result_list)) {
				foreach ($result_list as $key => $model) {
					$userName = '';
					if ($model['userID'] != null) {
						$userName = UserModule::user($model['userID'])->username;
					}
					if (!empty($model['taskID'])) {
						$taskPrice = Tasks::model()->findByPk($model['taskID']);
						$price = $taskPrice->price;
					} else {
						$price = $model['dop_price'];
					}
					$response[$key]['id'] = $model['id'];
					$response[$key]['taskID'] = $model['taskID'];
					$response[$key]['userID'] = $model['userID'];
					$response[$key]['userName'] = $userName;
					$response[$key]['token'] = $model['token'];
					$response[$key]['is_avaliable'] = $model['is_avaliable'];
					$response[$key]['title'] = $model['title'];
					$response[$key]['content'] = $model['content'];
					$response[$key]['price'] = $price;
					$response[$key]['locale'] = $model['locale'];
					$response[$key]['timeCreate'] = yii()->dateFormatter->format('dd.MM.yyyy HH:mm', $model['timeCreate']);
					$response[$key]['viewers'] = $model['viewers'];

					$thumbnail = '';

					if ($model['thumbnail'] != '') {
						$thumbnail = createUrl(yii()->storage->getFileUrl($model['thumbnail']));
					}
					$response[$key]['thumbnail'] = $thumbnail;

					$image = '';
					if ($model['image'] != '') {
						$image = createUrl(yii()->storage->getFileUrl($model['image']));
					}
					$response[$key]['image'] = $image;

					$video = '';
					if ($model['video'] != '') {
						$video = createUrl(yii()->storage->getFileUrl($model['video']));
					}
					$response[$key]['video'] = $video;

					$like = $this->checkLikeNewsByUser($model['id'], $attr['userID'], $attr['token']);
					if ($like->like == 1) {
						$userLike = 1;
					} else {
						$userLike = 0;
					}
					$response[$key]['LikeByUser'] = $userLike;
				}
			}
		}
		return $this->dump_result($response, 'News');
	}

	public function newsPopular($attr)
	{
		$settings = NewsSettings::model()->findByPk(NewsSettings::PAGE_COUNT)->value;
		$paginationManager = new Pagination_Manager($settings, 100, $_REQUEST);

		$sql = "SELECT
           SQL_CALC_FOUND_ROWS
           `id`,`title`,`content`,`video`,`image`,`thumbnail`,`viewers`,`userID`,`token`,`is_avaliable`,
           `locale`,`timeCreate`,`taskID`
        FROM
           `yc_news`
        WHERE
            `is_avaliable`=" . News::STRIP . "
        ORDER BY
           `viewers` DESC
        LIMIT " .
			$paginationManager->getStartLimit() . "," .
			$paginationManager->getStopLimit();
		$result_list = yii()->db->createCommand($sql)->queryAll();
		$response = array();
		if (!empty($result_list)) {
			foreach ($result_list as $key => $model) {
				$userName = '';
				if ($model['userID'] != null) {
					$userName = UserModule::user($model['userID'])->username;
				}
				$response[$key]['id'] = $model['id'];
				$response[$key]['taskID'] = $model['taskID'];
				if (!empty($model['taskID'])) {
					$taskPrice = Tasks::model()->findByPk($model['taskID']);
					$price = $taskPrice->price;
				} else {
					$price = $model['dop_price'];
				}
				$response[$key]['userID'] = $model['userID'];
				$response[$key]['userName'] = $userName;
				$response[$key]['token'] = $model['token'];
				$response[$key]['is_avaliable'] = $model['is_avaliable'];
				$response[$key]['price'] = $price;
				$response[$key]['title'] = $model['title'];
				$response[$key]['content'] = $model['content'];
				$response[$key]['locale'] = $model['locale'];
				$response[$key]['timeCreate'] = yii()->dateFormatter->format('dd.MM.yyyy HH:mm', $model['timeCreate']);
				$response[$key]['viewers'] = $model['viewers'];

				$thumbnail = '';
				if ($model['thumbnail'] != '') {
					$thumbnail = createUrl(yii()->storage->getFileUrl($model['thumbnail']));
				}
				$response[$key]['thumbnail'] = $thumbnail;

				$image = '';
				if ($model['image'] != '') {
					$image = createUrl(yii()->storage->getFileUrl($model['image']));
				}
				$response[$key]['image'] = $image;

				$video = '';
				if ($model['video'] != '') {
					$video = createUrl(yii()->storage->getFileUrl($model['video']));
				}
				$response[$key]['video'] = $video;


				$like = $this->checkLikeNewsByUser($model['id'], $attr['userID'], $attr['token']);
				if ($like->like == 1) {
					$userLike = 1;
				} else {
					$userLike = 0;
				}
				$response[$key]['LikeByUser'] = $userLike;
			}
		}
		return $this->dump_result($response, 'News');
	}

	public function newsUpImage($attr)
	{

		if (isset($attr['News'])) {
			$attr['userID'] = $attr['News']['userID'];
			$attr['token'] = $attr['News']['token'];
			unset($attr['News']);
		}

		$model = new News();
		$response = array();
		if (isset($_FILES)) {
			$resModel = $this->processFile($model, $attr);
		}

		if ($attr['userID'] != '') {
			$userID = $attr['userID'];
		} else {
			$userID = NULL;
		}

		if ($attr['token'] != '') {
			$token = $attr['token'];
		} else {
			$token = '';
		}

		$time = time();
		yii()->db->createCommand()->insert(
			'yc_news',
			array(
				'userID' => $userID,
				'token' => $token,
				'image' => $resModel->image,
				'timeCreate' => $time,
				'thumbnail' => $resModel->thumbnail,
			));
		if ($userID == "") {
			$userID = $this->getUserIdByToken($token);
			$this->setUserIDInNews($userID, $token);
		}
		$model = News::model()->findByAttributes(array('timeCreate' => $time));
		$response['id'] = $model->id;
		return $this->dump_result($response);
	}

	public function newsUpVideo($attr)
	{
		if (isset($attr['News'])) {
			$attr['userID'] = $attr['News']['userID'];
			$attr['token'] = $attr['News']['token'];
			unset($attr['News']);
		}
		$model = new News();
		if (isset($_FILES)) {
			$resModel = $this->processFileUpload($model, $attr);
		}

		if ($attr['userID'] != '') {
			$userID = $attr['userID'];
		} else {
			$userID = NULL;
		}

		if ($attr['token'] != '') {
			$token = $attr['token'];
		} else {
			$token = '';
		}

		$time = time();
		yii()->db->createCommand()->insert(
			'yc_news',
			array(
				'userID' => $userID,
				'token' => $token,
				'video' => $resModel->video,
				'timeCreate' => $time,
			));

		$model = News::model()->findByAttributes(array('timeCreate' => $time));
		$response['id'] = $model->id;
		return $this->dump_result($response);
	}

	/*Like UnLike News*/
	public function newsLike($attr)
	{
		$this->validate(array('newsID' => $attr['newsID']), 'empty');
		$checkActionLike = $this->checkLikeNewsByUser($attr['newsID'], $attr['userID'], $attr['token']);
		if ($checkActionLike == null) {
			$newsLike = new NewsLikes();
			if ($attr['userID'] != '') {
				$newsLike->userID = $attr['userID'];
			} elseif ($attr['token'] != '') {
				$newsLike->token = $attr['token'];
				$userID = $this->getUserIdByToken($attr['token']);
				if ($userID != null) {
					$newsLike->userID = $userID;
				}
			}
			$newsLike->newsID = $attr['newsID'];
			$newsLike->like = NewsLikes::NEWS_LIKE;
			$newsLike->save(false);
			$news = News::model()->findByPk($attr['newsID']);
			$news->saveCounters(array('viewers' => 1));
		}
		if ($checkActionLike->like == 1) {
			$newsLike = $this->checkLikeNewsByUser($attr['newsID'], $attr['userID'], $attr['token']);
			$newsLike->like = NewsLikes::NEWS_UNLIKE;
			$newsLike->save(false);
			$news = News::model()->findByPk($attr['newsID']);
			$news->saveCounters(array('viewers' => -1));
		}
		if ($checkActionLike->like == 0 && $checkActionLike->like != null) {
			$newsLike = $this->checkLikeNewsByUser($attr['newsID'], $attr['userID'], $attr['token']);
			$newsLike->like = NewsLikes::NEWS_LIKE;
			$newsLike->save(false);
			$news = News::model()->findByPk($attr['newsID']);
			$news->saveCounters(array('viewers' => 1));
		}

		return $this->dump_result($newsLike, 'News');
	}

	public function newsByuser($attr)
	{
		$checkAccess = $this->confirmToken($attr['token'], $attr['userID']);

		$str = '';
		if (!empty($checkAccess['token'])) {
			if (is_array($checkAccess['token'])) {
				foreach ($checkAccess['token'] as $key => $val) {
					$str .= $val['code'] . ',';
				}
			}
		}

		$str = substr($str, 0, -1);
		$searchterm = explode(',', $str);
		$searchCondition = "id='" . implode("' OR id ='", $searchterm) . "'";

		if ($checkAccess['success'] == true && !empty($checkAccess['token']) && empty($checkAccess['userID'])) {
			if ($attr['is_avaliable'] == 0) {
				$models = News::model()->findAllByAttributes(array('token' => $checkAccess['token']));
			} else {
				$models = News::model()->findAllByAttributes(array('token' => $checkAccess['token']));
			}
		}


		$criteria = new CDbCriteria();
		if ($checkAccess['success'] == true && !empty($checkAccess['userID'])) {
			if ($attr['is_avaliable'] == 0) {
				$criteria->condition = 'userID=:userID AND token=' . $searchCondition;
				$criteria->params = array(':userID' => $checkAccess['userID']);
				$models = News::model()->findAll($criteria);
			} elseif ($checkAccess['success'] == true && !empty($checkAccess['token'])) {
				$criteria->condition = 'is_avaliable=:is_available AND (userID=:userID OR token=' . $searchCondition . ' )';
				$criteria->params = array(':is_available' => News::STRIP, ':userID' => $checkAccess['userID']);
				$models = News::model()->findAll($criteria);
			}
		}

		$response = array();
		if (!empty($models)) {
			foreach ($models as $key => $model) {
				$response[$key]['id'] = $model->id;
				$response[$key]['taskID'] = $model->taskID;
				$response[$key]['userID'] = $model->userID;
				$response[$key]['token'] = $model->token;
				$response[$key]['managerID'] = $model->managerID;
				$response[$key]['is_avaliable'] = $model->is_avaliable;
				$response[$key]['latitude'] = $model->latitude;
				$response[$key]['longitude'] = $model->longitude;
				$response[$key]['title'] = $model->title;
				$response[$key]['content'] = $model->content;
				$response[$key]['locale'] = $model->locale;
				$response[$key]['timeCreate'] = yii()->dateFormatter->format('dd.MM.yyyy HH:mm', $model->timeCreate);
				$response[$key]['viewers'] = $model->viewers;


				$thumbnail = '';
				if ($model['thumbnail'] != '') {
					$thumbnail = createUrl(yii()->storage->getFileUrl($model->thumbnail));
				}
				$response[$key]['thumbnail'] = $thumbnail;

				$image = '';
				if ($model['image'] != '') {
					$image = createUrl(yii()->storage->getFileUrl($model->image));
				}
				$response[$key]['image'] = $image;

				$video = '';
				if ($model['video'] != '') {
					$video = createUrl(yii()->storage->getFileUrl($model->video));
				}
				$response[$key]['video'] = $video;

				if ($model->task != null) {
					$price = $model->task->price;
				} else {
					$price = $model->dop_price;
				}
				$response[$key]['price'] = $price;
				if ($model->author != null) {
					$author = $model->author->username;
				} else {
					$author = null;
				}
				$response[$key]['username'] = $author;
			}
		}

		return $this->dump_result($response, 'News');
	}


	protected function checkLikeNewsByUser($newsID, $userID = null, $token = null)
	{
		if ($userID != '') {
			$model = NewsLikes::model()->findByAttributes(array('newsID' => $newsID, 'userID' => $userID));
		} elseif ($token != '') {
			$model = NewsLikes::model()->findByAttributes(array('newsID' => $newsID, 'token' => $token));
		}
		return $model;
	}

	private function processFile($model, $attr)
	{
		Yii::import('application.extensions.image.Image');

		if (yii()->request->isPostRequest) {
			$model->attributes = $attr;
			$model->image = CUploadedFile::getInstance($model, 'image');
			$ext = $model->image->extensionName;
			if ($model->image instanceof CUploadedFile) {
				$folder = Yii::app()->runtimePath . DIRECTORY_SEPARATOR;

				$tmpName = tempnam($folder, 'img') . '.' . $ext;
				$origTmpName = $model->image->tempName;
				$img_info = getimagesize($origTmpName);
				$img_w = $img_info[0];
				$img_h = $img_info[1];
				$resize = new Image($origTmpName);
				$resize->resize($img_w, $img_h, Image::AUTO);
				$resize->save($tmpName);
				$model->image = Yii::app()->storage->addFile($tmpName, $tmpName);

				$tmpName = tempnam($folder, 'img') . '.' . $ext;
				$resize = new Image($origTmpName);
				$resize->resize(yii()->params['files']['thumbnailWidth'], yii()->params['files']['thumbnailHeight'], Image::AUTO);
				$resize->save($tmpName);
				$model->thumbnail = Yii::app()->storage->addFile($tmpName, $tmpName);
			}
			return $model;
		}
	}

	/*Set token if userID = null*/
	public function getToken($attr)
	{
		if ($attr['i_need_token'] == 'yes') {
			$apiKey = md5(randString(20));
			yii()->db->createCommand()->insert(
				'yc_api_keys',
				array(
					'id_user' => NULL,
					'code' => $apiKey,
					'status_confirm' => self::API_CONFIRM_BEFORE,
				));
			return $this->dump_result($apiKey, 'api_token');
		} else {
			$response['response'] = array('Result' => "error", 'Message' => yii::t('api', 'Specify the required parameters for the token', null, null, $this->locale));
			$this->_sendResponse(501, CJSON::encode($response));
		}
	}

	/*Set token if userID = null*/
	public function setAppleToken($attr)
	{
		$appleToken = $attr['set_token'];
		$userToken = $attr['user_token'];
		if (isset($appleToken)) {
			$checkToken = AppleToken::model()->findByAttributes(array('token' => $appleToken, 'userToken' => $userToken));
			if ($checkToken == null) {
				yii()->db->createCommand()->insert(
					'yc_apple_token',
					array(
						'token' => $appleToken,
						'userToken' => $userToken,
						'timeCreate' => time(),
					));
			} else {
				yii()->db->createCommand()->update(
					'yc_apple_token',
					array(
						'token' => $appleToken,
						'timeCreate' => time(),
					),
					'userToken=:userToken',
					array(':userToken' => $userToken));
			}
		}
		$response['response'] = array('Result' => "Success");
		$this->_sendResponse(200, CJSON::encode($response));
	}

	private function processFileUpload($model)
	{
		$model->attributes = $_POST['News'];
		$model->video = CUploadedFile::getInstance($model, 'video');
		if ($model->video instanceof CUploadedFile) {
			$model->video = yii()->storage->addFile($model->video->tempName, $model->video->name);
		}
		return $model;
	}

	protected function loadTaskModel($id)
	{
		return Tasks::model()->findByPk($id);
	}

	protected function confirmToken($token = null, $userID = null)
	{
		if ($token == null && $userID == null) {
			$this->validate(array('token OR userID' => $token), 'empty');
		}
		$arResp = array();
		if ($token != null) {
			$sql = 'SELECT count(id),id_user FROM yc_api_keys WHERE code ="' . $token . '"';
			$checkApiKey = yii()->db->createCommand($sql)->queryAll();
			if ($checkApiKey[0]['count(id)'] > 0) {
				$arResp['token'] = $token;
				$arResp['userID'] = $checkApiKey[0]['id_user'];
				$arResp['success'] = true;
			} else {
				$response['response'] = array('Result' => "error", 'Message' => yii::t('api', 'This Token not registered in the System', null, null, $this->locale));
				$this->_sendResponse(501, CJSON::encode($response));
			}
		} elseif ($userID != null) {
			$this->validate(array('userID' => $userID,), 'numeric');
			$sql = "SELECT count(id) FROM yc_users WHERE id =" . $userID;
			$checkUser = yii()->db->createCommand($sql)->queryAll();
			if ($checkUser[0]['count(id)'] > 0) {
				$sql = 'SELECT code FROM yc_api_keys WHERE id_user =' . $userID;
				$arToken = yii()->db->createCommand($sql)->queryAll();
				$arResp['userID'] = $userID;
				$arResp['token'] = $arToken;
				$arResp['success'] = true;
			} else {
				$response['response'] = array('Result' => "error", 'Message' => yii::t('api', 'This user is not registered in the system', null, null, $this->locale));
				$this->_sendResponse(501, CJSON::encode($response));
			}
		} else {
			$response['response'] = array('Result' => "error", 'Message' => yii::t('api', 'Token is empty!'));
			$this->_sendResponse(501, CJSON::encode($response));
		}
		return $arResp;
	}

	protected function getUserIdByToken($token)
	{
		return ApiKeys::model()->findByAttributes(array('code' => $token))->id_user;
	}

	protected function setTokenByUserID($userID = null, $token = null)
	{
		$model = ApiKeys::model()->findByAttributes(array('code' => $token));
		if ($model != null) {
			if ($model->id_user == null) {
				$model->id_user = $userID;
				$model->status_confirm = self::API_CONFIRM_SUCCESS;
				$model->save(false);
			}
		}
		return $model;
	}

	private function setUserIDInNews($userID, $token)
	{
		$news = News::model()->findAllByAttributes(array('token' => $token, 'userID' => null));
		if (!empty($news)) {
			foreach ($news as $val) {
				News::model()->updateByPk(
					$val->id,
					array(
						'userID' => $userID,
					)
				);
			}
		}

	}
}