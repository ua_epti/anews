<?
Yii::setPathOfAlias('helpers', realpath(dirname(__FILE__) . '/../components/helpers'));
Yii::setPathOfAlias('bootstrap', realpath(dirname(__FILE__) . '/../extensions/bootstrap'));
Yii::setPathOfAlias('modules', realpath(dirname(__FILE__) . '/../modules'));
Yii::setPathOfAlias('models', realpath(dirname(__FILE__) . '/../models'));
Yii::setPathOfAlias('editable', dirname(__FILE__) . '/../extensions/x-editable');
Yii::setPathOfAlias('NewsStatusButtonWidget', dirname(__FILE__) . '/../extensions/newsStatusButton/NewsStatusButtonWidget');

$arConfig = array(
	'theme' => 'bootstrap',
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'aNews',
	'preload' => array('log', 'bootstrap'),
	'language' => 'ru',
	'sourceLanguage' => 'en',
	'import' => array(
		'application.models.*',

		'application.components.*',

		'application.modules.user.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',

		'application.modules.admin.modules.rights.*',
		'application.modules.admin.modules.rights.models.*',
		'application.modules.admin.modules.rights.components.*',

		'ext.adminMenu.*',
		'ext.GMapCrop.*',
		'ext.managerMenu.*',
		'ext.tabsMenu.*',
		'ext.sideBarMenu.*',
		'ext.videoWidget.*',
		'ext.editable.*',
		'ext.LanguageSelector.*',
		'ext.newsStatusButton.NewsStatusButtonWidget',
		'ext.userMenu.*',
		'ext.SocialShareWidget.*',
	),
	'modules' => array(
		'admin' => array(
			'defaultController' => 'admin',
			'modules' => array(
				'rights' => array(
					'debug' => YII_DEBUG,
				),
				'user' => array(
					'hash' => 'md5',
					'sendActivationMail' => true,
					'loginNotActiv' => true,
					'activeAfterRegister' => false,
					'autoLogin' => true,
					'registrationUrl' => array('/user/registration'),
					'recoveryUrl' => array('/user/recovery'),
					'loginUrl' => array('/user/login'),
					'logoutUrl' => array('//user/logout'),
					'returnUrl' => array('/user'),
					'returnLogoutUrl' => array('//site/index'),
					'loginRequiredAjaxResponse' => array('/site/login'),
					'profileUrl' => array('/'),
//					'managerReturnUrl' => array('//admin'),
				),
				'tasks' => array(),
				'news' => array(),
//				'images' => array(),
//				'content' => array(),
//				'video' => array(),
				'paypal' => array(),
				'payment' => array(),
			),
		),
		'user' => array(
			'hash' => 'md5',
			'sendActivationMail' => true,
			'loginNotActiv' => false,
			'activeAfterRegister' => false,
			'autoLogin' => false,
			'registrationUrl' => array('//user/registration'),
			'recoveryUrl' => array('//user/recovery'),
			'loginUrl' => array('//user/login'),
			'logoutUrl' => array('//user/logout'),
			'returnUrl' => array('//user'),
			'returnLogoutUrl' => array('//site/index'),
			'loginRequiredAjaxResponse' => array('/site/login'),
			'profileUrl' => array('/'),
//			'managerReturnUrl' => array('//admin'),
		),
		'tasks' => array(),
		'news' => array(),
//		'images' => array(),
//		'content' => array(),
//		'video' => array(),
		'payment' => array(),
		'cms' => array(),
		'paypal' => array(),
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'enabled' => YII_DEBUG,
			'password' => 'iddqd',
			'ipFilters' => array('127.0.0.1', '::1', '192.168.1.*'),
			'generatorPaths' => array(
				'bootstrap.gii',
			),
		),
	),
	'components' => array(
			'gcm' => array(
				'class' => 'application.components.GCMPushMessage.GCMPushMessage',
				'serverApiKey' => 'AIzaSyCZUlmrQYbyqnILEtV6-_55kwwCm5V-e2Y',
			),
			'cms' => array(
				'class' => 'cms.components.Cms',
				'languages' => array('ru' => 'Русский', 'en' => 'English', 'il' => 'Israel'),
				'defaultLanguage' => 'ru',
				'allowedFileTypes' => 'jpg, gif, png',
				'allowedFileSize' => 1024,
				'attachmentPath' => '/files/cms/attachments/',
				'headingTemplate' => '<h1 class="heading">{heading}</h1>',
				'widgetHeadingTemplate' => '<h3 class="heading">{heading}</h3>',
				'pageTitleTemplate' => '{title} | {appName}',
				'appLayout' => 'application.views.layouts.main',
			),
			'PayPal' => array(
				'class' => 'application.components.paypal.PayPal',
				'apiLive' => true,

				/*sandBox data*/

				/*'apiUsername' => 'adib-facilitator_api1.9tv.co.il',
				'apiPassword' => '1396555067',
				'apiSignature' => 'AhJR.wS-s9ezDGVekTd79lnr1JeDAmmn0AssT6P0CYyaCixiSos4ap2c',
				'appID' => 'APP-80W284485P519543T',
				'endPoint' => 'https://svcs.sandbox.paypal.com/AdaptivePayments/Pay',*/
				/*live data*/

				'apiUsername' => 'appchannel9_api1.gmail.com',
				'apiPassword' => 'GBKD3SAFWVWWEY9F',
				'apiSignature' => 'ADf0YHBRID1eIM7O09hEegIkrukOA56I6ZZGzGojhf22L86Ck6iryf8g',
				'appID' => 'APP-8WS56164JV309822B',
				'endPoint' => 'https://api-3t.paypal.com/nvp',



				'returnUrl' => '/paypal/paypal/confirm/',
				'cancelUrl' => '/paypal/paypal/cancel/',
				'currency' => 'USD',
			),
			'editable' => array(
				'class' => 'editable.EditableConfig',
				'form' => 'bootstrap',
				'mode' => 'popup',
				'defaults' => array(
					'emptytext' => 'Click to edit'
				)
			),
			'clientScript' => array(
				'scriptMap' => array(
					'jquery.js' => false,
				),
				'enableJavaScript' => true,
			),
			'cache' => array(
				'class' => 'CDummyCache',
				'enabled' => !YII_DEBUG,
			),
			'user' => array(
				'class' => 'CRWebUser',
				'allowAutoLogin' => true,
				'autoUpdateFlash' => false,
			),
			'authManager' => array(
				'class' => 'RDbAuthManager',
				'defaultRoles' => array('Guest')
			),
			'urlManager' => array(
				'class' => 'UrlManager',
				'urlFormat' => 'path',
				'showScriptName' => false,
				'rules' => array(
					'<language:(ru|en|il)>/' => 'site/index',
//				'page/<name>-<id:\d+>.html' => 'cms/node/page',
					'<language:(ru|en|il)>/<controller:\w+>/<id:\d+>' => '<controller>/view',
					'<language:(ru|en|il)>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
					'<language:(ru|en|il)>/<controller:\w+>/<action:\w+>/*' => '<controller>/<action>',
					'<language:(ru|en|il)>/<module:[\w\-]+>/<controller:[\w\-]+>/<id:\d+>' => '<module>/<controller>/view',
					'<language:(ru|en|il)>/<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
					'<language:(ru|en|il)>/<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
					'<language:(ru|en|il)>/admin/<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>' => 'admin/<module>/<controller>/<action>',
				),
			),
			'db' => array(
				'connectionString' => 'mysql:host=localhost;dbname=livenews_db',
				'emulatePrepare' => true,
				'username' => 'livenews_user',
				'password' => 'Mfglgitiw3s',
				'charset' => 'utf8',
				'tablePrefix' => 'yc_',
				'enableProfiling' => YII_DEBUG,
				'schemaCachingDuration' => (YII_DEBUG ? 0 : 3600),
			),
			'errorHandler' => array(
				'errorAction' => '//site/error',
			),
			'log' => array(
				'class' => 'CLogRouter',
				'routes' => array(
					array(
						'class' => 'CFileLogRoute',
						'levels' => 'error, warning, info, strace',
					),
				),
			),
			'bootstrap' => array(
				'class' => 'bootstrap.components.Bootstrap',
			),
			'mailer' => array(
				'class' => 'CRMailer', //Core Mailer class name
				'mailer' => 'mail', //Engine: mail, sendmail or smtp
				'encoding' => '8bit', //Content-type encoding
				'fromEmail' => 'admin@LarkPay.ru', //Letter from e-mail
				'fromName' => 'Admin', //Letter from name
				'smtpHost' => 'localhost', //SMTP hostname or IP address
				'smtpPort' => '25', //SMTP Port: 25, 587 for ssl, 465 for tls
				'smtpSecure' => '', //SMTP secure connection: off, ssl or tls
				'smtpAuth' => false, //SMTP server requires authentication
				'smtpUsername' => '', //SMTP authentication username
				'smtpPassword' => '', //SMTP authentication password
				'smtpDebug' => false, //Enable SMTP debug
			),
			'storage' => array(
				'class' => 'CRStorage',
				'directory' => 'webroot.images.news.',
				'subDirectory' => true,
				'subDirectoryLevel' => 2,
				'subDirectoryChunk' => 2,
			),
		),
		'params' => array(
			'adminEmail' => 'adib@9tv.co.il',
			'files' => array(
				'allowImageTypes' => 'jpg,jpeg,png,bmp,gif,tif,tiff',
				'maxUploadSize' => 10 * 1024 * 1024,
				'imageWidth' => 667,
				'thumbnailWidth' => 300,
				'thumbnailMapWidth' => 964,
				'imageHeight' => 484,
				'thumbnailHeight' => 300,
				'thumbnailMapHeight' => 1226,
				'relatedLimit' => 4,
			),
			'languages' => array('ru', 'en', 'il'),
		),
	);
$sLocalConfig = getLocalConfigName(__FILE__);
$arConfig = (file_exists($sLocalConfig)) ? CMap::mergeArray($arConfig, require_once($sLocalConfig)) : $arConfig;
return $arConfig;