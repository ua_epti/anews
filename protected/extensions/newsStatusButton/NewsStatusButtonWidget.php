<?php
class NewsStatusButtonWidget extends CWidget
{
	public $status;
	public $id;

	public function init()
	{
		yii::import('application.modules.news.News');
	}

	public function run()
	{
		$arStatus = News::model()->getAvailableStatuses($this->status);
		if (!empty($arStatus)) {
			echo '<div class="span10">';
			echo '<div class="news_status_button">';
			foreach ($arStatus as $key => $status) {
				echo $this->GenerateButton($key, $this->id);
			}
			echo '</div>';
			echo '</div>';
		}
	}

	private function GenerateButton($status, $id)
	{
		$label = null;
		$url = createUrl("//news/newsStatus/single", array('id' => $id));
		$ajaxOptions = null;
		$htmlOptions = null;
		switch ($status) {
			case News::DEFAULT_STATUS:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
						'action' => 'statusModerate'
					),
					'success' => 'function(){
						window.location.reload();
					}'
				);
				$htmlOptions = array('class' => 'btn btn-info');
				break;
			case News::CONFIRM_STEP_1:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
						'action' => 'statusFirstLvl'
					),
					'success' => 'function(){
						window.location.reload();
					}'
				);
				$htmlOptions = array('class' => 'btn btn-info');
				break;
			case News::CONFIRM_STEP_2:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
						'action' => 'statusSecondLvl'
					),
					'success' => 'function(){
						window.location.reload();
					}'
				);
				$htmlOptions = array('class' => 'btn btn-info');
				break;
			case News::CONFIRM_STEP_3:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
						'action' => 'statusBeforePaid'
					),
					'success' => 'function(){
						window.location.reload();
					}'
				);
				$htmlOptions = array('class' => 'btn btn-info');
				break;
			case News::CONFIRM_STEP_4:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
						'action' => 'statusPaid'
					),
					'success' => 'function(data){
						$("#news_view").replaceWith(data);
					}'
				);
				$htmlOptions = array('class' => 'btn btn-info');
				break;
			case News::STRIP:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
						'action' => 'statusStrip'
					),
					'success' => 'function(){
						window.location.reload();
					}'
				);
				$htmlOptions = array('class' => 'btn btn-info');
				break;
			case News::ARCHIVE:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
						'action' => 'statusArchive'
					),
					'success' => 'function(){
						window.location.reload();
					}'
				);
				$htmlOptions = array('class' => 'btn btn-danger');
				break;
			case News::BANNED:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
						'action' => 'statusBanned'
					),
					'success' => 'function(){
						window.location.reload();
					}'
				);
				$htmlOptions = array('class' => 'btn btn-danger');
				break;
			case News::PAY_ERROR:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
						'action' => 'statusSecondLvl'
					),
					'success' => 'function(){
						window.location.reload();
					}'
				);
				$htmlOptions = array('class' => 'btn btn-danger');
				break;
			/*case News::DOWNLOAD:
				$label = News::$STATUSES[$status];
				$ajaxOptions = array(
					'type' => 'POST',
					'data' => array(
							'action' => 'download'
					),
					'success' => 'function(){

					}'
				);
				$htmlOptions = array('class' => 'btn btn-info');
				break;*/
		}
		return CHtml::ajaxButton($label, $url, $ajaxOptions, $htmlOptions);
	}
}

