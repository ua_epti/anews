<? $items = array(); ?>
<?
if (user()->isJuniorManager(user()->id)) {
	$items[] = array('label' => yii::t('inbox', 'Inbox'), 'url' => array('//news/default/index'));
	$items[] = array('label' => yii::t('tasks', 'Create Tasks'), 'url' => array('//tasks/default/create'));
//	$items[] = array('label' => yii::t('tasks', 'Create Tasks'), 'url' => array('//tasks/default/create'));
}
if (user()->isMiddleManager(user()->id)) {
//	$items[] = array('label' => yii::t('site', 'Tasks'), 'url' => '#', 'items' => array(
//		array('label' => yii::t('tasks', 'Create Tasks'), 'url' => array('//tasks/default/create')),
//		array('label' => yii::t('tasks', 'Control Tasks'), 'url' => array('//tasks/')),
//		array('label' => yii::t('tasks', 'Archive Tasks'), 'url' => array('//tasks/default/archive')),
//	));

//	$items[] = array('label' => yii::t('news', 'News'), 'url' => '#', 'items' => array(
//		array('label' => yii::t('news', 'Control News'), 'url' => array('//news/')),
//	));
}
if (user()->isHighManager(user()->id)) {
//	$items[] = array('label' => yii::t('tasks', 'Tasks'), 'url' => '#', 'items' => array(
//		array('label' => yii::t('tasks', 'Create Tasks'), 'url' => array('//tasks/default/create')),
//		array('label' => yii::t('tasks', 'Control Tasks'), 'url' => array('//tasks/')),
//		array('label' => yii::t('tasks', 'Archive Tasks'), 'url' => array('//tasks/default/archive')),
//	));
//	$items[] = array('label' => yii::t('site', 'News'), 'url' => '#', 'items' => array(
//		array('label' => yii::t('news', 'Control News'), 'url' => array('//news/')),
//		array('label' => yii::t('news', 'Archive News'), 'url' => array('//news/default/archive')),
//	));
}
if (user()->isAdmin()) {
	$items[] = array('label' => yii::t('inbox', 'Inbox'), 'url' => array('//admin/news/default/index'));
	$items[] = array('label' => yii::t('tasks', 'Tasks'), 'url' => array('//admin/tasks/default/index'));
//	$items[] = array('label' => yii::t('site', 'Strip'), 'url' => array('//admin/news/default/strip'));
	$items[] = array('label' => 'Платежи', 'url' => array('//payment/default/index'), 'active' => true);

	$items[] = array('label' => yii::t('site', 'Users'), 'url' => '#', 'class' => 'pull-left', 'items' => array(
		array('label' => yii::t('site', 'Control'), 'url' => array('//admin/user/admin')),
		array('label' => yii::t('site', 'Group fields'), 'url' => array('//admin/user/profile-field/admin')),
		array('label' => yii::t('site', 'Permissions'), 'url' => array('//admin/rights')))
	);
}
?>

<? $this->widget('bootstrap.widgets.TbMenu', array(
	'type' => 'tabs',
	'stacked' => false,
	'items' => $items
));
?>