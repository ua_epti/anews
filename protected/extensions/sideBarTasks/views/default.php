<? /*Admin menu*/ ?>
<? if (user()->isAdmin()): ?>
	<?
	/*TASKS*/
	$items = array();
	$items[] = array('label' => 'Задачи');
	$publishTrue = Tasks::model()->countByAttributes(array('is_publish' => Tasks::PUBLISH_TRUE));
	$publishFalse = Tasks::model()->countByAttributes(array('is_publish' => Tasks::PUBLISH_FALSE));
	$result = $publishTrue + $publishFalse;
	$items[] = array(
		'label' => 'Задания ' . '(' . $result . ')',
		'url' => createUrl('//admin/tasks/default/index'));?>
	<div class="row">
		<?$this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));?>
	</div>
	<? $items = array(); ?>
	<?$items[] = array(
		'label' => 'Архив заданий ' . '(' . Tasks::model()->countByAttributes(array('is_publish' => Tasks::CLOSE)) . ')',
		'url' => createUrl('//admin/tasks/default/archive'));
	?>
	<?$this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => $items
	));?>
<? endif; ?>
<? /*Manager menu*/ ?>
<? if (user()->isHighManager()): ?>
	<?
	/*TASKS*/
	$items = array();
	$items[] = array('label' => 'Задачи');
	$publishTrue = Tasks::model()->countByAttributes(array('is_publish' => Tasks::PUBLISH_TRUE));
	$publishFalse = Tasks::model()->countByAttributes(array('is_publish' => Tasks::PUBLISH_FALSE));
	$result = $publishTrue + $publishFalse;
	$items[] = array(
		'label' => 'Задания ' . '(' . $result . ')',
		'url' => createUrl('//admin/tasks/default/index'));?>
	<div class="row">
		<?$this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));?>
	</div>
	<? $items = array(); ?>
	<?$items[] = array(
		'label' => 'Архив заданий ' . '(' . Tasks::model()->countByAttributes(array('is_publish' => Tasks::CLOSE)) . ')',
		'url' => createUrl('//admin/tasks/default/archive'));
	?>
	<?$this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => $items
	));?>
<? endif; ?>

<? /*middle manager menu*/ ?>
<? if (user()->isMiddleManager()): ?>
	<?/*TASKS*/
	$items = array();
	$items[] = array('label' => 'Задачи');
	$publishTrue = Tasks::model()->countByAttributes(array('is_publish' => Tasks::PUBLISH_TRUE));
	$publishFalse = Tasks::model()->countByAttributes(array('is_publish' => Tasks::PUBLISH_FALSE));
	$result = $publishTrue + $publishFalse;
	$items[] = array(
	'label' => 'Задания ' . '(' . $result . ')',
	'url' => createUrl('//tasks/default/index'));?>
	<div class="row">
		<?$this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));?>
	</div>
	<? $items = array(); ?>
	<?$items[] = array(
		'label' => 'Архив заданий ' . '(' . Tasks::model()->countByAttributes(array('is_publish' => Tasks::CLOSE)) . ')',
		'url' => createUrl('//tasks/default/archive'));
	?>
	<?$this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => $items
	));?>
<? endif; ?>

<? /*junior manager menu*/ ?>
<? if (user()->isJuniorManager()): ?>
	<?
	$publishTrue = Tasks::model()->countByAttributes(array('is_publish' => Tasks::PUBLISH_TRUE));
	$publishFalse = Tasks::model()->countByAttributes(array('is_publish' => Tasks::PUBLISH_FALSE));
	$publishClose = Tasks::model()->countByAttributes(array('is_publish' => Tasks::CLOSE));
	$result = $publishTrue + $publishFalse;
	$items = array();
	$items[] = array('label' => 'Задачи');
	$items[] = array('label' => 'Задания (' . $result . ')', 'url' => createUrl('//tasks/default/index'));
	?>

	<div class="row">
		<?php $this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));
		?>
	</div>
	<?
	$items = array();
	$items[] = array('label' => 'Архив заданий (' . $publishClose . ')', 'url' => createUrl('//tasks/default/archive'));
	?>
	<?php $this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => $items
	));
	?>
<? endif; ?>