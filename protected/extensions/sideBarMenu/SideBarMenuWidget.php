<?php
class SideBarMenuWidget extends CWidget
{

	public function init()
	{
		yii::import('application.modules.news.models.*');
		yii::import('application.modules.tasks.models.*');
	}

	public function run()
	{
		$this->render('default', array());
	}
}

