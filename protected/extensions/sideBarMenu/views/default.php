<? /*Admin menu*/ ?>
<? if (user()->isAdmin()): ?>
	<?
	$items = array();
	$items[] = array('label' => 'Новости');
	$items[] = array(
		'label' => 'Входящие' . '(' . \News::model()->countByAttributes(array('is_avaliable' => News::DEFAULT_STATUS)) . ')',
		'url' => createUrl('//admin/news/default/index'));
	?>
	<div class="row">
		<?$this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));
		?>
	</div>
	<?
	$items = array();
	$items[] = array(
		'label' => 'Первый уровень' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::CONFIRM_STEP_1)) . ')',
		'url' => createUrl('//admin/news/default/firstLvl'),
	);
	$items[] = array(
		'label' => 'Второй уровень' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::CONFIRM_STEP_2)) . ')',
		'url' => createUrl('//admin/news/default/secondLvl')
	);?>
	<?$this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => $items
	));?>
	<?
	$items = array();
	$items[] = array(
		'label' => 'Готов к оплате' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::CONFIRM_STEP_3)) . ')',
		'url' => createUrl('//admin/news/default/thirdLvl')
	);
	?>
	<div class="row">
		<?$this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));?>
	</div>
	<?
	$items = array();
	$items[] = array(
		'label' => 'Оплачено' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::CONFIRM_STEP_4)) . ')',
		'url' => createUrl('//admin/news/default/paid')
	);
	$items[] = array(
		'label' => 'Лента' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::STRIP)) . ')',
		'url' => createUrl('//admin/news/default/strip')
	);
	$items[] = array(
		'label' => 'Не оплачено' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::PAY_ERROR)) . ')',
		'url' => createUrl('//admin/news/default/payError')
	);
	$this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => $items
	));
	$items = array();
	$items[] = array(
		'label' => 'Архив' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::ARCHIVE)) . ')',
		'url' => createUrl('//admin/news/default/archive')
	);
	?>
	<div class="row">
		<?    $this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));?>
	</div>
<? endif; ?>

<? /*Manager menu*/ ?>
<? if (user()->isHighManager()): ?>
	<?
	$items = array();
	$items[] = array('label' => 'Новости');?>
	<div class="row">
		<?$this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));?>
	</div>
	<?$items = array();
	$items[] = array(
		'label' => 'Второй уровень' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::CONFIRM_STEP_2)) . ')',
		'url' => createUrl('//news/default/secondLvl')
	);
	?>
	<?$this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => $items
	));?>
	<?
	$items = array();
	$items[] = array(
		'label' => 'Архив' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::ARCHIVE)) . ')',
		'url' => createUrl('//news/default/archive')
	);?>
	<div class="row">
		<?php $this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));
		?>
	</div>
<? endif; ?>

<? /*middle manager menu*/ ?>
<? if (user()->isMiddleManager()): ?>
	<?
	$items = array();
	$items[] = array('label' => 'Новости');
	/*$items[] = array(
		'label' => 'Входящие' . '(' . \News::model()->countByAttributes(array('is_avaliable' => News::DEFAULT_STATUS)) . ')',
		'url' => createUrl('//news/default/index'));
	*/
	?>
	<div class="row">
		<?$this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));
		?>
	</div>
	<?
	$items = array();
	/*$items[] = array(
		'label' => 'Второй уровень' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::CONFIRM_STEP_2)) . ')',
		'url' => createUrl('//news/default/secondLvl')
	);*/
	?>
	<? $items[] = array('label' => 'Первый уровень' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::CONFIRM_STEP_1)) . ')', 'url' => createUrl('//news/default/firstLvl')); ?>
	<?$this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => $items
	));?>
	<?
	/*$items = array();
	$items[] = array(
		'label' => 'Готов к оплате' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::CONFIRM_STEP_3)) . ')',
		'url' => createUrl('//news/default/thirdLvl')
	);
	*/?><!--
	<div class="row">
		<?/*$this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));*/?>
	</div>-->
	<?
	$items = array();
	$items[] = array(
		'label' => 'Архив' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::ARCHIVE)) . ')',
		'url' => createUrl('//news/default/archive')
	);?>
	<div class="row">
		<? $this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));?>
	</div>
<? endif; ?>

<? /*junior manager menu*/ ?>
<? if (user()->isJuniorManager()): ?>
	<?
	$items[] = array('label' => 'Новости');
	$items[] = array(
		'label' => 'Входящие' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::DEFAULT_STATUS)) . ')',
		'url' => createUrl('//news/default/index'));
	?>
	<div class="row">
		<? $this->widget('bootstrap.widgets.TbMenu', array(
			'type' => 'list',
			'items' => $items
		));
		?>
	</div>
	<?
	$items = array();
	$items[] = array(
		'label' => 'Архив' . '(' . News::model()->countByAttributes(array('is_avaliable' => News::ARCHIVE)) . ')',
		'url' => createUrl('//news/default/archive'),
	);
	?>
	<?php $this->widget('bootstrap.widgets.TbMenu', array(
		'type' => 'list',
		'items' => $items
	));
	?>
<? endif; ?>