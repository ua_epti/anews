<!--<script type="text/javascript" src="<?/*= baseUrl() . '/js/jquery.Jcrop.js ' */?>"></script>
<link rel="stylesheet" type="text/css" href="<?/*= baseUrl() ."/css/jquery.Jcrop.css"*/?>" />-->
<?php
class GMapCropWidget extends CWidget
{
	public $address;

	public function init()
	{
		registerScriptFile(baseUrl() . '/js/jquery.Jcrop.js');
		registerCssFile(baseUrl() . '/css/jquery.Jcrop.css');
	}

	public function run()
	{
		$this->render('default', array('address' => $this->address));
	}
}

