<style type="text/css">
	.control-panel
	{
		margin-bottom: 10px;
	}
</style>

<div class="control-panel">
	<button id="plus-button" type="button" class="btn">+</button>
	<button id="minus-button" type="button" class="btn">-</button>
</div>
<img id="cropbox"
     src="http://maps.googleapis.com/maps/api/staticmap?center=<?= $address ?>&zoom=8&size=1024x1024&scale=2&maptype=roadmap&format=jpg&sensor=false">


<script type="text/javascript">
	zoom = 6;
	center = "<?=$address?>";
	apiUrl = "http://maps.googleapis.com/maps/api/staticmap?";
	optionSize = "&size=1024x1024";
	optionMapType = "&maptype=roadmap";
	optionFormat = "&format=jpg";
	scale = "&scale=2";
	optionSensor = "&sensor=false";
	//	apiKey = "&key=AIzaSyBSdChsYp2kdWulgfbELOWLM8fY9daJ1Oc";

	function GMapAction() {
		var optionCenter = "center=" + center;
		var optionZoom = "&zoom=" + zoom;
		var mapUrl = apiUrl + optionCenter + optionZoom + optionSize + optionMapType + optionFormat + scale + optionSensor;
		console.log(mapUrl);
		$(".jcrop-holder img").attr("src", mapUrl);
		$("#map_url").val(mapUrl);
	}

	function updateCoords(c) {
		$('#x').val(c.x);
		$('#y').val(c.y);
		$('#w').val(c.w);
		$('#h').val(c.h);
	}

	function checkCoords() {
		if (parseInt($('#w').val())) return true;
		alert('Please select a crop region then press submit.');
		return false;
	}

	$(document).ready(function () {
		$("#map_url").val("http://maps.googleapis.com/maps/api/staticmap?center=<?=$address?>&zoom=8&size=1024x1024&scale=2&sensor=false&format=jpg");
		$("#plus-button").bind("click", function () {
			zoom++;
			center = $("#address").val();
			GMapAction();
		});
		$("#minus-button").bind("click", function () {
			zoom--;
			center = $("#address").val();
			GMapAction(zoom);
		});

		$("#address").bind("focusout", function () {
			center = $(this).val();
			GMapAction();
		});

		$('#cropbox').Jcrop({
			setSelect: [0, 0, 1024, 1024],
			allowResize: false,
			allowSelect: false,
			aspectRatio: 1,
			onSelect: updateCoords,
			onChange: updateCoords
//			onRelease: updateCoords
		});
	});
</script>