<?php
/**
 * TbButtonColumn class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright  Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets
 * @since 0.9.8
 */

Yii::import('zii.widgets.grid.CButtonColumn');

/**
 * Bootstrap button column widget.
 * Used to set buttons to use Glyphicons instead of the defaults images.
 */
class TbButtonColumn extends CButtonColumn
{

	/**
	 * @var string the view button icon (defaults to 'eye-open').
	 */
	public $viewButtonIcon = 'eye-open';

	/**
	 * @var string the update button icon (defaults to 'pencil').
	 */
	public $updateButtonIcon = 'pencil';

	/**
	 * @var string the delete button icon (defaults to 'trash').
	 */
	public $deleteButtonIcon = 'trash';
	public $shoppingcartButtonIcon = 'shopping-cart';
	public $downloadaltButtonIcon = 'download-alt';
	public $thumbsupButtonIcon = 'thumbs-up';
	public $okButtonIcon = 'ok';
	public $plusButtonIcon = 'plus';
	public $arrowupButtonIcon = 'arrow-up';
	public $arrowdownButtonIcon = 'arrow-down';
	public $userButtonIcon = 'user';
	public $cogButtonIcon = 'cog';
	public $removeButtonIcon = 'remove';

	/**
	 * Initializes the default buttons (view, update and delete).
	 */
	protected function initDefaultButtons()
	{
		parent::initDefaultButtons();
		if ($this->viewButtonIcon !== false && !isset($this->buttons['view']['icon'])) {
			$this->buttons['view']['icon'] = $this->viewButtonIcon;
		}
		if ($this->updateButtonIcon !== false && !isset($this->buttons['update']['icon'])) {
			$this->buttons['update']['icon'] = $this->updateButtonIcon;
		}
		if ($this->deleteButtonIcon !== false && !isset($this->buttons['delete']['icon'])) {
			$this->buttons['delete']['icon'] = $this->deleteButtonIcon;
		}
		if ($this->shoppingcartButtonIcon !== false && !isset($this->buttons['shopping-cart']['icon'])) {
			$this->buttons['shopping-cart']['icon'] = $this->shoppingcartButtonIcon;
		}
		if ($this->downloadaltButtonIcon !== false && !isset($this->buttons['download-alt']['icon'])) {
			$this->buttons['download-alt']['icon'] = $this->downloadaltButtonIcon;
		}
		if ($this->thumbsupButtonIcon !== false && !isset($this->buttons['thumbs-up']['icon'])) {
			$this->buttons['thumbs-up']['icon'] = $this->thumbsupButtonIcon;
		}
		if ($this->okButtonIcon !== false && !isset($this->buttons['ok']['icon'])) {
			$this->buttons['ok']['icon'] = $this->okButtonIcon;
		}
		if ($this->plusButtonIcon !== false && !isset($this->buttons['plus']['icon'])) {
			$this->buttons['plus']['icon'] = $this->plusButtonIcon;
		}
		if ($this->arrowupButtonIcon !== false && !isset($this->buttons['arrow-up']['icon'])) {
			$this->buttons['arrow-up']['icon'] = $this->arrowupButtonIcon;
		}
		if ($this->arrowdownButtonIcon !== false && !isset($this->buttons['arrow-down']['icon'])) {
			$this->buttons['arrow-down']['icon'] = $this->arrowdownButtonIcon;
		}
		if ($this->userButtonIcon !== false && !isset($this->buttons['user']['icon'])) {
			$this->buttons['user']['icon'] = $this->userButtonIcon;
		}
		if ($this->cogButtonIcon !== false && !isset($this->buttons['cog']['icon'])) {
			$this->buttons['cog']['icon'] = $this->cogButtonIcon;
		}
		if ($this->removeButtonIcon !== false && !isset($this->buttons['remove']['icon'])) {
			$this->buttons['remove']['icon'] = $this->removeButtonIcon;
		}
	}

	/**
	 * Renders a link button.
	 * @param string $id the ID of the button
	 * @param array $button the button configuration which may contain 'label', 'url', 'imageUrl' and 'options' elements.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data object associated with the row
	 */
	protected function renderButton($id, $button, $row, $data)
	{
		if (isset($button['visible']) && !$this->evaluateExpression($button['visible'], array('row' => $row, 'data' => $data))) {
			return;
		}

		$label = isset($button['label']) ? $button['label'] : $id;
		$url = isset($button['url']) ? $this->evaluateExpression($button['url'], array('data' => $data, 'row' => $row)) : '#';
		$options = isset($button['options']) ? $button['options'] : array();

		if (!isset($options['title'])) {
			$options['title'] = $label;
		}

		if (!isset($options['rel'])) {
			$options['rel'] = 'tooltip';
		}

		if (isset($button['icon'])) {
			if (strpos($button['icon'], 'icon') === false) {
				$button['icon'] = 'icon-' . implode(' icon-', explode(' ', $button['icon']));
			}

			echo CHtml::link('<i class="' . $button['icon'] . '"></i>', $url, $options);
		} else {
			if (isset($button['imageUrl']) && is_string($button['imageUrl'])) {
				echo CHtml::link(CHtml::image($button['imageUrl'], $label), $url, $options);
			} else {
				echo CHtml::link($label, $url, $options);
			}
		}
	}
}
