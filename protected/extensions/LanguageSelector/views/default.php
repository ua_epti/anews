<div id="language-select">
	<?if (sizeof($languages) < 4) {
		foreach ($languages as $key => $lang) {
			if ($key != $currentLang) {
				echo CHtml::link(
					'<img src="' . createUrl('/images/' . $key . '.png') . '" title="' . $lang . '" style="padding: 1px;" width=16 height=11>',
					$this->owner->createMultilanguageReturnUrl($key));
			};
		}
	} else {
		echo CHtml::form();
		foreach ($languages as $key => $lang) {
			echo CHtml::hiddenField(
				$key,
				$this->owner->createMultilanguageReturnUrl($key));
		}
		echo CHtml::dropDownList('language', $currentLang, $languages,
			array(
				'submit' => '',
			)
		);
		echo CHtml::endForm();
	}
	?>
</div>