<?
class LanguageSelectorWidget extends CWidget
{
	public function run()
	{
		$currentLang = Yii::app()->language;
		$languages = Yii::app()->params->languages;
		$this->render('default', array('currentLang' => $currentLang, 'languages' => $languages));
	}
}
