<? $items = array(); ?>
<? $items[] = array(
	'label' => yii::t('site', 'Account settings'),
	'url' => '#',
	'items' => array(
		array(
			'label' => yii::t('site', 'Profile'),
			'url' => array('//user/profile/'),
		),
	));
?>
<div class="head_menu">
	<?$this->widget('bootstrap.widgets.TbNavbar', array(
		'type' => 'inverse',
		'fixed' => false,
		'brand' => false,
		'collapse' => true,
		'items' => array(
			array(
				'class' => 'bootstrap.widgets.TbMenu',
				'htmlOptions' => array('class' => 'pull-right'),
				'items' => array(array('label' => yii::t('site', 'Logout') . '(' . yii()->user->name . ')', 'url' => array('/user/logout'), 'visible' => !yii()->user->isGuest),)
			),
			array(
				'class' => 'bootstrap.widgets.TbMenu',
				'items' => $items,
				'htmlOptions' => array('class' => 'pull-right', 'style' => 'margin-right:20px'),
			),
		),
	));
	?>
</div>