<? $items = array(); ?>
<?
if (user()->isJuniorManager(user()->id)) {
	$items[] = array('label' => "Входящие", 'url' => array('//news/default/index'));
	$items[] = array('label' => "Задания", 'url' => array('//tasks/default/index'), 'active' => true);
}
if (user()->isMiddleManager(user()->id)) {
	$items[] = array('label' => "Входящие", 'url' => array('//news/default/index'));
	$items[] = array('label' => "Задания", 'url' => array('//tasks/default/index'), 'active' => true);
}
if (user()->isHighManager(user()->id)) {

}
if (user()->isAdmin()) {
	$items[] = array('label' => yii::t('inbox', 'Inbox'), 'url' => array('//admin/news/default/index'));
	$items[] = array('label' => yii::t('tasks', 'Tasks'), 'url' => array('//admin/tasks/default/index'), 'active' => true);
//	$items[] = array('label' => yii::t('site', 'Strip'), 'url' => array('//admin/news/default/strip'));
	$items[] = array('label' => 'Платежи', 'url' => array('//payment/default/index'));

	$items[] = array('label' => yii::t('site', 'Users'), 'url' => '#', 'class' => 'pull-left', 'items' => array(
		array('label' => yii::t('site', 'Control'), 'url' => array('//admin/user/admin')),
		array('label' => yii::t('site', 'Group fields'), 'url' => array('//admin/user/profile-field/admin')),
		array('label' => yii::t('site', 'Permissions'), 'url' => array('//admin/rights')),
	));
}
?>

<? $this->widget('bootstrap.widgets.TbMenu', array(
	'type' => 'tabs',
	'stacked' => false,
	'items' => $items
));
?>