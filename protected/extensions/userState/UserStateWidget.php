<?php
class UserStateWidget extends CWidget
{

	public function init()
	{
		yii::import('models.UserState');
		yii::import('models.Currency');
	}

	public function run()
	{
		$user = User::model()->findByPk(user()->id);
		$userState = $user->userState;
		$this->render('default', array(
			'userState' => $userState,
		));
	}
}

