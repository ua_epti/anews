<?php
class VideoWidget extends CWidget
{

	public $videoUrl = null;

	public $width = 50;

	public $cssClass = null;

	public $height = 35;

	public function registerJs()
	{
//		$QtObject = 'qtobject.js';
//		registerScriptFile(baseUrl() . '/js/' . $QtObject);
//		$myQTPlayer = 'myQTPlayer.js';
//		registerScriptFile(baseUrl() . '/js/' . $myQTPlayer);
	}

	public function init()
	{
		$this->registerJs();
	}

	public function run()
	{
		$this->render('default', array(
			'videoUrl' => $this->videoUrl
		));
	}
}

